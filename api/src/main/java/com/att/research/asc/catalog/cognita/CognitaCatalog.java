package com.att.research.asc.catalog.cognita;


import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.io.FileWriter;
import java.io.IOException;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.UUID;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;
import java.util.Spliterator;
import java.util.Spliterators;


import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import java.util.stream.Collectors;

import java.util.function.Function;
import java.util.function.BiFunction;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.JXPathException;
import org.apache.commons.jxpath.JXPathNotFoundException;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.DumperOptions;

import com.google.common.collect.ImmutableMap;

import org.json.JSONObject;
import org.json.JSONArray;

import org.apache.commons.io.IOUtils;

import com.att.research.asc.checker.Target;
import com.att.research.asc.checker.Construct;
import com.att.research.asc.checker.Facet;
import com.att.research.asc.checker.Checker;
import com.att.research.asc.checker.TargetLocator;

import com.att.research.asc.catalog.Catalog;
import com.att.research.asc.catalog.ItemCatalog;

import com.att.research.asc.catalog.commons.MapBuilder;
import com.att.research.asc.catalog.commons.Proxy;
import com.att.research.asc.catalog.commons.ProxyBuilder;
import com.att.research.asc.catalog.commons.Future;
import com.att.research.asc.catalog.commons.Futures;
import com.att.research.asc.catalog.commons.Actions;
import com.att.research.asc.catalog.commons.Http;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import org.springframework.util.Base64Utils;

/*
import com.att.research.cognita.ccds.transport.CognitaArtifact;
import com.att.research.cognita.ccds.transport.CognitaSolution;
import com.att.research.cognita.ccds.client.ICommonDataServiceRestClient;
import com.att.research.cognita.ccds.client.CommonDataServiceRestClientImpl;
*/



/**
 */
public class CognitaCatalog extends ItemCatalog 
															implements Catalog {

	private ProxyBuilder	proxies;
	private URI						cdsLocation,
												nexusLocation;
	private String				cdsUser,
												nexusUser;


	public CognitaCatalog() {

		this.proxies = new ProxyBuilder()
											.withConverter(v -> { return v == null ? null : UUID.fromString(v.toString()); }, UUID.class)
											.withExtensions(new ImmutableMap.Builder<String, BiFunction<Proxy, Object[], Object>>() 
																			.put("data", (proxy, args) -> { return proxy.data(); } )
																			.build())
											.withContext(new ImmutableMap.Builder<String,Object>()
																			.put("catalog", this)
      																.build());
	}

	public void setCommonDataUri(URI theLocation) {
		this.cdsUser = theLocation.getUserInfo();

		try {
			this.cdsLocation = new URI(theLocation.getScheme(),
																 null,
																 theLocation.getHost(),
																 theLocation.getPort(),
																 theLocation.getPath(),
																 theLocation.getQuery(),
																 null);
		}
		catch (URISyntaxException urix) {
			throw new IllegalArgumentException("Invalid uri", urix);
		}
	}

	public void setNexusUri(URI theLocation) {
		this.nexusUser = theLocation.getUserInfo();

		try {
			this.nexusLocation = new URI(theLocation.getScheme(),
																	 null,
																	 theLocation.getHost(),
																	 theLocation.getPort(),
																	 theLocation.getPath(),
																	 theLocation.getQuery(),
																	 null);
		}
		catch (URISyntaxException urix) {
			throw new IllegalArgumentException("Invalid uri", urix);
		}
	}

	/**
	 * Specific 'decoding'
	 */
	protected String getArtifactName(JSONObject theArtifactInfo) {
		return ((JSONObject)theArtifactInfo).getString("name");
	}

	protected URI getArtifactURI(JSONObject theArtifactInfo) {
		return asURI(((JSONObject)theArtifactInfo).getString("uri"));
	}
		
	protected String getArtifactVersion(JSONObject theArtifactInfo) {
		return ((JSONObject)theArtifactInfo).getString("version");
	}

	protected JSONArray getItemArtifacts(JSONObject theItemInfo) {
		return theItemInfo.optJSONArray("artifacts");
	}

	protected boolean isArtifactOfName(String theName, JSONObject theArtifactInfo) {
		int pos = theName.indexOf(".");
		if (pos > 0)
			theName = theName.substring(0, theName.indexOf("."));
		return theName.equals(getArtifactName(theArtifactInfo));
	}

	/**
	 * Backend specific content retrieval
	 */
	protected String fetchArtifactContent(JSONObject theArtifactInfo) throws IOException {
		try {
			return Http.exchange(this.nexusLocation.toString() + getArtifactURI(theArtifactInfo),
													 HttpMethod.GET, new HttpEntity(prepareNexusHeaders()), String.class)
									.waitForResult();
		}
		catch(Exception x) {
			throw new IOException("Failed to fetch artifact content", x);
		}
	}

	// must make sure that all artifacts are present
	protected JSONObject fetchItemInfo(String theItemId) throws IOException {
		
		String[] 	idParts = theItemId.split("@");
		String		itemUri = this.cdsLocation + "/solution/" + idParts[0];

		if (idParts.length != 2)
			throw new IllegalArgumentException("Item identifier expected in form solution_uuid@revision_version");
		
		JSONObject itemInfo = null;
		try {
			itemInfo = Http.exchange(itemUri, HttpMethod.GET, new HttpEntity(prepareCdsHeaders()), JSONObject.class)
											.waitForResult();
		}
		catch(Exception x) {
			throw new IOException("Failed to fetch solution information", x);
		}
//System.out.println("solution info: " + itemInfo);
		String revisionId = null;
		JSONArray itemRevisions = null;
		try {
				itemRevisions = Http.exchange(itemUri + "/revision",
																			HttpMethod.GET, new HttpEntity(prepareCdsHeaders()), JSONArray.class)
														.waitForResult();
		}
		catch(Exception x) {
			throw new IOException("Failed to fetch solution revision information", x);
		}

		for (int i = 0; itemRevisions != null && i < itemRevisions.length(); i++) {
			JSONObject itemRevision = itemRevisions.getJSONObject(i);
			if (idParts[1].equals(itemRevision.getString("version"))) {
				revisionId = itemRevision.getString("revisionId");
			}
		}

		if (revisionId == null)
			throw new IOException("No revision " + idParts[1] + " is available for this solution");

		JSONArray itemArtifacts = null;
		try {
			itemArtifacts = Http.exchange(itemUri + "/revision/" + revisionId + "/artifact",
																		HttpMethod.GET, new HttpEntity(prepareCdsHeaders()), JSONArray.class)
										.waitForResult();
		}
		catch(Exception x) {
			throw new IOException("Failed to fetch solution revision artifacts information", x);
		}

System.out.println("solution artifacts: " + itemArtifacts);
		itemInfo.put("artifacts", itemArtifacts);

		return itemInfo;
	}

	protected JSONArray fetchItems(JSONObject theSelector) throws IOException {
		try {
			JSONObject data = Http.exchange(this.cdsLocation + "/solution",
													 HttpMethod.GET, new HttpEntity(prepareCdsHeaders()), JSONObject.class)
									.waitForResult();

			return data.getJSONArray("content");
		}
		catch(Exception x) {
			throw new IOException("Failed to fetch solution revision information", x);
		}
	}

	private HttpHeaders prepareCdsHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_OCTET_STREAM_VALUE);
		if (this.cdsUser != null)
	  	headers.add(HttpHeaders.AUTHORIZATION, "Basic " + Base64Utils.encodeToString(this.cdsUser.getBytes()));
		return headers;
	}
	
	private HttpHeaders prepareNexusHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.ACCEPT, MediaType.ALL_VALUE);
		//if (this.nexusUser != null)
	  //	headers.add(HttpHeaders.AUTHORIZATION, "Basic " + Base64Utils.encodeToString(this.nexusUser.getBytes()))
		return headers;
	}

	/**
	 * Catalog interface
	 */
	public Future<Folders> roots() {
		throw new UnsupportedOperationException();
	}

	/** */
	public Future<Folders> rootsByLabel(String theLabel) {
		throw new UnsupportedOperationException();
	}

	/** */
	public Future<Mixels> lookup(JSONObject theSelector) {
		Mixels mixels = new Mixels();
		try {
			for (Iterator i = fetchItems(theSelector).iterator(); i.hasNext();) {
				mixels.add(proxy((JSONObject)i.next(), Item.class));
			}
		}
		catch(IOException iox) {
			return Futures.failedFuture(iox);
		}
		return Futures.succeededFuture(mixels);
	}
	
	public Future<Mixels> lookup(String theAnnotation, JSONObject theSelector) {
		throw new UnsupportedOperationException();
	}
	
	/**
   */
	public FolderAction folder(String theFolderId) {
		throw new UnsupportedOperationException();
	}

	/**
	 */
	public <T extends Item> ItemAction<T> item(String theItemId) {
		throw new UnsupportedOperationException();
	}

	/**
   */
	public TemplateAction template(String theTemplateId) {
		return new ItemTemplateAction(theTemplateId);
	}

	/**
   */
	public TypeAction type(String theNamespace, String theTypeName) {
		return new ItemTypeAction("", theTypeName);
	}

	/** general catalog interface */

	/** */
	public URI getUri() {
		return this.cdsLocation;
	}

	/** */
	public String namespace() {
		return "cognita";
	}

	/** */
	public boolean same(Catalog theCatalog) {
		return true;
	}

	/** */
	public <T> T proxy(JSONObject theData, Class<T> theType) {
		return this.proxies.build(theData, theType);
	}


	
}
