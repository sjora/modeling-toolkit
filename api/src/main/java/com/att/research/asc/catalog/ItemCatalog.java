package com.att.research.asc.catalog;


import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.io.FileWriter;
import java.io.IOException;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.UUID;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;
import java.util.Spliterator;
import java.util.Spliterators;


import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import java.util.stream.Collectors;

import java.util.function.Function;
import java.util.function.BiFunction;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.JXPathException;
import org.apache.commons.jxpath.JXPathNotFoundException;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.DumperOptions;

import com.google.common.collect.ImmutableMap;

import org.json.JSONObject;
import org.json.JSONArray;

import org.apache.commons.io.IOUtils;

import com.att.research.asc.checker.Target;
import com.att.research.asc.checker.Construct;
import com.att.research.asc.checker.Facet;
import com.att.research.asc.checker.Checker;
import com.att.research.asc.checker.TargetLocator;

import com.att.research.asc.catalog.commons.MapBuilder;
import com.att.research.asc.catalog.commons.Proxy;
import com.att.research.asc.catalog.commons.ProxyBuilder;
import com.att.research.asc.catalog.commons.Future;
import com.att.research.asc.catalog.commons.Futures;
import com.att.research.asc.catalog.commons.Actions;


/**
 * Helper for the common case where templates are stored as yaml documents (on some backend system) of some
 * item.
 */
public abstract class ItemCatalog implements Catalog {

	/**
	 * caches tosca info catalogs between invocations related to data (type, template) stored in the catalog
	 * The key is whatever an concrete item catalog will be using as item identifier
	 */
	private Map<Object, com.att.research.asc.checker.Catalog>		catalogs = 
																								new HashMap<Object, com.att.research.asc.checker.Catalog>();




	/**
	 * Isolate the extraction of information from the JSON representations
	 */
	protected abstract String getArtifactName(JSONObject theArtifactInfo);
	
	protected abstract URI getArtifactURI(JSONObject theArtifactInfo);
		
	protected abstract String getArtifactVersion(JSONObject theArtifactInfo);

	protected abstract JSONArray getItemArtifacts(JSONObject theItemInfo);

  /**
	 * To be used by the locator, allows for the implementation of alternative comparison techniques
	 */
	protected boolean isArtifactOfName(String theName, JSONObject theArtifactInfo) {
		return theName.equals(getArtifactName(theArtifactInfo));
	}

	/**
	 * Isolates the backend specific content retrieval
	 */
	protected abstract String fetchArtifactContent(JSONObject theArtifactInfo) throws IOException;

	protected abstract JSONObject fetchItemInfo(String theItemId) throws IOException;

	/** */
	protected void putCatalog(Object theId, com.att.research.asc.checker.Catalog theCatalog) {
		this.catalogs.put(theId, theCatalog);
	}

	protected com.att.research.asc.checker.Catalog getCatalog(Object theId) {
		return this.catalogs.get(theId);
	}

	/** processing utilities */	
	protected Object resolve(JXPathContext theContext, String thePath) {
		try {
			return theContext.getValue(thePath);
		}
		catch(JXPathNotFoundException pnfx) {
			return null;
		}
	}

	//covers common TOSCA pattern of single entry maps
	protected Map.Entry<String,Map> toEntry(Object theValue) {
		return (Map.Entry<String,Map>)((Map)theValue).entrySet().iterator().next();
	}

	protected Map selectEntries(Map theOriginal, String... theKeys) {
		Arrays.sort(theKeys);
		Map selection = ((Set<Map.Entry>)theOriginal.entrySet()).stream()
																		.filter(e -> Arrays.binarySearch(theKeys, e.getKey().toString()) >= 0)
        														.collect(Collectors.toMap(
            																	e -> e.getKey(),
            																	e -> e.getValue()));
		return selection;
	}
	
	protected Map evictEntries(Map theOriginal, String... theKeys) {
		Arrays.sort(theKeys);
		Map selection = ((Set<Map.Entry>)theOriginal.entrySet()).stream()
																		.filter(e -> Arrays.binarySearch(theKeys, e.getKey().toString()) < 0)
        														.collect(Collectors.toMap(
            																	e -> e.getKey(),
            																	e -> e.getValue()));
		return selection;
	}

	protected MapBuilder renderEntry(Map.Entry theEntry, String... theKeys) {
		MapBuilder out = new MapBuilder();
		out.put("name", theEntry.getKey());

		for (String key: theKeys) {
			out.put(key, ((Map)theEntry.getValue()).get(key));
		}
		return out;
	}

	protected <T> Stream<T> stream(Iterator<T> theSource) {
		return 															 
			StreamSupport.stream(
				Spliterators.spliteratorUnknownSize(theSource,
																						Spliterator.NONNULL | Spliterator.DISTINCT | Spliterator.IMMUTABLE),
				false);
	}
		

	/*
	protected JSONArray selectModels(JSONArray theArtifacts) {
		JSONArray models = new JSONArray();
		if (theArtifacts == null)
			return models;

		for (int i = 0; i < theArtifacts.length(); i++) {
			JSONObject artifact = theArtifacts.getJSONObject(i);
			String description = artifact.optString("artifactDescription");
			if (description != null && description.equals("template")) {
				models.put(new JSONObject()
												.putOpt("name", artifact.optString("artifactName"))
												.putOpt("version", artifact.optString("artifactVersion"))
												.putOpt("description", artifact.optString("artifactType"))
												.putOpt("id", artifact.optString("artifactURL"))
												.putOpt("itemId", artifact.optString("artifactURL"))
										);
			}
		}
		return models;
	}
	*/

	protected void dumpTargets(String theDirName, Collection<Target> theTargets) {

		try {
			File targetDir = new File(theDirName);
			if(!targetDir.exists() && !targetDir.mkdirs()) {
    		throw new IllegalStateException("Couldn't create dir: " + theDirName);
			}
			for (Target t: theTargets) {
				FileWriter dump = new FileWriter(new File(theDirName, t.getName()));
				IOUtils.copy(t.open(), dump);
				dump.close();
			}
		}
		catch(IOException iox) {
			iox.printStackTrace();
		}	
	}						

	protected URI asURI(String theValue) {
		try {
			return new URI(theValue);
		}
		catch (URISyntaxException urisx) {
			throw new IllegalArgumentException("Invalid URI", urisx);
		}	
	}	
	
	protected UUID asUUID(String theValue) {
		return UUID.fromString(theValue);
	}

	/** 
	 */
	public class ItemTemplateAction implements Catalog.TemplateAction {

		private String																itemId;
		private Target																target;
		private com.att.research.asc.checker.Catalog	catalog;
		//the input context is built around the POJO strcture resulting from the checker processing of the
		//incoming template
		//the output context is where we build the json representation
		private JXPathContext													inCtx,
																									outCtx = JXPathContext.newContext(new HashMap());

		private boolean					doNodes,
														doNodeProperties,
														doNodePropertiesAssignments,
														doNodeRequirements,
														doNodeCapabilities,
														doNodeCapabilityProperties,
														doNodeCapabilityPropertyAssignments;

		public ItemTemplateAction(Target theTarget) {
			this.target = theTarget;
		}

		/**
		 * @param theArtifactId the uri of the template artifact
		 */
		public ItemTemplateAction(String theItemId) {
			this.itemId = theItemId;
		}

		public Catalog.TemplateAction withInputs() {
			return this;
		}
		
		public Catalog.TemplateAction withOutputs() {
			return this;
		}

		public Catalog.TemplateAction withNodes() {
			this.doNodes = true;
			return this;
		}

		protected ItemTemplateAction doNodes() {
			if (!this.doNodes)
				return this;

			Map nodes = (Map)resolve(this.inCtx, "/topology_template/node_templates");
			if (nodes == null)
				return this;

			this.outCtx.setValue(
				"/nodes",
				nodes.entrySet()
					.stream()
					.map(nodeEntry ->	new MapBuilder()
														.put("name", ((Map.Entry)nodeEntry).getKey())
														.put("description", this.itemId)
														.putAll(selectEntries((Map)((Map.Entry)nodeEntry).getValue(), "type"))	
														.build())
					.collect(Collectors.toList()));
			
			return this;
		}

		//pre-requisite: a call to 'withNodes'
		public Catalog.TemplateAction withNodeProperties() {
			this.doNodeProperties = true;
			return this;
		}

		protected ItemTemplateAction doNodeProperties() {
			if (!this.doNodeProperties)
				return this;

			Map nodes = (Map)resolve(this.inCtx, "/topology_template/node_templates");
			if (nodes == null)
				return this;
				
			nodes.entrySet()
				.stream()
					.forEach(node ->
										this.outCtx.setValue(
																	 "/nodes[name='" + ((Map.Entry)node).getKey() + "']/properties",
																	 stream(catalog.facets(Construct.Node,
																												 Facet.properties,
																												 ((Map)((Map.Entry)node).getValue()).get("type").toString()))
																		.map(propEntry -> 
																										new MapBuilder()
																													.put("name", propEntry.getKey())
																													.putAll((Map)propEntry.getValue())
																													.build())
																		.collect(Collectors.toList())
																	)
									);

			return this;
		}

		//pre-requisite: a call to 'withNodesProperties'
		public Catalog.TemplateAction withNodePropertiesAssignments() {
			this.doNodePropertiesAssignments = true;
			return this;
		}
	
		protected ItemTemplateAction doNodePropertiesAssignments() {
			if (!this.doNodePropertiesAssignments)
				return this;

			Map nodes = (Map)resolve(this.inCtx, "/topology_template/node_templates");
			if (nodes == null)
				return this;

			nodes.entrySet()
				.stream()
					.forEach(node -> 
						{
							List nodeProps = (List) resolve(
																this.outCtx, "/nodes[name='" + ((Map.Entry)node).getKey() + "']/properties");
							if (nodeProps == null) {
								return;
							}

							nodeProps
								.stream()
									.forEach(prop ->
										{
											//pick from
											String propPath = "/topology_template/node_templates/" + ((Map.Entry)node).getKey() +
																					"/properties/" + ((Map)prop).get("name");
											Object propValue = resolve(this.inCtx, propPath);
		//System.out.println("prop " + propValue);
											//to conform with the db based api we should analyze the value for function calls
											//dump at ..
											propPath = "/nodes[name='" + ((Map.Entry)node).getKey() + "']/properties[name='" + ((Map)prop).get("name") + "']";
											if (propValue != null) {
//												System.out.println("Current node value at '" + propPath + "' : " + ctx.getValue(propPath));

												this.outCtx.setValue(propPath + "/assignment",
																						 new ImmutableMap.Builder() 
																								.put("value", propValue)
																								.build());
											} 
										});
						});

			return this;
		}

		protected Map renderRequirementDefinition(Map.Entry theReq) {
			Map def = (Map)theReq.getValue();
			return new MapBuilder()
							.put("name", theReq.getKey())
							//capability must be present
							.put("capability", new MapBuilder()
																	.put("name", def.get("capability"))
																	.put("id", this.target.getName() + "/" + def.get("capability"))
																	.build())
							.putAll(evictEntries(def, "capability"))
							.build();
		}

		//TODO: see how this comes out of neo and match it
		protected Map renderRequirementAssignment(Map.Entry theReq) {
			Map def = (Map)theReq.getValue();
			return new MapBuilder()
							.put("name", theReq.getKey())
							//capability must be present
							.put("capability", new MapBuilder()
																	.put("name", def.get("capability"))
																	//we provide an id only if the capability points to a type
																	.putOpt("id", catalog.hasType(Construct.Capability, (String)def.get("capability")) ? (this.target.getName() + "/" + def.get("capability")) : null)
																	.build())
							.putAll(evictEntries(def, "capability"))
							.build();
			//return null;
		}

		public Catalog.TemplateAction withNodeRequirements() {
			this.doNodeRequirements = true;
			return this;
		}
	
		protected ItemTemplateAction doNodeRequirements() {
			if (!this.doNodeRequirements)
				return this;

			//requirements come first from the type and then can be further refined by their assignment within the
			//node template
			Map nodes = (Map)resolve(this.inCtx, "/topology_template/node_templates");
			if (nodes == null)
				return this;
			
			//type	
			nodes.entrySet()
				.stream()
					.forEach(node ->
										outCtx.setValue("/nodes[name='" + ((Map.Entry)node).getKey() + "']/requirements",
																	 StreamSupport.stream(
																			Spliterators.spliteratorUnknownSize(
																				catalog.requirements(
																											 ((Map)((Map.Entry)node).getValue()).get("type").toString()),
																				Spliterator.NONNULL | Spliterator.DISTINCT | Spliterator.IMMUTABLE),
																			false)
																		.map((Map.Entry reqEntry) -> 
																						renderRequirementDefinition(reqEntry))
																		.collect(Collectors.toList())
																	)
									);

			//merge assignments on top of definitions
			nodes.entrySet()
				.stream()
					.forEach(node -> {
							List nodeReqsAssigns = (List)resolve(this.inCtx,
														"/topology_template/node_templates/" + ((Map.Entry)node).getKey() + "/requirements");
							if (nodeReqsAssigns == null)
								return;
							nodeReqsAssigns
								.stream()
									.forEach(req -> {
											Map.Entry reqAssign = toEntry(req);
											catalog.mergeDefinitions(
												(Map)outCtx.getValue("/nodes[name='" + ((Map.Entry)node).getKey()+"']/requirements[name='" + reqAssign.getKey() + "']"),
												renderRequirementAssignment(reqAssign));
										});
									});

			return this;	
		}
		
		public Catalog.TemplateAction withNodeCapabilities() {
			this.doNodeCapabilities = true;
			return this;
		}
		
		protected Map renderCapabilityDefinition(Map.Entry theCap) {
			Map def = (Map)theCap.getValue();
			return new MapBuilder()
										.put("name", theCap.getKey())
										.put("type", new MapBuilder()
																	.put("name", def.get("type"))
																	.put("id", this.target.getName() + "/" + def.get("type"))
																	.build())
										.putAll(evictEntries(def, "properties", "type"))
										.build();
		}

		protected ItemTemplateAction doNodeCapabilities() {
			if (!this.doNodeCapabilities)
				return this;

			Map nodes = (Map)resolve(this.inCtx, "/topology_template/node_templates");
			if (nodes == null)
				return this;
			
			//collect capabilities through the node type hierarchy
	
			//we evict the properties from the node type capability declaration (when declaring a capability with the
			//node type some re-definition of capability properties can take place).
			nodes.entrySet()
				.stream()
				.forEach(node ->
									outCtx.setValue("/nodes[name='" + ((Map.Entry)node).getKey() + "']/capabilities",
															 		stream(catalog.facets(Construct.Node,
																											  Facet.capabilities,
																											  ((Map)((Map.Entry)node).getValue()).get("type").toString()))
																	.map((Map.Entry capEntry) -> 
																				renderCapabilityDefinition(capEntry))	
																	.collect(Collectors.toList())
																	)
									);

			return this;
		}
		
		public Catalog.TemplateAction withNodeCapabilityProperties() {
			this.doNodeCapabilityProperties = true;
			return this;
		}

		protected ItemTemplateAction doNodeCapabilityProperties() {
			
			if (!this.doNodeCapabilityProperties)
				return this;

			Map nodes = (Map)resolve(this.inCtx, "/topology_template/node_templates");
			if (nodes == null)
				return this;
			
			//pick up all the properties from the capability type hierarchy definition
			nodes.entrySet()
				.stream()
					.forEach(node ->
						{
							List nodeCapabilities = null;
							try {
								nodeCapabilities = (List)
																inCtx.getValue("/nodes[name='" + ((Map.Entry)node).getKey() + "']/capabilities");
							}
							catch(JXPathNotFoundException pnfx) {
								return;
							}

							//collect properties from the capability type hierarchy
							nodeCapabilities
								.stream()
									.forEach(capability -> {
										List capabilityProperties = StreamSupport.stream(
												Spliterators.spliteratorUnknownSize(
													catalog.facets(Construct.Capability,
																				 Facet.properties,
																				 ((Map)capability).get("type").toString()),
													Spliterator.NONNULL | Spliterator.DISTINCT | Spliterator.IMMUTABLE),
												false)
											.map((Map.Entry capEntry) -> 
																	new MapBuilder()
																		.put("name", capEntry.getKey())
																		.putAll((Map)capEntry.getValue())
																		.build())
											.collect(Collectors.toList());
										
										if (!capabilityProperties.isEmpty()) {
											outCtx.setValue("/nodes[name='" + ((Map.Entry)node).getKey() + "']/capabilities[name='" + ((Map)capability).get("name") + "']/properties", capabilityProperties);
										}
									});
											
								//and go over the node type (hierarchy) and pick up any re-definitions from there.
								StreamSupport.stream(
									Spliterators.spliteratorUnknownSize(
										catalog.facets(Construct.Node,
																	 Facet.capabilities,
																	 ((Map)((Map.Entry)node).getValue()).get("type").toString()),
										Spliterator.NONNULL | Spliterator.DISTINCT | Spliterator.IMMUTABLE),
									false)
								.forEach((Map.Entry capability) -> {
									//for each capability property that has some node type level re-definition
									Map properties = (Map)((Map)capability.getValue()).get("properties");
									if (properties == null)
										return;

									properties.entrySet()
										.stream()
											.forEach(property -> {
													String propertyLoc = "/nodes[name='" + ((Map.Entry)node).getKey() + "']/capabilities[name='" + ((Map)capability).get("name") + "']/properties[name='" + ((Map.Entry)property).getKey() + "']";
													outCtx.setValue(propertyLoc,
																				 	catalog.mergeDefinitions(
																						(Map)inCtx.getValue(propertyLoc), (Map)((Map.Entry)property).getValue()));
												});
									});
						});	

			return this;
		}
		

		public Catalog.TemplateAction withNodeCapabilityPropertyAssignments() {
			this.doNodeCapabilityPropertyAssignments = true;
			return this;
		}
	
		protected ItemTemplateAction doNodeCapabilityPropertyAssignments() {
			if (!this.doNodeCapabilityPropertyAssignments)
				return this;

			/*	
			Iterator nodeCapabilityProperties = contexts.get(this.target)
												.iteratePointers("/topology_template/node_templates//capabilities//properties");
			while (nodeCapabilityProperties.hasNext())	
				System.out.println("!!! :" + nodeCapabilityProperties.next());
			*/

			//this is a wasteful: we go over all declared nodes/capabilities/properties and check if there is an assigned 
			//value in the actual template. It is optimal to approach the problem from the other direction: go over delared
			//assignments and set them in the output structure ..

			List nodes = null;
			try {
				nodes = (List)inCtx.getValue("/nodes");
			}
			catch(JXPathNotFoundException pnfx) {
				return this;
			}

			if (nodes == null) //why do I need this test ??
				return this;
			
			nodes
				.stream()
					.forEach(node ->
						{
							List capabilities = null; 
							try {
								capabilities = (List)inCtx.getValue("/nodes[name='" + ((Map)node).get("name") + "']/capabilities");
							}
							catch(JXPathNotFoundException pnfx) {
								return;
							}

							capabilities
								.stream()
								.forEach(capability -> {
										List properties = null;
										try {
											properties = (List)inCtx.getValue("/nodes[name='" + ((Map)node).get("name") + "']/capabilities[name='" + ((Map)capability).get("name") + "']/properties");
										}
										catch(JXPathNotFoundException pnfx) {
											return;
										}

										properties
											.stream()
												.forEach(property -> {
													String location = "/nodes[name='" + ((Map)node).get("name") + "']/capabilities[name='" + ((Map)capability).get("name") + "']/properties[name='" + ((Map)property).get("name") + "']/assignment";

													//pick the value from the original
													try {
														Object assignment = resolve(this.inCtx, "/topology_template/node_templates/" +  ((Map)node).get("name") + "/capabilities/" + ((Map)capability).get("name") + "/properties/" + ((Map)property).get("name"));
														if (assignment != null) {
															outCtx.setValue(location,
																						  new ImmutableMap.Builder() 
																									.put("value", assignment)
																									.build());
														}
													}
													catch (JXPathNotFoundException pnfx) {
														//it's ok, no assignment
													}	
												});
									});
						});

			return this;
		}

		public Catalog.TemplateAction withPolicies() {
			return this;
		}

		public Catalog.TemplateAction withPolicyProperties() {
			return this;
		}

		public Catalog.TemplateAction withPolicyPropertiesAssignments() {
			return this;
		}

		/**
		 * Default implementation, if proxying needs to change then overrite.
		 */		
		public Future<Catalog.Template> execute() {
			try {
				doTemplate();
			}
			catch (Exception x) {
				return Futures.failedFuture(x);	
			}
			return Futures.succeededFuture(
									ItemCatalog.this.proxy(new JSONObject((Map)this.outCtx.getContextBean()), Catalog.Template.class));
		}

		/**
	 	 * If succesfull the jxpathcontext bean (ctx.getContextBean()) contains the POJO template representation 
		 */
		protected void doTemplate() throws Exception {

			if (this.target == null) {

				JSONObject itemInfo = fetchItemInfo(this.itemId);

				Checker checker = new Checker();
				ArtifactLocator locator = new ArtifactLocator(
																				ItemCatalog.this.getItemArtifacts(itemInfo),
																				ItemCatalog.this.getCatalog(this.itemId));
				checker.setTargetLocator(locator);

				Target template = locator.resolve("template");
				if (template == null)
					throw new Exception("Failed to locate template in " + locator);
	
				checker.check(template);
	
				for (Target t: checker.targets()) {
					if (t.getReport().hasErrors()) {
						//dumpTargets(resourceId.toString(), checker.targets());
						throw new Exception("Failed template validation: " + t.getReport());	
					}
				}
		
				this.target = template;
				this.catalog = checker.catalog();

				ItemCatalog.this.putCatalog(this.itemId, this.catalog);
				this.inCtx = JXPathContext.newContext(template.getTarget());
			}
		
			this.doNodes()
					.doNodeProperties()
					.doNodePropertiesAssignments()
					.doNodeRequirements()
					.doNodeCapabilities()
					.doNodeCapabilityProperties()
					.doNodeCapabilityPropertyAssignments();

			System.out.println(
				new JSONObject((Map)this.outCtx.getContextBean()).toString(2));

		}	
	}
	
	public class ItemTypeAction implements Catalog.TypeAction {

		private String 					name;
		private String					itemId;
		private JXPathContext		inCtx,
														outCtx;

		private boolean doHierarchy = false,
										doRequirements = false,
										doCapabilities = false;

		public ItemTypeAction(String theItemId,
												/*Construct theConstruct,*/ String theName) {
			this.itemId = theItemId;
			this.name = theName;
		}

		public Catalog.TypeAction withHierarchy() {
			this.doHierarchy = true;
			return this;
		}

		protected ItemTypeAction doHierarchy(com.att.research.asc.checker.Catalog theCatalog) {
			if (!this.doHierarchy)
				return this;

			outCtx.setValue(
				"/hierarchy",
				stream(theCatalog.hierarchy(Construct.Node, this.name))
					.skip(1) //skip self
					.map((Map.Entry type) -> 
										new MapBuilder()
													.put("name", type.getKey()) 
													.put("id", this.itemId + "/" + type.getKey())
													.putOpt("description", ((Map)type.getValue()).get("description"))
													.build())
										//renderEntry((Map.Entry)type, "description").build())
					.collect(Collectors.toList()));
			return this;
		}

		public Catalog.TypeAction withRequirements() {
			this.doRequirements = true;
			return this;
		}

		protected ItemTypeAction doRequirements(com.att.research.asc.checker.Catalog theCatalog) {
			if (!this.doRequirements)
				return this;

			outCtx.setValue("requirements",
									 stream(theCatalog.requirements(this.name))
										.map((Map.Entry req) -> {
													String capability = (String)((Map)req.getValue()).get("capability"),
																 node = (String)((Map)req.getValue()).get("capability");
													return new MapBuilder()
														.put("name", req.getKey())
														.put("id", this.itemId + "/" + req.getKey())
														.put("occurrences", ((Map)req.getValue()).get("occurrences")) 
														.put("capability", new MapBuilder()
																										.put("name", capability)
																										//if the capability points to a capability type then encode
																										//the type reference, else it is a name (within a node type)
																										.put("id", getCatalog(this.itemId).hasType(Construct.Capability, capability) ? (this.itemId + "/" + capability) : capability.toString())
																										.build())
														.put("node", new MapBuilder()
																										.putOpt("name", node)
																										.putOpt("id", node == null ? null : (this.itemId + "/" + node))
														 												.buildOpt())
														.put("relationship", ((Map)req.getValue()).get("relationship"))
													//renderEntry((Map.Entry)requirement, "occurrences", "node", "capability", "relationship")
														.build();
													})
									 	.collect(Collectors.toList()));
													
			return this;
		}

		public Catalog.TypeAction withCapabilities() {
			this.doCapabilities = true;
			return this;
		}

		protected ItemTypeAction doCapabilities(com.att.research.asc.checker.Catalog theCatalog) {
			if (!this.doCapabilities)
				return this;

			outCtx.setValue("capabilities",
									 stream(theCatalog.facets(Construct.Node, Facet.capabilities, this.name))
										 .map((Map.Entry capability) ->
														new MapBuilder()
															.put("name", capability.getKey())
															.put("type", new MapBuilder()
																							.put("name", ((Map)capability.getValue()).get("type"))
																							.put("id", this.itemId + "/" +  ((Map)capability.getValue()).get("type"))
																							.build())
															.put("occurrences", ((Map)capability.getValue()).get("occurrences")) 
															.putOpt("validSourceTypes", ((Map)capability.getValue()).get("validSourceTypes")) 
															.build()
														//renderEntry((Map.Entry)capability, "occurrences", "validSourceTypes")
													)
									 	 .collect(Collectors.toList()));
			return this;
		}
	
		/** */
		public Future<Catalog.Type> execute() {
			try {
				doType();
			}
			catch (Exception x) {
				return Futures.failedFuture(x);	
			}
			return Futures.succeededFuture(
								ItemCatalog.this.proxy(new JSONObject((Map)this.outCtx.getContextBean()), Catalog.Type.class));
		}

		/** */
		protected void doType() throws Exception {
			com.att.research.asc.checker.Catalog catalog = ItemCatalog.this.getCatalog(this.itemId);
			if (catalog == null)
				throw new Exception("No catalog available for item " + this.itemId + ". You might want to fetch the model first.");	

			if (!catalog.hasType(Construct.Node, this.name))
				throw new Exception("No " + this.name + " type in catalog for resource " +this.itemId);	

			this.outCtx = JXPathContext.newContext(
												new MapBuilder()
													.put("name", this.name)
													.put("id", this.itemId + "/" + this.name)
													.put("itemId", this.itemId + "/" + this.name)
													.build());
										//selectEntries(
										//	catalog.getTypeDefinition(Construct.Node, theName), "description"));
			this.doHierarchy(catalog)
					.doRequirements(catalog)
					.doCapabilities(catalog);

			System.out.println(new JSONObject((Map)this.outCtx.getContextBean()).toString(2));
		}	
	}

	
	/**
	 */
	public class ArtifactLocator implements TargetLocator {

		private JSONArray artifacts;
		private com.att.research.asc.checker.Catalog	catalog;

		protected ArtifactLocator(JSONArray theArtifacts, com.att.research.asc.checker.Catalog theCatalog) {
			this.artifacts = theArtifacts;
			this.catalog = theCatalog;
		}
	
		public boolean addSearchPath(URI theURI) {
			return false;
		}

		public boolean addSearchPath(String thePath) {
			return false;
		}

		public Iterable<URI> searchPaths() {
			return Collections.EMPTY_SET;
		}

		public Target resolve(String theName) {		
			JSONObject targetArtifactInfo = null;

			if (this.artifacts == null)
				return null;

			for (int i = 0; i < this.artifacts.length() && targetArtifactInfo == null; i++) {
				JSONObject artifactInfo = this.artifacts.getJSONObject(i);
				if (isArtifactOfName(theName, artifactInfo))
					targetArtifactInfo = artifactInfo;
			}

			if (targetArtifactInfo == null)
				return null;

			ArtifactTarget target = null;
			if (this.catalog != null) {
				//this is the caching ?!
				target = (ArtifactTarget)this.catalog.getTarget(ItemCatalog.this.getArtifactURI(targetArtifactInfo));
				if (target != null &&
						target.getVersion().equals(ItemCatalog.this.getArtifactVersion(targetArtifactInfo))) {
					return target;
				}
			}
			
			return new ArtifactTarget(targetArtifactInfo);
		}

	}


	/**
	 * A target (tosca document) represented as a backend artifact stored as a JSON object 
	 */
	public class ArtifactTarget extends Target {

		private String 			content;
		private JSONObject	artifactInfo;

		
		protected ArtifactTarget(JSONObject theArtifactInfo) {
			
			super(getArtifactName(theArtifactInfo),
						getArtifactURI(theArtifactInfo));

			this.artifactInfo = theArtifactInfo;
		}

		@Override
		public Reader open() throws IOException {
			if (this.content == null) {
				this.content = ItemCatalog.this.fetchArtifactContent(this.artifactInfo);
			}

			return new StringReader(this.content);
		}

		public String getVersion() {
			return getArtifactVersion(this.artifactInfo);
		}

	}


}
