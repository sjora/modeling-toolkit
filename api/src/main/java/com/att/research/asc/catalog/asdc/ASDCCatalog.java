package com.att.research.asc.catalog.asdc;


import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.io.FileWriter;
import java.io.IOException;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.UUID;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;
import java.util.Spliterator;
import java.util.Spliterators;


import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import java.util.stream.Collectors;

import java.util.function.Function;
import java.util.function.BiFunction;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.JXPathException;
import org.apache.commons.jxpath.JXPathNotFoundException;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.DumperOptions;

import com.google.common.collect.ImmutableMap;

import org.json.JSONObject;
import org.json.JSONArray;

import org.apache.commons.io.IOUtils;

import com.att.research.asc.checker.Target;
import com.att.research.asc.checker.Construct;
import com.att.research.asc.checker.Facet;
import com.att.research.asc.checker.Checker;
import com.att.research.asc.checker.TargetLocator;

import com.att.research.asc.catalog.Catalog;

import com.att.research.asc.catalog.commons.MapBuilder;
import com.att.research.asc.catalog.commons.Proxy;
import com.att.research.asc.catalog.commons.ProxyBuilder;
import com.att.research.asc.catalog.commons.Future;
import com.att.research.asc.catalog.commons.Futures;
import com.att.research.asc.catalog.commons.Actions;

import com.att.research.asc.catalog.asdc.ASDC;

/** */
public class ASDCCatalog 
													implements Catalog {

	private ASDC		asdc;

	private JSONObject									folders = new JSONObject();
	private String[]										folderFields = new String[] {"id", "itemId", "name"};

	private ProxyBuilder								proxies;
	private Map<Target, JXPathContext> 	contexts = new HashMap<Target, JXPathContext>();

	//resource and its catalog
	private Map<UUID, com.att.research.asc.checker.Catalog>		catalogs = 
																								new HashMap<UUID, com.att.research.asc.checker.Catalog>();

	public ASDCCatalog(URI theURI) {

		this.asdc = new ASDC();
		this.asdc.setUri(theURI);

		//this.catalog = theCatalog;
		initFolders();
	
		this.proxies = new ProxyBuilder()
											.withConverter(v -> { return v == null ? null : UUID.fromString(v.toString()); }, UUID.class)
											.withExtensions(new ImmutableMap.Builder<String, BiFunction<Proxy, Object[], Object>>() 
																			.put("data", (proxy, args) -> { return proxy.data(); } )
																			.build())
											.withContext(new ImmutableMap.Builder<String,Object>()
																			.put("catalog", this)
      																.build());
	}

	private void initFolders() {

		JSONArray labels = new JSONArray();
		labels.put("Folder");
		labels.put("DCAE");
		labels.put("Superportfolio");  //for CCD compatibility

		folders.put("Database", new JSONObject()
																.put("name", "Database")
																.put("id", "dcae_database")
																.put("itemId", "Database")
																.put("labels", labels));
		folders.put("Collector", new JSONObject()
																.put("name", "Collector")
																.put("id", "dcae_collector")
																.put("itemId", "Collector")
																.put("labels", labels));
		folders.put("Microservice", new JSONObject()
																.put("name", "Microservice")
																.put("id", "dcae_microservice")
																.put("itemId", "Microservice")
																.put("labels", labels));
		folders.put("Analytics", new JSONObject()
																.put("name", "Analytics")
																.put("id", "dcae_analytics")
																.put("itemId", "Analytics")
																.put("labels", labels));
		folders.put("Policy", new JSONObject()
																.put("name", "Policy")
																.put("id", "dcae_policy")
																.put("itemId", "Policy")
																.put("labels", labels));
		folders.put("Source", new JSONObject()
																.put("name", "Source")
																.put("id", "dcae_source")
																.put("itemId", "Source")
																.put("labels", labels));
		folders.put("Utility", new JSONObject()
																.put("name", "Utility")
																.put("id", "dcae_utility")
																.put("itemId", "Utility")
																.put("labels", labels));
	}

	
	public URI getUri() {
		return this.asdc.getUri();
	}

	public String namespace() {
		return "asdc";
	}

	public boolean same(Catalog theCatalog) {
		return true;
	}

	public <T> T proxy(JSONObject theData, Class<T> theType) {
		return proxies.build(theData, theType);
	}

	/** */
	public Future<Folders> roots() {

		Folders roots = new Folders();
		for (Iterator fi = folders.keys(); fi.hasNext(); ) {
			roots.add(proxies.build(folders.getJSONObject((String)fi.next()),
															Folder.class));
		} 
		return Futures.succeededFuture(roots);
	}
	
	/** */
	public Future<Folders> rootsByLabel(String theLabel) {

		Folders roots = new Folders();
		for (Iterator fi = folders.keys(); fi.hasNext(); ) {
			JSONObject folder = folders.getJSONObject((String)fi.next());
			JSONArray labels = folder.getJSONArray("labels"); 
			
			for (int i = 0; i < labels.length(); i++) {
				if (labels.get(i).equals(theLabel)) {
						roots.add(proxies.build(folder, Folder.class));
				}
			}
		} 
		return Futures.succeededFuture(roots);
	}
	
	/** */
	public Future<Mixels> lookup(JSONObject theSelector) {
		return Futures.succeededFuture(new Mixels());
	}
	
	public Future<Mixels> lookup(String theAnnotation, JSONObject theSelector) {
		return Futures.succeededFuture(new Mixels());
	}

	/** */
	public ItemAction item(String theItemId) {
		return new ResourceAction(UUID.fromString(theItemId));
	}

	/** */
	public FolderAction folder(String theFolderId) {
		return new FolderAction(theFolderId);
	}


	public TemplateAction template(String theId) {
/*
		Target target = catalog.targets().stream()
												.filter(t -> theName.equals(resolveTargetName(t)))
													.findAny()
														.get();
		if (target == null)
			return null; //unknown template

		//expensive, we calculate a context every time
		//contexts.putIfAbsent(t, new JXPathContext(t.getTarget()));
		if (null == contexts.get(target))
			contexts.put(target, JXPathContext.newContext(target.getTarget()));

		return new TemplateAction(target);
*/
		return new TemplateAction(theId);
	}	
	
	public TypeAction type(String theItemId, String theName) {
		return new TypeAction(UUID.fromString(theItemId), theName);
	}

	protected static String resolveTargetName(Target theTarget) {
		return (String)((Map)((Map)theTarget.getTarget()).get("metadata")).get("template_name");
	}
	
	protected Object resolve(Target theTarget, String thePath) {
//System.out.println("> " + contexts.get(theTarget));
//System.out.println("> " + contexts.get(theTarget).getContextBean());
//System.out.println("> " + contexts.get(theTarget).getValue(thePath));
		try {
			return contexts.get(theTarget).getValue(thePath);
		}
		catch(JXPathNotFoundException pnfx) {
			return null;
		}
	}

	//covers common TOSCA pattern of single entry maps
	public Map.Entry<String,Map> toEntry(Object theValue) {
		return (Map.Entry<String,Map>)((Map)theValue).entrySet().iterator().next();
	}

	protected Map selectEntries(Map theOriginal, String... theKeys) {
		Arrays.sort(theKeys);
		Map selection = ((Set<Map.Entry>)theOriginal.entrySet()).stream()
																		.filter(e -> Arrays.binarySearch(theKeys, e.getKey().toString()) >= 0)
        														.collect(Collectors.toMap(
            																	e -> e.getKey(),
            																	e -> e.getValue()));
		return selection;
	}
	
	protected Map evictEntries(Map theOriginal, String... theKeys) {
		Arrays.sort(theKeys);
		Map selection = ((Set<Map.Entry>)theOriginal.entrySet()).stream()
																		.filter(e -> Arrays.binarySearch(theKeys, e.getKey().toString()) < 0)
        														.collect(Collectors.toMap(
            																	e -> e.getKey(),
            																	e -> e.getValue()));
		return selection;
	}

	protected MapBuilder renderEntry(Map.Entry theEntry, String... theKeys) {
		MapBuilder out = new MapBuilder();
		out.put("name", theEntry.getKey());
//System.out.println("selection > " + theEntry.getValue());

		for (String key: theKeys) {
			out.put(key, ((Map)theEntry.getValue()).get(key));
		}
		return out;
	}

	protected <T> Stream<T> stream(Iterator<T> theSource) {
		return 															 
			StreamSupport.stream(
				Spliterators.spliteratorUnknownSize(theSource,
																						Spliterator.NONNULL | Spliterator.DISTINCT | Spliterator.IMMUTABLE),
				false);
	}
		
	protected JSONArray selectModels(JSONArray theArtifacts) {
		JSONArray models = new JSONArray();
		if (theArtifacts == null)
			return models;

		for (int i = 0; i < theArtifacts.length(); i++) {
			JSONObject artifact = theArtifacts.getJSONObject(i);
			String description = artifact.optString("artifactDescription");
			if (description != null && description.equals("template")) {
				models.put(new JSONObject()
												.putOpt("name", artifact.optString("artifactName"))
												.putOpt("version", artifact.optString("artifactVersion"))
												.putOpt("description", artifact.optString("artifactType"))
												.putOpt("id", artifact.optString("artifactURL"))
												.putOpt("itemId", artifact.optString("artifactURL"))
										);
			}
		}
		return models;
	}

	protected JSONObject patchResource(JSONObject theResource) {
	
		theResource.remove("resources");
		theResource.putOpt("id", theResource.opt("uuid"));
		theResource.putOpt("itemId", theResource.opt("uuid"));

		return theResource;
	}

	protected static void dumpTargets(String theDirName, Collection<Target> theTargets) {

		try {
			File targetDir = new File(theDirName);
			if(!targetDir.exists() && !targetDir.mkdirs()) {
    		throw new IllegalStateException("Couldn't create dir: " + theDirName);
			}
			for (Target t: theTargets) {
				FileWriter dump = new FileWriter(new File(theDirName, t.getName()));
				IOUtils.copy(t.open(), dump);
				dump.close();
			}
		}
		catch(IOException iox) {
			iox.printStackTrace();
		}	
	}						

	protected static URI asURI(String theValue) {
		try {
			return new URI(theValue);
		}
		catch (URISyntaxException urisx) {
			throw new IllegalArgumentException("Invalid URI", urisx);
		}	
	}	
	
	protected static UUID asUUID(String theValue) {
		return UUID.fromString(theValue);
	}

	protected com.att.research.asc.checker.Catalog getCatalog(UUID theResourceId) {
		return this.catalogs.get(theResourceId);
	} 

	protected String getArtifactVersion(JSONObject theData) {
		return theData.getString("artifactVersion");
	} 
	
	protected String getArtifactName(JSONObject theData) {
		return theData.getString("artifactName");
	} 
	
	protected String getArtifactURL(JSONObject theData) {
		return theData.getString("artifactURL");
	} 
	
	protected URI getArtifactURI(JSONObject theData) {
		return asURI(theData.getString("artifactURL"));
	} 

	/** */
	public class ResourceAction implements Catalog.ItemAction<Resource> {

		private UUID 		iid;
		private boolean	doModels;

		protected ResourceAction(UUID theItemId) {
			this.iid = theItemId;
		}

		public ResourceAction withModels() {
			this.doModels = true;
			return this;
		}

		public ResourceAction withAnnotations() {
			return this;
		}
		
		@Override
		public Future<Resource> execute() {

			return Futures.advance(
							asdc.getResource(this.iid, JSONObject.class),
							resourceData -> {
								if (doModels) {
									resourceData.put("models", selectModels(resourceData.optJSONArray("artifacts")));
								}
								return proxies.build(patchResource(resourceData), Resource.class);
							});

/*
			JSONObject resourceData = asdc.getResource(this.iid, JSONObject.class)
																			.waitForResult();
			if (resourceData == null)
				return Futures.failedFuture(new RuntimeException("Failed to retrieve item " + this.iid));
			
			if (this.doModels) {
				//the (some) artifacts constitute the models
				resourceData.put("models", selectModels(resourceData.optJSONArray("artifacts")));
			}

			return Futures.succeededFuture(
							proxies.build(patchResource(resourceData), Resource.class));
*/
		}

		protected Future<JSONObject> executeRaw() {
		
			return Futures.advance(
							asdc.getResource(this.iid, JSONObject.class),
							resourceData -> {
								if (doModels) {
									resourceData.put("models", selectModels(resourceData.optJSONArray("artifacts")));
								}
								return resourceData;
							},
							resourceError -> new RuntimeException("Failed to retrieve item " + this.iid, resourceError));

		/*
			JSONObject resourceData = asdc.getResource(this.iid, JSONObject.class)
																	.waitForResult();
			if (resourceData == null)
				return Futures.failedFuture(new RuntimeException("Failed to retrieve item " + this.iid));
			
			if (this.doModels) {
				//the (some) artifacts constitute the models
				resourceData.put("models", selectModels(resourceData.optJSONArray("artifacts")));
			}
		
			return Futures.succeededFuture(resourceData);
			*/
		}			

	}


	/** */
	public class FolderAction implements Catalog.FolderAction {
		
		private boolean	doItems,
										doModels,
										doItemModels;
		private String  folderName;

		//use the id/UUID of the folder ??
		private FolderAction(String theFolderName) {
			this.folderName = theFolderName;
		}

		public FolderAction withAnnotations() {
			return this;
		}

		public FolderAction withAnnotations(String theSelector) {
			return this;
		}

	  public FolderAction withItems() {
			this.doItems = true;
			return this;
		}

		public FolderAction withItemAnnotations() {
			return this;
		}

		public FolderAction withItemAnnotations(String theSelector) {
			return this;
		}

		public FolderAction withItemModels() { 
			doItemModels = true;
			return this;
		}

		public FolderAction withParts() {
			return this;
		}
		
		public FolderAction withPartAnnotations() {
			return this;
		}

		public FolderAction withPartAnnotations(String theSelector) {
			return this;
		}

		@Override
		public Future<Folder> execute() {

			JSONObject folder = folders.optJSONObject(this.folderName);
			if (folder == null)
				return Futures.failedFuture(new RuntimeException("No such folder " + this.folderName));

			final JSONObject folderView = new JSONObject(folder, folderFields);
	
			return Futures.advance(
							asdc.getResources(JSONArray.class, "DCAE Component", this.folderName),
							resourcesData -> {
							/*
								for (int i = 0; i < resourcesData.length(); i++) { 
									JSONObject resource = resourcesData.getJSONObject(i);
									if (resource.getString("category").equals("DCAE Component") &&
											resource.getString("subCategory").equals(this.folderName)) {
		
										if (doItemModels) {
											try {
												resource = new ResourceAction(asUUID(resource.getString("uuid")))
																						.withModels()
																						.executeRaw()
																						.waitForResult();
											}
											catch (Exception x) {
												throw new RuntimeException(
																		"Failed to retrieve details of resource " + resource.getString("uuid"), x);
											}
										}

										folderView.append("items", patchResource(resource));
									}
								}
							*/
								Actions.CompoundAction<Resource> itemsAction = new Actions.BasicCompoundAction<Resource>();
								for (int i = 0; i < resourcesData.length(); i++) { 
									JSONObject resource = resourcesData.getJSONObject(i);
		//							if (resource.getString("category").equals("DCAE Component") &&
		//									resource.getString("subCategory").equals(this.folderName)) {
		
										if (doItemModels) {
											itemsAction.addAction(new ResourceAction(asUUID(resource.getString("uuid")))
																									.withModels());
										}
										else
											folderView.append("items", patchResource(resource));
									}
		//						}

								try {
									List<Resource> items = itemsAction.execute().waitForResult();
								System.out.println("Items: " + items.size()); 

									for (Resource r: items)
										folderView.append("items", patchResource(r.data()));
								}
								catch (Exception x) {
									throw new RuntimeException("Failed to retrieve folder items", x);
								}
								
								return proxies.build(folderView, Folder.class); 
							},
							resourcesError -> new RuntimeException("Failed to retrieve resources", resourcesError));

/*
			for (int i = 0; i < resources.length(); i++) { 
				JSONObject resource = resources.getJSONObject(i);
				if (resource.getString("category").equals("DCAE Component") &&
						resource.getString("subCategory").equals(this.folderName)) {
		
					if (this.doItemModels) {
						try {
							resource = new ResourceAction(asUUID(resource.getString("uuid")))
																					.withModels()
																					.executeRaw()
																					.waitForResult();
						}
						catch (Exception x) {
							return Futures.failedFuture(x);
						}
					}

					folder.append("items", patchResource(resource));
				}
			}
			
			return Futures.succeededFuture(
								proxies.build(folder, Folder.class)); 
*/
		}

	}

	/** */
	public class TemplateAction implements Catalog.TemplateAction {

		private String																artifactId;
		private Target																target;
		private com.att.research.asc.checker.Catalog	catalog;
		private JXPathContext													ctx = JXPathContext.newContext(new HashMap());

		private boolean					doNodes,
														doNodeProperties,
														doNodePropertiesAssignments,
														doNodeRequirements,
														doNodeCapabilities,
														doNodeCapabilityProperties,
														doNodeCapabilityPropertyAssignments;

		protected TemplateAction(Target theTarget) {
			this.target = theTarget;
		}

		/* expected to be the relative url provided by asdc for the template artifact */
		protected TemplateAction(String theArtifactId) {
			this.artifactId = theArtifactId;
		}

		public TemplateAction withInputs() {
			return this;
		}
		
		public TemplateAction withOutputs() {
			return this;
		}

		public TemplateAction withNodes() {
			this.doNodes = true;
			return this;
		}

		protected TemplateAction doNodes() {
			if (!this.doNodes)
				return this;

			Map nodes = (Map)resolve(this.target, "/topology_template/node_templates");
			if (nodes == null)
				return this;

			ctx.setValue(
				"/nodes",
				nodes.entrySet()
					.stream()
					.map(nodeEntry ->	new MapBuilder()
														.put("name", ((Map.Entry)nodeEntry).getKey())
														.put("description", this.artifactId)
														.putAll(selectEntries((Map)((Map.Entry)nodeEntry).getValue(), "type"))	
														.build())
					.collect(Collectors.toList()));
			
			return this;
		}

		//pre-requisite: a call to 'withNodes'
		public TemplateAction withNodeProperties() {
			this.doNodeProperties = true;
			return this;
		}

		protected TemplateAction doNodeProperties() {
			if (!this.doNodeProperties)
				return this;

			Map nodes = (Map)resolve(this.target, "/topology_template/node_templates");
			if (nodes == null)
				return this;
				
			nodes.entrySet()
				.stream()
					.forEach(node ->
											ctx.setValue("/nodes[name='" + ((Map.Entry)node).getKey() + "']/properties",
																	 stream(catalog.facets(Construct.Node,
																												 Facet.properties,
																												 ((Map)((Map.Entry)node).getValue()).get("type").toString()))
																		.map(propEntry -> 
																										new MapBuilder()
																													.put("name", propEntry.getKey())
																													.putAll((Map)propEntry.getValue())
																													.build())
																		.collect(Collectors.toList())
																	)
									);

			return this;
		}

		//pre-requisite: a call to 'withNodesProperties'
		public TemplateAction withNodePropertiesAssignments() {
			this.doNodePropertiesAssignments = true;
			return this;
		}
	
		protected TemplateAction doNodePropertiesAssignments() {
			if (!this.doNodePropertiesAssignments)
				return this;

			Map nodes = (Map)resolve(this.target, "/topology_template/node_templates");
			if (nodes == null)
				return this;

			nodes.entrySet()
				.stream()
					.forEach(node -> 
						{
							List nodeProps = null;
							try {
								nodeProps = (List)ctx.getValue("/nodes[name='" + ((Map.Entry)node).getKey() + "']/properties");
							}
							catch(JXPathNotFoundException pnfx) {
								return;
							}

							nodeProps
								.stream()
									.forEach(prop ->
										{
											//pick from
											String propPath = "/topology_template/node_templates/" + ((Map.Entry)node).getKey() +
																					"/properties/" + ((Map)prop).get("name");
											Object propValue = resolve(this.target, propPath);
		//System.out.println("prop " + propValue);
											//to conform with the db based api we should analyze the value for function calls
											//dump at ..
											propPath = "/nodes[name='" + ((Map.Entry)node).getKey() + "']/properties[name='" + ((Map)prop).get("name") + "']";
											if (propValue != null) {
//												System.out.println("Current node value at '" + propPath + "' : " + ctx.getValue(propPath));

												ctx.setValue(propPath + "/assignment",
																		 new ImmutableMap.Builder() 
																					.put("value", propValue)
																					.build());
											} 
										});
						});

			return this;
		}

		protected Map renderRequirementDefinition(Map.Entry theReq) {
			Map def = (Map)theReq.getValue();
			return new MapBuilder()
							.put("name", theReq.getKey())
							//capability must be present
							.put("capability", new MapBuilder()
																	.put("name", def.get("capability"))
																	.put("id", this.target.getName() + "/" + def.get("capability"))
																	.build())
							.putAll(evictEntries(def, "capability"))
							.build();
		}

		//TODO: see how this comes out of neo and match it
		protected Map renderRequirementAssignment(Map.Entry theReq) {
			Map def = (Map)theReq.getValue();
			return new MapBuilder()
							.put("name", theReq.getKey())
							//capability must be present
							.put("capability", new MapBuilder()
																	.put("name", def.get("capability"))
																	//we provide an id only if the capability points to a type
																	.putOpt("id", catalog.hasType(Construct.Capability, (String)def.get("capability")) ? (this.target.getName() + "/" + def.get("capability")) : null)
																	.build())
							.putAll(evictEntries(def, "capability"))
							.build();
			//return null;
		}

		public TemplateAction withNodeRequirements() {
			this.doNodeRequirements = true;
			return this;
		}
	
		protected TemplateAction doNodeRequirements() {
			if (!this.doNodeRequirements)
				return this;

			//requirements come first from the type and then can be further refined by their assignment within the
			//node template
			Map nodes = (Map)resolve(this.target, "/topology_template/node_templates");
			if (nodes == null)
				return this;
			
			//type	
			nodes.entrySet()
				.stream()
					.forEach(node ->
											ctx.setValue("/nodes[name='" + ((Map.Entry)node).getKey() + "']/requirements",
																	 StreamSupport.stream(
																			Spliterators.spliteratorUnknownSize(
																				catalog.requirements(
																											 ((Map)((Map.Entry)node).getValue()).get("type").toString()),
																				Spliterator.NONNULL | Spliterator.DISTINCT | Spliterator.IMMUTABLE),
																			false)
																		.map((Map.Entry reqEntry) -> 
																						renderRequirementDefinition(reqEntry))
																		.collect(Collectors.toList())
																	)
									);

			//merge assignments on top of definitions
			nodes.entrySet()
				.stream()
					.forEach(node -> {
							List nodeReqsAssigns = (List)resolve(this.target,
														"/topology_template/node_templates/" + ((Map.Entry)node).getKey() + "/requirements");
							if (nodeReqsAssigns == null)
								return;
							nodeReqsAssigns
								.stream()
									.forEach(req -> {
											Map.Entry reqAssign = toEntry(req);
											catalog.mergeDefinitions(
												(Map)ctx.getValue("/nodes[name='" + ((Map.Entry)node).getKey()+"']/requirements[name='" + reqAssign.getKey() + "']"),
												renderRequirementAssignment(reqAssign));
										});
									});

			return this;	
		}
		
		public TemplateAction withNodeCapabilities() {
			this.doNodeCapabilities = true;
			return this;
		}
		
		protected Map renderCapabilityDefinition(Map.Entry theCap) {
			Map def = (Map)theCap.getValue();
			return new MapBuilder()
										.put("name", theCap.getKey())
										.put("type", new MapBuilder()
																	.put("name", def.get("type"))
																	.put("id", this.target.getName() + "/" + def.get("type"))
																	.build())
										.putAll(evictEntries(def, "properties", "type"))
										.build();
		}

		protected TemplateAction doNodeCapabilities() {
			if (!this.doNodeCapabilities)
				return this;

			Map nodes = (Map)resolve(this.target, "/topology_template/node_templates");
			if (nodes == null)
				return this;
			
			//collect capabilities through the node type hierarchy
	
			//we evict the properties from the node type capability declaration (when declaring a capability with the
			//node type some re-definition of capability properties can take place).
			nodes.entrySet()
				.stream()
				.forEach(node ->
									ctx.setValue("/nodes[name='" + ((Map.Entry)node).getKey() + "']/capabilities",

															 stream(catalog.facets(Construct.Node,
																									  Facet.capabilities,
																									  ((Map)((Map.Entry)node).getValue()).get("type").toString()))
																.map((Map.Entry capEntry) -> 
																				renderCapabilityDefinition(capEntry))	
																.collect(Collectors.toList())
															)
									);

			return this;
		}
		
		public TemplateAction withNodeCapabilityProperties() {
			this.doNodeCapabilityProperties = true;
			return this;
		}

		protected TemplateAction doNodeCapabilityProperties() {
			
			if (!this.doNodeCapabilityProperties)
				return this;

			Map nodes = (Map)resolve(this.target, "/topology_template/node_templates");
			if (nodes == null)
				return this;
			
			//pick up all the properties from the capability type hierarchy definition
			nodes.entrySet()
				.stream()
					.forEach(node ->
						{
							List nodeCapabilities = (List)ctx.getValue("/nodes[name='" + ((Map.Entry)node).getKey() + "']/capabilities");
							if (nodeCapabilities == null)
								return;

							//collect properties from the capability type hierarchy
							nodeCapabilities
								.stream()
									.forEach(capability -> {
										List capabilityProperties = StreamSupport.stream(
												Spliterators.spliteratorUnknownSize(
													catalog.facets(Construct.Capability,
																				 Facet.properties,
																				 ((Map)capability).get("type").toString()),
													Spliterator.NONNULL | Spliterator.DISTINCT | Spliterator.IMMUTABLE),
												false)
											.map((Map.Entry capEntry) -> 
																	new MapBuilder()
																		.put("name", capEntry.getKey())
																		.putAll((Map)capEntry.getValue())
																		.build())
											.collect(Collectors.toList());
										
										if (!capabilityProperties.isEmpty()) {
											ctx.setValue("/nodes[name='" + ((Map.Entry)node).getKey() + "']/capabilities[name='" + ((Map)capability).get("name") + "']/properties", capabilityProperties);
										}
									});
											
								//and go over the node type (hierarchy) and pick up any re-definitions from there.
								StreamSupport.stream(
									Spliterators.spliteratorUnknownSize(
										catalog.facets(Construct.Node,
																	 Facet.capabilities,
																	 ((Map)((Map.Entry)node).getValue()).get("type").toString()),
										Spliterator.NONNULL | Spliterator.DISTINCT | Spliterator.IMMUTABLE),
									false)
								.forEach((Map.Entry capability) -> {
									//for each capability property that has some node type level re-definition
									Map properties = (Map)((Map)capability.getValue()).get("properties");
									if (properties == null)
										return;

									properties.entrySet()
										.stream()
											.forEach(property -> {
													String propertyLoc = "/nodes[name='" + ((Map.Entry)node).getKey() + "']/capabilities[name='" + ((Map)capability).get("name") + "']/properties[name='" + ((Map.Entry)property).getKey() + "']";
													ctx.setValue(propertyLoc,
																			 catalog.mergeDefinitions(
																					(Map)ctx.getValue(propertyLoc), (Map)((Map.Entry)property).getValue()));
												});
									});
						});	

			return this;
		}
		

		public TemplateAction withNodeCapabilityPropertyAssignments() {
			this.doNodeCapabilityPropertyAssignments = true;
			return this;
		}
	
		protected TemplateAction doNodeCapabilityPropertyAssignments() {
			if (!this.doNodeCapabilityPropertyAssignments)
				return this;

			/*	
			Iterator nodeCapabilityProperties = contexts.get(this.target)
												.iteratePointers("/topology_template/node_templates//capabilities//properties");
			while (nodeCapabilityProperties.hasNext())	
				System.out.println("!!! :" + nodeCapabilityProperties.next());
			*/

			//this is a wasteful: we go over all declared nodes/capabilities/properties and check if there is an assigned 
			//value in the actual template. It is optimal to approach the problem from the other direction: go over delared
			//assignments and set them in the output structure ..

			List nodes = null;
			try {
				nodes = (List)ctx.getValue("/nodes");
			}
			catch(JXPathNotFoundException pnfx) {
				return this;
			}
			
			nodes
				.stream()
					.forEach(node ->
						{
							List capabilities = (List)ctx.getValue("/nodes[name='" + ((Map)node).get("name") + "']/capabilities");
							if (capabilities == null)
								return;

							capabilities
								.stream()
								.forEach(capability -> {
										List properties = null;
										try {
											properties = (List)ctx.getValue("/nodes[name='" + ((Map)node).get("name") + "']/capabilities[name='" + ((Map)capability).get("name") + "']/properties");
										}
										catch(JXPathNotFoundException pnfx) {
											return;
										}

										properties
											.stream()
												.forEach(property -> {
													String location = "/nodes[name='" + ((Map)node).get("name") + "']/capabilities[name='" + ((Map)capability).get("name") + "']/properties[name='" + ((Map)property).get("name") + "']/assignment";

													//pick the value from the original
													try {
														Object assignment = resolve(this.target, "/topology_template/node_templates/" +  ((Map)node).get("name") + "/capabilities/" + ((Map)capability).get("name") + "/properties/" + ((Map)property).get("name"));
														if (assignment != null) {
															ctx.setValue(location,
																					 new ImmutableMap.Builder() 
																						.put("value", assignment)
																						.build());
														}
													}
													catch (JXPathNotFoundException pnfx) {
														//it's ok, no assignment
													}	
												});
									});
						});

			return this;
		}

		public TemplateAction withPolicies() {
			return this;
		}

		public TemplateAction withPolicyProperties() {
			return this;
		}

		public TemplateAction withPolicyPropertiesAssignments() {
			return this;
		}
		
		public Future<Template> execute() {
			
			if (this.target == null) {
				/* need to fetch the schema first
					artifact id expected in the form:
					/asdc/v1/catalog/{assetType}/{uuid}/artifacts/{artifactUUID}
				*/
				
				String[] parts = this.artifactId.split("/");
				if (parts.length != 8)
					return Futures.failedFuture(new Exception("Unexpected artifact id for template " + this.artifactId));	
					
				UUID resourceId = asUUID(parts[5]);
				this.catalog = ASDCCatalog.this.catalogs.get(resourceId);
				
				//if (this.catalog == null) {
					//if we find a catalog for this resource we have to figure out if it contains the required target ..

					try {
						JSONObject resource = new ResourceAction(resourceId)
																				.executeRaw()
																				.waitForResult();

						Checker checker = new Checker();
						TargetLocator locator = new ASDCLocator(
																					resource.getJSONArray("artifacts"),
																					 ASDCCatalog.this.catalogs.get(resourceId));
						checker.setTargetLocator(locator);

						Target template = locator.resolve("template");
						if (template == null)
								return Futures.failedFuture(new Exception("Failed to locate template in " + resource));
	
						checker.check(template);
	
						for (Target t: checker.targets()) {
							if (t.getReport().hasErrors()) {
								dumpTargets(resourceId.toString(), checker.targets());
								return Futures.failedFuture(new Exception("Failed template validation: " + t.getReport()));	
							}
						}
		
						this.target = template;
						this.catalog = checker.catalog();
						ASDCCatalog.this.catalogs.put(resourceId, this.catalog);
					//we should only be doing this if we discovered an update (by checking timestampts). Actually, we should
					//only do the artifact fetching if we detect an update
						ASDCCatalog.this.contexts.put(template, JXPathContext.newContext(template.getTarget()));
					}
					catch (Exception x) {
						return Futures.failedFuture(x);
					}
				//}	
			}
		
			this.doNodes()
					.doNodeProperties()
					.doNodePropertiesAssignments()
					.doNodeRequirements()
					.doNodeCapabilities()
					.doNodeCapabilityProperties()
					.doNodeCapabilityPropertyAssignments();

			JSONObject pack =	new JSONObject((Map)ctx.getContextBean())
													.put("name", this.target.getName().toString())
													.put("id", this.target.getLocation().toString())
													.put("itemId", this.target.getLocation().toString());
			System.out.println(pack.toString(2));
		
			return Futures.succeededFuture(
								proxies.build(pack, Template.class));
		}	
	}
	
	public class TypeAction implements Catalog.TypeAction {

		private String 					name;
		private UUID						resourceId;
		private JXPathContext		ctx;

		private boolean doHierarchy = false,
										doRequirements = false,
										doCapabilities = false;

		private TypeAction(UUID theResourceId,
												/*Construct theConstruct,*/ String theName) {
			this.resourceId = theResourceId;
			this.name = theName;
		}

		public TypeAction withHierarchy() {
			this.doHierarchy = true;
			return this;
		}

		protected TypeAction doHierarchy(com.att.research.asc.checker.Catalog theCatalog) {
			if (!this.doHierarchy)
				return this;

			ctx.setValue(
				"/hierarchy",
				stream(theCatalog.hierarchy(Construct.Node, this.name))
					.skip(1) //skip self
					.map((Map.Entry type) -> 
										new MapBuilder()
													.put("name", type.getKey()) 
													.put("id", resourceId + "/" + type.getKey())
													.putOpt("description", ((Map)type.getValue()).get("description"))
													.build())
										//renderEntry((Map.Entry)type, "description").build())
					.collect(Collectors.toList()));
			return this;
		}

		public TypeAction withRequirements() {
			this.doRequirements = true;
			return this;
		}

		protected TypeAction doRequirements(com.att.research.asc.checker.Catalog theCatalog) {
			if (!this.doRequirements)
				return this;

			ctx.setValue("requirements",
									 stream(theCatalog.requirements(this.name))
										.map((Map.Entry req) -> {
													String capability = (String)((Map)req.getValue()).get("capability"),
																 node = (String)((Map)req.getValue()).get("capability");
													return new MapBuilder()
														.put("name", req.getKey())
														.put("id", resourceId + "/" + req.getKey())
														.put("occurrences", ((Map)req.getValue()).get("occurrences")) 
														.put("capability", new MapBuilder()
																										.put("name", capability)
																										//if the capability points to a capability type then encode
																										//the type reference, else it is a name (within a node type)
																										.put("id", getCatalog(resourceId).hasType(Construct.Capability, capability) ? (resourceId + "/" + capability) : capability.toString())
																										.build())
														.put("node", new MapBuilder()
																										.putOpt("name", node)
																										.putOpt("id", node == null ? null : (resourceId + "/" + node))
														 												.buildOpt())
														.put("relationship", ((Map)req.getValue()).get("relationship"))
													//renderEntry((Map.Entry)requirement, "occurrences", "node", "capability", "relationship")
														.build();
													})
									 	.collect(Collectors.toList()));
													
			return this;
		}

		public TypeAction withCapabilities() {
			this.doCapabilities = true;
			return this;
		}

		protected TypeAction doCapabilities(com.att.research.asc.checker.Catalog theCatalog) {
			if (!this.doCapabilities)
				return this;

			ctx.setValue("capabilities",
									 stream(theCatalog.facets(Construct.Node, Facet.capabilities, this.name))
										 .map((Map.Entry capability) ->
														new MapBuilder()
															.put("name", capability.getKey())
															.put("type", new MapBuilder()
																							.put("name", ((Map)capability.getValue()).get("type"))
																							.put("id", resourceId + "/" +  ((Map)capability.getValue()).get("type"))
																							.build())
															.put("occurrences", ((Map)capability.getValue()).get("occurrences")) 
															.putOpt("validSourceTypes", ((Map)capability.getValue()).get("validSourceTypes")) 
															.build()
														//renderEntry((Map.Entry)capability, "occurrences", "validSourceTypes")
													)
									 	 .collect(Collectors.toList()));
			return this;
		}
	
		public Future<Type> execute() {
			com.att.research.asc.checker.Catalog catalog = ASDCCatalog.this.catalogs.get(this.resourceId);
			if (catalog == null)
				return Futures.failedFuture(new Exception("No catalog available for resource " + this.resourceId + ". You might want to fetch the model first."));	

			if (!catalog.hasType(Construct.Node, this.name))
				return Futures.failedFuture(new Exception("No " + this.name + " type in catalog for resource " + this.resourceId));	

			this.ctx = JXPathContext.newContext(
										new MapBuilder()
											.put("name", this.name)
											.put("id", this.resourceId + "/" + this.name)
											.put("itemId", this.resourceId + "/" + this.name)
											.build());
										//selectEntries(
										//	catalog.getTypeDefinition(Construct.Node, theName), "description"));
			this.doHierarchy(catalog)
					.doRequirements(catalog)
					.doCapabilities(catalog);

			JSONObject pack =	new JSONObject((Map)this.ctx.getContextBean());
			System.out.println(pack.toString(2));

			return Futures.succeededFuture(
								proxies.build((Map)ctx.getContextBean(), Type.class));
		}	
	}

	
	public static interface Resource extends Catalog.Item<Resource> {

		@Override
		@Proxy.DataMap(map="uuid")
		public String id();

		public UUID uuid();

		public UUID invariantUUID();

		public String category();

		public String subCategory();

		public String lastUpdaterFullName();

		@Proxy.DataMap(proxy=true, elementType=Artifact.class)
		public Artifacts artifacts();

	}
	
	public static class Resources extends Elements<Resource> {
	}

	public static interface Artifact extends Catalog.Element<Artifact> {
		
		@Proxy.DataMap(map="artifactName")
		public String name();

		@Proxy.DataMap(map="artifactType")
		public String type();

		@Proxy.DataMap(map="artifactDescription")
		public String description();

		@Proxy.DataMap(map="artifactUUID")
		public UUID uuid();

		@Proxy.DataMap(map="artifactVersion")
		public int version();

/*
		public default void fetchAndCatalog() {
			data().put("artifactBody",
								 catalog().   ((String)data().get("artifactURL"), byte[].class));
			try {
				System.out.println("fetched " + new String((byte[])data().get("artifactBody"), "UTF-8"));
			}
			catch (Exception x) {
				x.printStackTrace();
			}
		}
*/
	}

	public static class Artifacts extends Elements<Artifact> {
	}


	public class ASDCLocator implements TargetLocator {

		private JSONArray artifacts;
		private com.att.research.asc.checker.Catalog	catalog;

		private ASDCLocator(JSONArray theArtifacts, com.att.research.asc.checker.Catalog	theCatalog) {
			this.artifacts = theArtifacts;
			this.catalog = theCatalog;
		}
	
		public boolean addSearchPath(URI theURI) {
			return false;
		}

		public boolean addSearchPath(String thePath) {
			return false;
		}

		public Iterable<URI> searchPaths() {
			return Collections.EMPTY_SET;
		}

		public Target resolve(String theName) {		
			JSONObject targetArtifact = null;

			for (int i = 0; i < this.artifacts.length(); i++) {
				JSONObject artifact = this.artifacts.getJSONObject(i);
				if (artifact.getString("artifactName").equalsIgnoreCase(theName) ||
						artifact.getString("artifactDescription").equalsIgnoreCase(theName) )
					targetArtifact = artifact;
			}

			if (targetArtifact == null)
				return null;

			ASDCTarget target = null;
			if (this.catalog != null) {
				//this is the caching!!
				target = (ASDCTarget)this.catalog.getTarget(ASDCCatalog.this.getArtifactURI(targetArtifact));
				if (target != null &&
						target.getVersion().equals(ASDCCatalog.this.getArtifactVersion(targetArtifact))) {
					return target;
				}
			}
			
			return new ASDCTarget(targetArtifact);
		}
	}


	public class ASDCTarget extends Target {

		private String 			content;
		private JSONObject	artifact;

		private ASDCTarget(JSONObject theArtifact) {
			super(ASDCCatalog.this.getArtifactName(theArtifact),
						ASDCCatalog.this.getArtifactURI(theArtifact));
			this.artifact = theArtifact;
		}

//here is a chance for caching within the catalog! Do not go fetch the artifact if it has not been changed since the
//last fetch.

		@Override
		public Reader open() throws IOException {
			if (this.content == null) {
				try {
					this.content = ASDCCatalog.this.asdc.fetch(ASDCCatalog.this.getArtifactURL(this.artifact), String.class)
																								.waitForResult();
				}
				catch (Exception x) {
					throw new IOException("Failed to load " + ASDCCatalog.this.getArtifactURL(this.artifact), x);
				}	
			}

			//should return immediately a reader blocked until content available .. hard to handle errors
			return new StringReader(this.content);
		}

		public String getVersion() {
			return ASDCCatalog.this.getArtifactVersion(this.artifact);
		}

	}


	public static void main(String[] theArgs) throws Exception {

		ASDCCatalog catalog = new ASDCCatalog(new URI(theArgs[0]));

//		Folders roots = catalog.roots()
//												.waitForResult();

		Folder f = catalog.folder(theArgs[1])
												.withItems()
												.withItemModels()
												.execute()
												.waitForResult();

		System.out.println("folder: " + f.data());

		Resources items = (Resources)f.elements("items", Resources.class);
		if (items != null) {
			for (Resource item : items) {
				System.out.println("\titem: " + item.name() + " : " + item.data());
				Templates templates = (Templates)item.elements("models", Templates.class);
				if (templates != null) {
					for (Template t: templates) {
						Template ft = catalog.template(t.id())
																	.withNodes()
																	.withNodeProperties()
																	.withNodePropertiesAssignments()
																	.execute()
																	.waitForResult();
			
						System.out.println("template data: " + ft.data());
					}
				}
			}
		}

/*
		Resources items = (Resources)f.elements("items", Resources.class);
		for (Resource item : items) {
			
			System.out.println("\titem: " + item.name());
			item = (Resource)catalog.item(item.id())
																.withModels()
																.execute()
																.waitForResult();

			System.out.println("\titem data: " + item.data());

			

			Artifacts artifacts = item.artifacts();
			if (artifacts != null) {
				for (Artifact a: artifacts) {
					System.out.println("\t\t" + a.name());
				}
			}
		}
*/
//		catalog.item(theArgs[1])
//						.withModels()
//						.execute();

/*
		Checker checker = new Checker();
		checker.check(new File(theArgs[0]));

		Catalog catalog = checker.catalog();

		Actions acts = new Actions(checker.catalog());
		for (Target t: catalog.topTargets()) {
			if (t.getReport().hasErrors()) {
				System.out.println(t.getReport());
			}
			else {
				acts.type("com.att.d2.resource.vRouter")
						.withHierarchy()
						.withCapabilities()
						.withRequirements()
						.execute();
*/
/*	
				acts.template(resolveTargetName(t))
						.withNodes()
						.withNodeProperties()
						.withNodePropertiesAssignments()
						.withNodeRequirements()
						.withNodeCapabilities()
						.withNodeCapabilityProperties()
						.withNodeCapabilityPropertyAssignments()
						.execute();
*/
//			}
//		}
	}

}
