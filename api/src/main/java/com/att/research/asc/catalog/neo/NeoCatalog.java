package com.att.research.asc.catalog.neo;

//import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;

import java.lang.invoke.MethodHandles;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.EnumSet;

import java.util.stream.Collectors;

import java.util.logging.Logger;
import java.util.concurrent.CountDownLatch;

import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.BiFunction;

import java.io.Writer;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

import java.net.URI;
import java.net.URISyntaxException;

import java.text.MessageFormat;

import com.att.research.asc.catalog.Catalog;

import com.att.research.asc.catalog.commons.Neo;
import com.att.research.asc.catalog.commons.Action;
import com.att.research.asc.catalog.commons.Path;
import com.att.research.asc.catalog.commons.Future;
import com.att.research.asc.catalog.commons.Futures;
import com.att.research.asc.catalog.commons.Proxy;
import com.att.research.asc.catalog.commons.ProxyBuilder;

import org.json.JSONObject;
import org.json.JSONArray;

import org.apache.commons.io.IOUtils;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;

import org.apache.http.concurrent.FutureCallback;

import com.google.common.collect.ImmutableMap;
import com.google.common.reflect.Invokable;
import com.google.common.reflect.AbstractInvocationHandler;

import org.apache.commons.codec.binary.Base64;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.MethodUtils;


public class NeoCatalog implements Catalog {
					
	private URI											 neoUri,
																	 catalogUri;
	private String 									 neoAuth,
																	 namespace;
	private CloseableHttpAsyncClient client = null;
	private ProxyBuilder						 proxies;
	private Logger									 log = Logger.getLogger("com.att.research.asc.catalog.neo.NeoCatalog");


	public NeoCatalog(URI theUri) {
		try {
			this.neoUri = new URI(theUri.getScheme(),
														null, //userinfo
														theUri.getHost(),
														theUri.getPort(),
														null, //path
														null, //query
														null);  //fragment
			this.catalogUri = new URI(theUri.getScheme(),
														null, //userinfo
														theUri.getHost(),
														theUri.getPort(),
														null, //path
														null, //query
														theUri.getFragment());  //fragment
		}
		catch (URISyntaxException urisx) {
		}
		String userInfo = theUri.getUserInfo();
		if (userInfo != null) {
			this.neoAuth = "Basic " + new String(
												Base64.encodeBase64(
													userInfo.getBytes(Charset.forName("ISO-8859-1"))));
		}
		this.namespace = theUri.getFragment();

		this.proxies = new ProxyBuilder()
											.withExtensions(new ImmutableMap.Builder<String, BiFunction<Proxy, Object[], Object>>() 
																			.put("data", (proxy, args) -> { return proxy.data(); } )
																			.build())
											.withContext(new ImmutableMap.Builder<String,Object>()
																			.put("catalog", this)
      																.build());
	}

	public boolean same(Catalog theCatalog) {
		return this.catalogUri.equals(theCatalog.getUri());
	}

	public URI getUri() {
		return this.catalogUri;
	}

	public String namespace() {
		return this.namespace;
	}

	protected CloseableHttpAsyncClient client() {
		synchronized (this) {
			if (null == this.client || !this.client.isRunning()) {
				this.client = HttpAsyncClients.createDefault();
				this.client.start();
			}
		}
		return this.client;
	}

	/*
	 * closes the local http client. A new one would be created on the next
	 * query if needed ..
	 */
	public void close() {
		synchronized (this) {
			if (this.client != null) {				
				try {
					this.client.close();
				} 
				catch(IOException iox) {}
			}
		}
	}

	private static Map<String, Class> labelToTypeMapping =
																					new HashMap<String, Class>();
	static {
		labelToTypeMapping.put("CatalogItem", Catalog.Item.class);
		labelToTypeMapping.put("Folder", Catalog.Folder.class);
		labelToTypeMapping.put("Type", Catalog.Type.class);
		labelToTypeMapping.put("Template", Catalog.Template.class);
	}

	public Object proxy(JSONObject theData) {
		JSONArray labels = theData.getJSONArray("labels");
		if (labels == null) {
			//throw new RuntimeException("No labels information");
			return theData;
		}
		for (int i = 0; i < labels.length(); i++) {
			Class tt = labelToTypeMapping.get(labels.getString(i));
			if (tt != null) {
				return proxies.build(theData, tt);
			}
		}
		return theData;
		//throw new RuntimeException("No type information found for labels");
	}

	public <T> T proxy(JSONObject theData, Class<T> theType) {
		return proxies.build(theData, theType);
	}


	/****************** business interface ********************************/

	public Future<Folders> roots() {
		return new NeoAction<Folders>(Folders.class)
									.with("MATCH (n:Folder:" + this.namespace + ") " +
												"WHERE NOT (n)-[:PART_OF]->() " +
												"RETURN n, id(n) as id, LAST(labels(n)) as label")
									.execute();
	}

	public Future<Folders> rootsByLabel(String theLabel) {
		return new NeoAction<Folders>(Folders.class)
									.with("MATCH (f:Folder:" + this.namespace + ":" + theLabel + ") " +
												"WHERE NOT (f)-[:PART_OF]->() " +
												"RETURN f, id(f) as id, LAST(labels(f)) as label") 
									.execute();
	}	
	
	/**
   * Can return a mix of Item and Folder instances
   * improve on the selector!
   */
	public Future<Mixels> lookup(JSONObject theSelector) {
		return new NeoAction<Mixels>(Mixels.class, new AggregateDecoder())
									.with("MATCH (x:Folder:"+ NeoCatalog.this.namespace +
						 						(theSelector.has("label") ? (":" + theSelector.get("label") + ")") : ")") + " " +
											 "WHERE " + Neo.literalMap(theSelector, "x", "props", "=~", " AND ", f -> true) + " " +
											 "RETURN x, id(x) as id, LAST(labels(x)) as label",
											 new JSONObject().put("props", theSelector))
									.with("MATCH (x:CatalogItem) " +
									 		 "WHERE " + Neo.literalMap(theSelector, "x", "props", "=~", " AND ", f -> true) + " " +
											 "RETURN x, id(x) as id, LAST(labels(x)) as label",
											 new JSONObject().put("props", theSelector))
									.execute();
	}

	public Future<Mixels> lookup(String theAnnotation, JSONObject theSelector) {
		return new NeoAction<Mixels>(Mixels.class)
								.with(
			"MATCH (a:Annotation:" + namespace() + ":" + theAnnotation + (theSelector == null ? "" : " {" + Neo.literalMap(theSelector, "props") + "}") + ")-[:ANNOTATION_OF]->(c) " +
			"OPTIONAL MATCH (c)<-[:MODEL_OF]-(t:TOSCA:Template) " +
			"WHERE c:Folder OR c:CatalogItem " +
			"RETURN c, id(c) as id, LAST(labels(c)) as labels, collect(t) as models, a as annotation",
			new JSONObject().put("props", theSelector))
								.execute();
	}

	/**
   */
	public FolderAction folder(String theFolderId) {
		return new FolderAction("f.itemId=" + theFolderId);
	}

	/**
	 */
	public ItemAction item(String theItemId) {
		return new ItemAction("x.itemId=" + theItemId);
	}

	/**
	 */
	public TemplateAction template(String theTemplateId) {
		return new TemplateAction(theTemplateId);
	}

	/**
	 * The namespace is irrelevant here
	 */
	public TypeAction type(String theNamespace, String theTypeName) {
		return new TypeAction(theTypeName);
	}

	/****************** neo specific business interface ***************************/
	
	/*
   * Identify the catalog item(s) that have a node with the give properties and the correponding value assignments
	 * Note: the properties must belong to one node
   */
	public Future<Items> lookupItemsByToscaNodePropertyValue(JSONObject theSelector) {
		
		StringBuffer cypher = new StringBuffer(),
								 names = new StringBuffer();
		cypher.append("match ");
		for (Iterator i = theSelector.keys(); i.hasNext();) {
			String propName = (String)i.next();
			Object propValue = theSelector.get(propName);
			if (propValue != JSONObject.NULL) {
				cypher.append("(:Property:TOSCA{name:\"")
							.append(propName)
							.append("\"})")
							.append("<-[:OF_NODE_PROPERTY]-")
							.append("(:Assignment:TOSCA{value:\"")
							.append(propValue.toString())
							.append("\"})")
							.append("-[:OF_TEMPLATE]->")
							.append("(n:Node:TOSCA),");
			}
			names.append("'")
					 .append(propName)
					 .append("',");
		}
		cypher.append("(n)-[:NODE_OF]->(t:Template:TOSCA)-[:MODEL_OF]->(c:CatalogItem)<-[:ANNOTATION_OF]-(ca) ")
					.append("with n,t,c,collect(ca) as annotations ")
					.append("match (p:Property:TOSCA)<-[:OF_NODE_PROPERTY]-(a:Assignment:TOSCA)-[:OF_TEMPLATE]->(n:Node:TOSCA)-[:OF_TYPE]->(nt:Node:Type) ")
					.append("where p.name in [")
					.append(names.substring(0,names.length()-1))
					.append("] ")
					.append("return c, id(c) as id, labels(c) as labels, annotations, {name:t.name, description:t.description, node:{name:n.name, type:nt, properties:collect({name:p.name,value:a.value})}} as template");

		return new NeoAction<Items>(Items.class)
								.with(cypher.toString())
								.execute();
	}

	/* Provide the contents of recommendations folders not attached to anything (through annotations).
	 * We should handle this a more generic Recommendations Folder hierarchy ..
	 */
	public Future<Mixels> defaultRecommendations(/*String theAnnotationSelector */) {
		return new NeoAction<Mixels>(Mixels.class)
			.with("MATCH (rs:Folder:CCD:Recommendation)<-[:PART_OF]-(e) " +
						"WHERE NOT ()-[:ANNOTATION_OF]->(rs) " +
						"WITH e " +
						"OPTIONAL MATCH (e)<-[:MODEL_OF]-(t:TOSCA:Template) " +
						"WITH e, collect(t) as mdls " +
						"OPTIONAL MATCH (e)<-[:ANNOTATION_OF]-(a:Annotation) " +
						"RETURN e, id(e) as id, LAST(labels(e)) as label, mdls as models, collect({properties:a,label:LAST(labels(a)),id:id(a)}) as annotations")
			.execute();
	}

	public Future<Mixels> recommendationsForItemId(String theItemId) {
		return new NeoAction<Mixels>(Mixels.class)
			.with("MATCH " +
					  "(c:CatalogItem)<-[:ANNOTATION_OF]-" +
						"(a:Annotation:" + this.namespace + ":Recommendation)-[:ANNOTATION_OF]->" +
						"(rs:Folder:" + this.namespace + ":Recommendation)<-[:PART_OF]-(e) " +
						"WHERE c.itemId=" + theItemId + " " + 
						"RETURN e, id(e) as id, LAST(labels(e)) as labels")
			.execute();
	}

	/****************** specialized support classes ********************************/

	/**
	 */
	public class NeoDecoder {

		public JSONArray decodeResults(JSONArray theResults) {
			if (theResults.length() == 0)
				return new JSONArray();
			if (theResults.length() > 1)
				throw new RuntimeException("Expected only one result entry");

			JSONObject result = theResults.getJSONObject(0);
			try {
				return decodeData(result.getJSONArray("data"),
													result.getJSONArray("columns"));
			}
			catch (Throwable t) {
				System.out.println("Failed to decode data: " + t);
				throw t;
			}
			finally {
			}	
		}

		public String decodeErrors(JSONArray theErrors) {
			return theErrors.getJSONObject(0).getString("message");
		}
		
		/* process one result in 'data' format */
		protected JSONArray decodeData(JSONArray theData, JSONArray theCols) {
			JSONArray decodedData = new JSONArray();
			for (int i = 0; i < theData.length(); i++) {
				decodedData.put(
					decodeRow(theData.getJSONObject(i).getJSONArray("row"), theCols));
			}
			return decodedData;
		}
		
		/* Basic rules/expectations
		 * First column is the main object representation
		 * If there is second col
		 * Second column is the id (and must be aliased as 'id' in the statement)
		 *   if id = 0, first col is an array to be flatten in the main object (
		 *		(signals a path value)
		 * Other columns:
		 *   to be added to the main object (one level deep structure)
		 */
		protected JSONObject decodeRow(JSONArray theRow, JSONArray theCols) {

			JSONObject elem = null;
			if (theCols.length() == 1) {
				elem = theRow.getJSONObject(0);
			}
			else {
				long elemId = theRow.getLong(1);
				if (0 == elemId) {
					JSONArray elems = theRow.getJSONArray(0);
					elem = new JSONObject();
					for (int i = elems.length()-1; i>=0; i--) {
						mergeIn(elem, elems.getJSONObject(i));
					}
				}
				else {
					elem = theRow.getJSONObject(0);
				}

				elem = collectRowElem(elem);
				//!!this will overwrite any 'natural' id property the result might
				//contain
				//elem.putValue((String)theCols.get(1), theRow.get(1).toString());
				if (elemId != 0)
		//		try {
						//!!IMPOFRTANT: this is where the string representation of the id is created
						elem.put(theCols.getString(1), String.valueOf(elemId));
		//		} catch(Exception x) {
		//				x.printStackTrace(System.out);
		//		}
				for (int i = 2; i < theCols.length(); i++) {
					elem.put(theCols.getString(i), theRow.get(i));
				}
			}
			return elem;
		}

		protected JSONObject collectRowElem(JSONObject theElem) {
		
			for (Iterator fieldNames = theElem.keys(); fieldNames.hasNext(); ) {
				String fieldName = (String)fieldNames.next();
				Object elemValue = theElem.get(fieldName);	
				
				if (elemValue instanceof JSONArray) {
					elemValue = collect((JSONArray)elemValue);
					theElem.put(fieldName, elemValue);
				}
			}

			return theElem;
		}
		

		protected boolean check(JSONObject theOne, JSONObject theTwo, String theField) {
			return theOne.has(theField) && theTwo.has(theField) &&
						 theOne.get(theField).equals(theTwo.get(theField));
		}

		protected boolean checkId(JSONObject theOrig, JSONObject theNew) {
			return check(theOrig, theNew, "id");
		}
		
		protected boolean checkCollect(JSONObject theOne, JSONObject theTwo) {
			return check(theOne, theTwo, "collect") ||
						 checkId(theOne, theTwo);
		}

		/*
		 * The use of the 'collect' field is query local, we should have a 'merge' for merging items between queries
     * Because it is query local collect can be removed in here, merge would have to be kept until the whole
     * decoding is done ..
		 */
		protected JSONArray collect(JSONArray theArray) {

			int sz = theArray.length();
			if (sz < 2)
				return theArray;

			JSONArray res = new JSONArray();
			for (int i = sz-1; i>=0; i--) {
				boolean toAdd = true;
				
				for (int j = 0; j < res.length(); j++) {
					if (checkCollect(theArray.getJSONObject(i), res.getJSONObject(j))) {
						mergeIn(res.getJSONObject(j), theArray.getJSONObject(i));
						toAdd = false;
					}
				}

				if (toAdd)
					res.put(theArray.get(i));
			}

      for (int i = 0; i < res.length(); i++) {
        res.getJSONObject(i).remove("collect");
			}

			return res;
		}

		protected JSONObject mergeIn(JSONObject theTarget, JSONObject theDelta) {
			
			for (Iterator fieldNames = theDelta.keys(); fieldNames.hasNext(); ) {
				String fieldName = (String)fieldNames.next();
				theTarget.put(fieldName, theDelta.get(fieldName));
			}
			return theTarget;
		}
	}


	/**
	 */
	public class NeoFuture<T> 
										extends Futures.BasicFuture<T> {

		private Class<T>	ttype;			//target type, an instance of which is provided through the Future
		private Class			etype;			//element type: the parameterized type that was used to create a List target type 


		private JSONArray		 rawResult;
		private NeoDecoder	 decoder;


		NeoFuture(Class<T> theTargetType) {
			this(theTargetType, new NeoDecoder());
		}

		NeoFuture(Class<T> theTargetType, NeoDecoder theDecoder) {
			this.ttype = theTargetType;
			if (theTargetType.isArray()) {
				this.etype = theTargetType.getComponentType();
			}
			if (List.class.isAssignableFrom(this.ttype)) {
				this.etype = (Class)	
											((ParameterizedType)this.ttype.getGenericSuperclass()).
												getActualTypeArguments()[0];
			}
			if (theDecoder == null) {
				throw new IllegalArgumentException("Null decoder");
			}
			this.decoder = theDecoder;
		}
		
		@Override
		public T result() {
			if (this.result == null) {
				if (succeeded()) {
					try {
						this.result = proxyRawResult();
					}
					catch (Exception x) {
						throw new RuntimeException("Malformed raw result", x);
					}
				}
			}
			return this.result;
		}
		
		/**
		 * Access to the Json packed result (no proxying)
		 */
		public JSONArray rawResult() {
			return this.rawResult;
		}


		FutureCallback callback = new FutureCallback<HttpResponse>() {
			
			public void completed(final HttpResponse theResponse) {
//System.out.println("completed ->" + theResponse.getStatusLine());
//				try { NeoFuture.this.client.close(); } catch(IOException iox) {}
				NeoFuture.this.decodeResponse(theResponse);
			}

			public void failed(final Exception theError) {
//System.out.println("failed ->" + theError);
//				try { NeoFuture.this.client.close(); } catch(IOException iox) {}
				NeoFuture.this.cause(theError);
			}

			public void cancelled() {
//System.out.println("cancelled");
//				try { NeoFuture.this.client.close(); } catch(IOException iox) {}
				NeoFuture.this.result(null);
			}
		};

		protected NeoDecoder decoder() {
			return this.decoder;
		}

		protected void decodeResponse(HttpResponse theResponse) {
		
			int statusCode = theResponse.getStatusLine().getStatusCode();

			JSONObject neo4jResult = null;
			if (statusCode >= 300) {
				try {
					//try to read the error message
					neo4jResult = new JSONObject(
							IOUtils.toString(theResponse.getEntity().getContent(), "UTF-8"));
//System.err.println(neo4jResult);
				}
				catch (Exception x) {
					System.err.println("Failed to parse json from result: " + x);
				}
				cause(
					new IOException(
						"Neo REST request failed: " + theResponse.getStatusLine()));
				return;
			}

			try {
				neo4jResult = new JSONObject(IOUtils.toString(theResponse.getEntity().getContent(), "UTF-8"));
			}
			catch (Exception x) {
				cause(
		 			new IOException("no json in response", x));
				return;
			}
		
//System.out.println("result: " + neo4jResult);
			JSONArray data = null;
			data = neo4jResult.getJSONArray("errors");
			if (data != null && data.length() > 0) {
				cause(
		 			new IOException(decoder.decodeErrors(data)));
				return;
			}				

			this.rawResult = this.decoder
													.decodeResults(neo4jResult.getJSONArray("results"));
//System.out.println("raw result: " + this.rawResult);
			
			//this is used to signal the availability of the response	
			NeoFuture.this.result(null);
		}


	/**
	 * First stage decoding upon receiving the result extracts the JSON data from
	 * the response but does not proxy it.
	 */		
	/*
	 *  Aggregate functions (collect) over optional matches might can produce
	 *  empty collections which neo4j provides as an array with one element
	 *  whose all fields are null ..
	 *  A test can be embedded in the WITH or RETURN part of the cypher statement
	 *  in order to replace the result with a proper null value .. or .. we
	 *  could do the test in here.
	 *
			JSONArray row0 = ((JsonObject)data.get(0)).getArray("row");
			if (row0.size() == 1 &&
					isEmpty((JsonObject)row0.get(0)) {
				NeoFuture.this.setResult(null);
				return;
			}	
	*/

		/** */
		protected T proxyRawResult() throws Exception {
			if (null != this.etype) {
				//array/list
				if (this.ttype.isArray()) {
					Object a = Array.newInstance(this.etype, this.rawResult.length());
					for (int i = 0; i < this.rawResult.length(); i++) {
						Array.set(a, i, proxy(this.rawResult.getJSONObject(i), this.etype));
					}
					return (T)a;
				}
				else { //list
					List l = (List)this.ttype.newInstance();
					for (int i = 0; i < this.rawResult.length(); i++) {
						l.add(proxy(this.rawResult.getJSONObject(i), this.etype));
					}
					return (T)l;
				}
			}
			else {
				//single item
				if (this.rawResult.length() > 0) {
					return proxy(this.rawResult.getJSONObject(0), this.ttype);
				}
				else {
					return null;
				}
			}
		}

	}

	/** 
	 * A fluent interface for packing multiple cypher statements in one
	 * REST request. Intended to be derived from in order to build a friendly
	 * interfaces for larger retrieval procedures.
	 */
	public class NeoAction<T> implements Action<T> {

		protected JSONObject 		request;
		protected NeoFuture<T> 	result;

		protected NeoAction(Class<T> theMappingType) {
			this(null, theMappingType, new NeoDecoder());
		}

		public NeoAction(String theCypher, Class<T> theMappingType) {
			this(theCypher, theMappingType, new NeoDecoder());
		}
		
		public NeoAction(Class<T> theMappingType, NeoDecoder theDecoder) {
			this(null, theMappingType, theDecoder);
		}

		public NeoAction(String theCypher, Class<T> theMappingType, NeoDecoder theDecoder) {
	 		this.request = new JSONObject() 
											.put("statements", new JSONArray());
			this.result = new NeoFuture(theMappingType, theDecoder);
			if (theCypher != null) {	
				with(theCypher, (JSONObject)null);
			}
		}

		protected NeoDecoder decoder() {
			return this.result.decoder();
		}

		public NeoAction with(String theCypher,	JSONObject theParams) {
			JSONObject statement = new JSONObject()
																.put("statement", theCypher);
			if (theParams != null)
				statement.put("parameters", theParams);
			this.request
				.getJSONArray("statements")
					.put(statement);
			return this;
		}
		
		/* useful for creating based on separate params sets*/
		public NeoAction with(String theCypher,	JSONArray theParams) {
			JSONObject statement = new JSONObject()
																.put("statement", theCypher);
			if (theParams != null)
				statement.put("params", theParams);
			this.request
				.getJSONArray("statements")
					.put(statement);
			return this;
		}

		public NeoAction with(String theCypher) {
			return with(theCypher, (JSONObject)null);
		}

		public Future<T> execute() {
			HttpPost post = new HttpPost(
													NeoCatalog.this.neoUri + "/db/data/transaction/commit");
			post.setHeader(HttpHeaders.ACCEPT, "application/json");
			if (NeoCatalog.this.neoAuth != null)
				post.setHeader(HttpHeaders.AUTHORIZATION, NeoCatalog.this.neoAuth);
	
   		post.setEntity(new StringEntity(request.toString(),
	    							 									ContentType.APPLICATION_JSON));

			log.fine("NeoAction:execute :" + request);
		System.out.println("NeoAction:execute :" + request);

			NeoCatalog.this.client().execute(post, this.result.callback);
			return this.result;
		}
	}

	/* Utility for bridging the static catalog elements and the non-static 
	 * actions upon them 
	 */
	protected <E extends Element<E>, A extends NeoAction<E>> A
																newAction(Class<A> theActionType, E theTarget) {
		try {
			return
				theActionType
					.getConstructor(new Class[] {NeoCatalog.class, theTarget.getClass()})
						.newInstance(new Object[] {this, theTarget});
		}
		catch (InstantiationException |
					 NoSuchMethodException |
					 IllegalAccessException |
					 InvocationTargetException x) {
			//we should not end up here so make it loud
			throw new RuntimeException(x);
		}
	}

	/* Decodes nested Json structures by merging matching object. Results
	 * from multiple queries are seen as adding to an existing json structure.
	 *
	 * Example: we submit a set of queries such as
	 *
		{"statements":[
 			{"statement":"MATCH .. RETURN t, id(t) as id"},
 			{"statement":"MATCH .. RETURN i as inputs, id(i) as id"},
 			{"statement":"MATCH .. RETURN o as outputs, id(o) as id"},
 			{"statement":"MATCH .. RETURN n as nodes, id(n) as id, nt as type"},
 			{"statement":"MATCH .. RETURN .. as nodes, id(n) as id"}
 			..
 		]}
	 * 
	 * The result from the first query provides the main object while each of
	 * the following queries provides a (nested) facet of the main object.
	 * In the example above the second query provides the 'inputs' property of
	 * the main object, the second query the 'outputs' and so on ...
	 * When multiple queries use the same key for their results (as 'nodes' above)
	 * the results start to be merged after being decoded.
	 *
	 * The decoding of results from each individual query is done by default 
	 * through the NeoDecoder methods: decodeData (and decodeRow), merged (if
	 * necessary) and then added to the main object. 
	 */
	public class NestingDecoder extends NeoDecoder {

		private Path path = Path.root();
		private Map<String, BiConsumer<Path, Object>> processing = new HashMap<String, BiConsumer<Path, Object>>();
		

		public void at(String thePathPattern, BiConsumer<Path, Object> theProcessing) {
			processing.put(thePathPattern, theProcessing);
		}
		
		protected void process(Object theTarget) {
			processing
				.entrySet()
					.stream()
						.filter(e -> { return path.match(e.getKey()); })
							.forEach(e -> { e.getValue().accept(path, theTarget); });
		}
	
		// Picking up the key of the nesting structure from the name of the first
		// column is a shady proposal
		//
		public JSONArray decodeResults(JSONArray theResults) {
			//expect the first result to be the top target
			JSONArray results = new JSONArray();

			if (theResults.length() == 0)
				return results;

			//the top target itself
			JSONObject result = theResults.getJSONObject(0);
			JSONArray data = result.getJSONArray("data");
			if (data.length() == 0) {
				//no such template
				return results;
			}
			if (data.length() > 1) {
				throw new RuntimeException("Too many results");
			}

			JSONArray row = ((JSONObject)data.get(0)).getJSONArray("row");
			JSONObject target = decodeRow(row, result.getJSONArray("columns"));	

			results.put(target);

			for (int i = 1; i < theResults.length(); i++) {
				result = theResults.getJSONObject(i);
				String key = result.getJSONArray("columns").getString(0);

				path.enter(key);
				JSONArray newData = decodeHook(key,
																			 result.getJSONArray("data"),
																			 result.getJSONArray("columns"));

				JSONArray keyData = target.optJSONArray(key);
				if (keyData == null) {
					target.put(key, newData); //??!!
				}
				else {
					mergeById(keyData, newData); //??
				}

				path.exit();
			}
			return results;
		}
		
		//custom 'deep' merging
		protected JSONObject merge(JSONObject theOrig, JSONObject theNew) {
			if (theNew == null || theNew.equals(JSONObject.NULL))
				return theOrig;

			for (Iterator fieldNames = theNew.keys(); fieldNames.hasNext(); ) {
				String fieldName = (String)fieldNames.next();
				Object origValue = theOrig.opt(fieldName),
							 newValue = theNew.get(fieldName);

				path.enter(fieldName);

				if (newValue instanceof JSONArray)
					newValue = (JSONArray)newValue;

			  if (origValue == null || origValue.equals(JSONObject.NULL)) {
					//hook("merge" + path.label(), newValue == null ? new JsonObject[0] : newValue );
					process(newValue);
					theOrig.put(fieldName, newValue);	
				}
				else {
					if (newValue.equals(JSONObject.NULL)) 
						continue;
					
					//what if theNew does not have an object/array for this field??
					if (origValue instanceof JSONObject) {
							merge((JSONObject)origValue, (JSONObject)newValue);
					}
					else if (origValue instanceof JSONArray) {
						//we only merge further arrays of objects
						JSONArray origArray = (JSONArray)origValue,
											newArray = (JSONArray)newValue;
					//why the odd test in the first place???
					//	if (origArray.size() == 0 ||
					//			(origArray.get(0) instanceof JsonObject)) {
						if (origArray.length() > 0 &&
								(origArray.get(0) instanceof JSONObject)) {
							mergeById(origArray, newArray); 
						}
						else {
							//regular overwriting, same as below
							if (newArray != null) {
								theOrig.put(fieldName, newArray);
							}
						}
					}
					else {
						//this should be subject to some overwriting options
						if (newValue != null) {
							theOrig.put(fieldName, newValue);
						}
					}
				}

				path.exit();
			}

			return theOrig;
		}

		//positional merge		
		protected JSONArray merge(JSONArray theOrig, JSONArray theNew) {
		
			if (theOrig.length() != theNew.length()) {
				//should we reject ?? don't bother, checkId will fail at first
				//object merge ..
			}

			if (theNew == null || theNew.length() == 0)
				return theOrig;

			for (int i = 0; i < theNew.length(); i++) {

//System.out.println("merge array at " + path.enter("[" + i + "]"));

				Object origValue = theOrig.get(i);
				if (origValue instanceof JSONObject) {
					JSONObject newValue = theNew.getJSONObject(i);
					if (!checkId((JSONObject)origValue, newValue))  {
						log.warning("NestingDecoder:merge : attempting to merge 2 unrelated objects: " + origValue + " and " + newValue);
					}

					merge((JSONObject)origValue, newValue);
				}
				else if (origValue instanceof JSONArray) {
					merge((JSONArray)origValue, (JSONArray)theNew.get(i));
				}
//System.out.println("merge array at " + path.exit());
			}
			return theOrig;
		}

		// id based merge (faster if the results the arrays are ordered by the id
		// prop of their elements). Will only handle arrays of JsonObjects. 
		// ?? should we add elements of theNew not existing in theOrig ??
		// Switched to handling ids as Objects (and not strictly numbers), this
		// allows us to merge by whatever might be unique in the context of
		// a given query (names, ..)
		// 
		protected JSONArray mergeById(JSONArray theOrig, JSONArray theNew) {
	
			for (int j = 0; j < theOrig.length(); j++) {
				Object jid = theOrig.getJSONObject(j).get("id");
				for (int k = 0; k < theNew.length(); k++) {
					if (jid.equals(theNew.getJSONObject(k).get("id"))) {
//System.out.println("mergeById at " + path.enter("[" + jid + "]"));
						merge(theOrig.getJSONObject(j),
									theNew.getJSONObject(k));
//System.out.println("mergeById at " + path.exit());
					}
				}
			}

			return theOrig;
		}

		protected Object hook(String theName, Object... theArgs) {
			return hook(theName, null, theArgs);
		}

		protected Object hook(String theName, String theDefaultName, Object... theArgs) {
			System.out.println("hook " + theName + " with " + Arrays.toString(theArgs));
			try {
				return MethodUtils.invokeExactMethod(this, theName, theArgs);
			}
			catch (NoSuchMethodException nsmx) {
				System.out.println("no such hook " + theName);
				if (theDefaultName != null) {
					return hook(theDefaultName, null, theArgs);
				}
				return null;
			}
      catch (InvocationTargetException | IllegalAccessException ix) {
        log.warning("Invocation failed for hook " + theName + " : " + ix);
				ix.printStackTrace(System.out);
				return null;
      }
		}


		// Allows customization of the decoding process for one of the results.
		protected JSONArray decodeHook(String theResultSetName,
	                     				     JSONArray theRows,
	                          			 JSONArray theColumns) {
	    Invokable hookHandler = null;
  	  try {

//System.out.println("Looking for decoder method " + 
//	Character.toUpperCase(theResultSetName.charAt(0)) + theResultSetName.substring(1));

    	  Method m = this.getClass().getDeclaredMethod(
      	  "decode" + 
						Character.toUpperCase(theResultSetName.charAt(0)) + theResultSetName.substring(1),
					new Class[] {JSONArray.class, JSONArray.class});
  	    m.setAccessible(true);
    	  hookHandler = Invokable.from(m);
    	}
    	catch (NoSuchMethodException nsmx) {
      	//that's ok, call the default
				log.finer("Default decode data, no decode hook");
				return decodeData(theRows, theColumns);
    	}

      try {
				log.fine("Decode data with decode hook " + hookHandler);
        return (JSONArray)
									hookHandler.invoke(this, new Object[] {theRows, theColumns});
      }
      catch (InvocationTargetException | IllegalAccessException ix) {
        log.warning("Invocation failed for hook handler " + hookHandler + " : " + ix);
				ix.printStackTrace(System.out);
				return null;
      }
    }

	}

	//useful for a particular kind of nesting (i.e. a particular way of 
	//organizing the results)
	public class FacetDecoder extends NestingDecoder {
	
		private String[] facets;

		public FacetDecoder(String... theFacets) {
			this.facets = theFacets;
			Arrays.sort(this.facets);
		}
	
		protected JSONArray decodeHook(String theResultSetName,
																	 JSONArray theRows,
																   JSONArray theCols) {
			//ensure this a known facet or else default
			if (Arrays.binarySearch(this.facets, theResultSetName) < 0) {
				return super.decodeData(theRows, theCols);
			}

			//we need to aggregate the different aspects of a 
			//requirement as they come in separate objects collected in
			//an array (see the cypher query)
			JSONArray newRows = new JSONArray();
									
			for (int i = 0; i < theRows.length(); i++) {
				JSONArray row =	theRows.getJSONObject(i)
																	.getJSONArray("row");

				JSONArray rowe = row.getJSONArray(0);
				JSONObject requirement = rowe.getJSONObject(0);
				for (int j = 1; j < rowe.length(); j++) {
					merge(requirement, rowe.getJSONObject(j));
				}
										
				newRows.put(new JSONObject()
													.put("row", new JSONArray()
																						.put(requirement)));
			}				
			return super.decodeData(newRows, theCols);
		}
	
	}

	/*
	 * Concatenates responses (with the sam structure) from multiple queries 
	 */
	public class AggregateDecoder extends NeoDecoder {

		public JSONArray decodeResults(JSONArray theResults) {
			JSONArray results = new JSONArray();
			for (int i = 0; i < theResults.length(); i++) {
				JSONObject part = theResults.getJSONObject(i);
System.out.println("AggregateDecoder: " + part);

				JSONArray parts = decodeData(part.getJSONArray("data"),
																		 part.getJSONArray("columns"));
				for (int j = 0; j < parts.length(); j++) {
					results.put(parts.getJSONObject(j));
				}
			}
			return results;
		}
	}

	/*
	 * withRequirements and withCapabilities are strictly specific to node types.
	 */
	public class TypeAction extends NeoAction<Type>
													implements Catalog.TypeAction {
		
		private String tname;

		private TypeAction(String theName) {
			super("MATCH (t:Type) " +
						"WHERE t.name='" + theName + "' " +
						"RETURN t, id(t) as id",
						Type.class,
						new FacetDecoder("requirements", "capabilities"));
					 
			this.tname = theName;
		}

		public TypeAction withHierarchy() {
			return (TypeAction)
						 with("MATCH (st:Type)<-[:DERIVED_FROM*1..5]-(t:Type) " +
									"WHERE t.name='" + this.tname + "' " +
									"RETURN st as hierarchy, id(t) as id");
		}
	
		public TypeAction withRequirements() {
			return (TypeAction)
				with("MATCH (t:Type)-[:DERIVED_FROM*0..5]->(st:Type)<-[:REQUIREMENT_OF]-(r:Requirement)-[rr]->(tgt) " +
					"WHERE t.name='" + this.tname + "' " +
					"WITH st,r,collect ({name: r.name, occurrences: r.occurrences, " + 
					"node: (CASE WHEN type(rr) = 'REQUIRES' THEN tgt ELSE NULL END), "+
					"capability: (CASE WHEN type(rr) = 'CAPABILITY' THEN tgt ELSE NULL END), "+ 
					"relationship: (CASE WHEN type(rr) = 'RELATIONSHIP' THEN tgt ELSE NULL END) " +
					"}) as tgts " +
					 //"RETURN tgts as requirements, id(st) as id",
					 "RETURN tgts as requirements");
		}
		
		public TypeAction withCapabilities() {
			return (TypeAction)
				with("MATCH (t:Type)-[:DERIVED_FROM*0..5]->(st:Type)<-[:CAPABILITY_OF]-(c:Capability)-[:FEATURES]->(tgt:Type) " +
					"WHERE t.name='" + this.tname + "' " +
					"WITH st,c,collect ({name: c.name, occurrences: c.occurrences, " +
								"validSourceTypes: c.validSourceTypes, type: tgt}) as caps " + 
					"RETURN caps as capabilities");
		}
	}

	public class TemplateAction extends NeoAction<Catalog.Template>
															implements Catalog.TemplateAction {

		private String tid;

		private TemplateAction(String theId) {
			super("MATCH (t:Template) " +
						"WHERE id(t)=" + theId + " " +
						"RETURN t, id(t) as id",
						Template.class,
						new NestingDecoder());
			this.tid = theId;
		}

		public TemplateAction withInputs() {
			return (TemplateAction)
						 with("MATCH (t:Template)<-[:INPUT_OF]-(i:Input) " +
									"WHERE id(t)=" + this.tid + " " +
									"RETURN i as inputs, id(i) as id");
		}
		
		public TemplateAction withOutputs() {
			return (TemplateAction)
						 with("MATCH (t:Template)<-[:OUTPUT_OF]-(o:Output) " +
									"WHERE id(t)=" + this.tid + " " +
									"RETURN o as outputs, id(o) as id");
		}

		public TemplateAction withNodes() {
			return (TemplateAction)
				with("MATCH (t:Template)<-[:NODE_OF]-(n:Node)-[:OF_TYPE]->(nt:Type) " +
						 "WHERE id(t)=" + this.tid + " " +
						 "RETURN n as nodes, id(n) as id, nt as type");
		}
	
		/* I still did not find a way to construct an json object literal out
		 * of ALL its properties and its id without explicitly listing the 
		 * properties ..
		 */
		public TemplateAction withNodeProperties() {
			return (TemplateAction)
				with("MATCH (t:Template)<-[:NODE_OF]-(n:Node)-[:OF_TYPE]->(nt:Type)-[:DERIVED_FROM*0..5]->(st:Type)<-[:PROPERTY_OF]-(p:Property) " +
						"WHERE id(t)=" + this.tid + " " +
						"WITH n,nt," +
						"collect(DISTINCT {name:p.name,description:p.description,type:p.type,required:p.required,default:p.default,constraints:p.constraints,id:id(p),collect:p.name}) as props," +
						//"collect(DISTINCT p) as props," +
						"collect(id(p)) as propstest " +
						"RETURN {name:n.name,type:nt,properties: (CASE WHEN length(propstest) = 0 THEN NULL ELSE props END)} as nodes, id(n) as id");
		}
	
		/*
		 *
		 */	
		public TemplateAction withNodeRequirements() {
			return (TemplateAction)
				//first collect the requirements from the type specification
				//they'll have a capability type and maybe a target node type and
				//a relationship
				with(
					"MATCH (t:Template)<-[:NODE_OF]-(n)-[:OF_TYPE]->(nt:Type)-[:DERIVED_FROM*0..5]->(st:Type)<-[:REQUIREMENT_OF]-(r:Requirement)-[:CAPABILITY]->(rc) " +
					"WHERE id(t)=" + this.tid + " " +
					"WITH n, r, rc " +
					"OPTIONAL MATCH (r)-[:REQUIRES]->(rn), (r)-[:RELATIONSHIP]->(rr) " +
					"WITH n, collect(DISTINCT {id:r.name,name:r.name,capability:rc,target:rn,relationship:rr}) as reqs " +
					"RETURN {name:n.name,requirements:reqs} as nodes, id(n) as id").
			
				//requirement assignments from the node template will provide a target
				//node or redefined target node type and eventually refine to a
				//capability and relationship
				with(
					"MATCH (t:Template)<-[:NODE_OF]-(n)<-[:REQUIREMENT_OF]-(r:Requirement)-[:REQUIRES]->(rn) " +
					"WHERE id(t)=" + this.tid + " " +
					"WITH n, r, rn " +
					"OPTIONAL MATCH (r)-[:CAPABILITY]->(rc), (r)-[:RELATIONSIP]->(rr) " +
					"WITH n, collect({id:r.name,name:r.name,target:rn,target_type:LAST(labels(rn)),capability:rc,relationship:rr}) as reqs " +
					"RETURN {name:n.name,requirements:reqs} as nodes, id(n) as id");
		}
		
		/* Getting the input info for each valuation (the 'natural' structure)
		 * again forces me to enumerate explcitly all valuation properties
		 */
		public TemplateAction withNodePropertiesAssignments() {
			((NestingDecoder)decoder())
				.at("/nodes/properties/assignment",
						(l,t) -> { 
							JSONObject assignment = (JSONObject)t;
							//the query below provides the provides the property type within each assignment
							String		 type = (String)assignment.remove("type");
							if (type == null) 
								return;
							try {
								Object value = assignment.get("value");
								if (type.equals("list"))
									assignment.put("value", new JSONArray(value.toString()));
								else if (type.equals("map"))
									assignment.put("value", new JSONObject(value.toString()));
							}
							catch(Exception x) {
								System.out.println("Assignment processing exception for " + assignment.getString("value"));
								x.printStackTrace();
							} 
						}
					);	
			return (TemplateAction)
				with("MATCH (t:Template)<-[:NODE_OF]-(n:Node)-[:OF_TYPE]->(nt:Type)-[:DERIVED_FROM*0..5]->(st:Type)<-[:PROPERTY_OF]-(p:Property) " +
						"WHERE id(t)=" + this.tid + " " +
						"WITH collect(DISTINCT id(n)) as nodes, " +
						"collect(DISTINCT id(p)) as props " +
						"MATCH (a:Assignment)-[:OF_NODE_PROPERTY]->(p:Property),(a)-[:OF_TEMPLATE]->(n) " +
						"OPTIONAL MATCH (i)<-[:GET_INPUT]-(a) " +
						"WHERE id(p) IN props AND id(n) in nodes " +
						"WITH n," +
						"collect({id:id(p),assignment:{value:a.value,input:i,type:p.type}}) as propvals," +
						"collect(id(a)) as valstest " +
						"RETURN {name:n.name," +
						"properties: (CASE WHEN length(valstest) = 0 THEN NULL ELSE propvals END)} "+
						"as nodes, id(n) as id");
		}

		public TemplateAction withNodeCapabilities() {
			return (TemplateAction)
				with(
					"MATCH (t:Template)<-[:NODE_OF]-(n:Node)-[:OF_TYPE]->(nt:Type:Node)-[:DERIVED_FROM*0..5]->(st:Type:Node)<-[:CAPABILITY_OF]-(c:Capability)-[:FEATURES]->(e) " +
					"WHERE id(t)=" + this.tid + " " +
					"WITH n, collect({id:c.name,name:c.name,target:e,target_type:LAST(labels(e))}) as caps " +
					"RETURN {id:id(n),name:n.name,capabilities:caps} as nodes, id(n) as id");
			/*
			return (TemplateAction)
				with("MATCH (t:Template)<-[:NODE_OF]-(n)<-[:CAPABILITY_OF]-(c:Capability)-[:FEATURES]->(e) " +
						"WHERE id(t)=" + this.tid + " " +
						"WITH n, collect({name:c.name,target:e,target_type:LAST(labels(e))}) as caps " +
						"RETURN {name:n.name,capabilities:caps} as nodes, id(n) as id");
			*/
		}

		public TemplateAction withNodeCapabilityProperties() {
			return (TemplateAction)
		 with("MATCH (t:Template)<-[:NODE_OF]-(n:Node)-[:OF_TYPE]->(nt:Type:Node)-[:DERIVED_FROM*0..5]->(st:Type:Node)<-[:CAPABILITY_OF]-(c:Capability) " +
		"WHERE id(t)=" + this.tid + " " +
		"WITH n, c " +
		"OPTIONAL MATCH (c)-[:FEATURES]->(ct:Type:Capability)-[:DERIVED_FROM*0..5]->(cst:Type:Capability)<-[:PROPERTY_OF]-(p:Property) " +
	  "WITH n,c,collect({id:id(p),name:p.name,type:p.type,default:p.default,required:p.required}) as capprops,collect(id(p)) as cappropstest " +
	  "WITH n, collect ({id:c.name, name:c.name, properties: (CASE WHEN length(cappropstest) = 0 THEN NULL ELSE capprops END)}) as caps " +
  	"RETURN {id: id(n), name:n.name, capabilities: caps} as nodes, id(n) as id");
		}

		public TemplateAction withNodeCapabilityPropertyAssignments() {
			return (TemplateAction)
		 with("MATCH (t:Template)<-[:NODE_OF]-(n:Node)-[:OF_TYPE]->(nt:Type:Node)-[:DERIVED_FROM*0..5]->(st:Type:Node)<-[:CAPABILITY_OF]-(c:Capability) " +
		"WHERE id(t)=" + this.tid + " " +
		"WITH n, c " +
		"OPTIONAL MATCH (c)-[:FEATURES]->(ct:Type:Capability)-[:DERIVED_FROM*0..5]->(cst:Type:Capability)<-[:PROPERTY_OF]-(p:Property), " +
				"(p)<-[:OF_CAPABILITY_PROPERTY]-(a:Assignment)-[:OF_CAPABILITY]-(c) " +
	  "WITH n,c,collect({id:id(p),name:p.name,type:p.type,assignment:a}) as capprops,collect(id(p)) as cappropstest " +
	  "WITH n, collect ({id:c.name, name:c.name, properties: (CASE WHEN length(cappropstest) = 0 THEN NULL ELSE capprops END)}) as caps " +
  	"RETURN {id: id(n), name:n.name, capabilities: caps} as nodes, id(n) as id");
		}
		
		public TemplateAction withPolicies() {
			return (TemplateAction)
				with("MATCH (t:Template)<-[:POLICY_OF]-(p:Policy)-[:OF_TYPE]->(pt:Type:Policy) " +
						 "WHERE id(t)=" + this.tid + " " +
						 "RETURN p as policies, id(p) as id, pt as type");
		}

		public TemplateAction withPolicyProperties() {
			return (TemplateAction)
				with("MATCH (t:Template)<-[:POLICY_OF]-(p:Policy)-[:OF_TYPE]->(pt:Type)-[:DERIVED_FROM*0..5]->(st:Type)<-[:PROPERTY_OF]-(pp:Property) " +
						"WHERE id(t)=" + this.tid + " " +
						"WITH p, pt," +
						"collect({id:id(pp),name:pp.name,description:pp.description,type:pp.type,required:pp.required,default:pp.default,constraints:pp.constraints}) as props," +
						"collect(id(pp)) as propstest " +
						"RETURN {name:p.name,type:pt,properties: (CASE WHEN length(propstest) = 0 THEN NULL ELSE props END)} as policies, id(p) as id");
		}
		
		public TemplateAction withPolicyPropertiesAssignments() {
			return (TemplateAction)
				with("MATCH (t:Template)<-[:POLICY_OF]-(p:Policy)-[:OF_TYPE]->(pt:Type)-[:DERIVED_FROM*0..5]->(st:Type)<-[:PROPERTY_OF]-(pp:Property) " +
						"WHERE id(t)=" + this.tid + " " +
						"WITH collect(DISTINCT id(p)) as policies, " +
						"collect(DISTINCT id(pp)) as props " +
						"MATCH (a:Assignment)-[:OF_POLICY_PROPERTY]->(pp:Property), (a)-[:OF_TEMPLATE]->(p) " +
						"OPTIONAL MATCH (i)<-[:GET_INPUT]-(a) " +
						"WHERE id(pp) IN props AND id(p) in policies " +
						"WITH p," +
						"collect({id:id(pp),assignment:{value:a.value,input:i}}) as propvals," +
						"collect(id(a)) as valstest " +
						"RETURN {name:p.name," +
						"properties: (CASE WHEN length(valstest) = 0 THEN NULL ELSE propvals END)} "+
						"as policies, id(p) as id");
		}

		@Override
		public Future<Catalog.Template> execute() {
			System.out.println("TemplateAction::execute");
			return super.execute();
		}
	}


	/** */
	public class FolderAction	extends NeoAction<Catalog.Folder>	
														implements Catalog.FolderAction {

		private String selector;

    private FolderAction(String theSelector) {
			super("MATCH (f:Folder:"+ NeoCatalog.this.namespace + ") " +
						"WHERE " + theSelector + " " +
						"RETURN f, id(f) as id, LAST(labels(f)) as label, toString(f.itemId) as itemId " +
						"LIMIT 1",
						Folder.class,
						new NestingDecoder());
			this.selector = theSelector;
		}
		
		public FolderAction withAnnotations() {
			return withAnnotations(null);
		}

		public FolderAction withAnnotations(String theAnnotationLabel) {
			with("MATCH (f:Folder:"+ NeoCatalog.this.namespace + ")<-[:ANNOTATION_OF]-(a:Annotation:" + NeoCatalog.this.namespace + 
					 ((theAnnotationLabel == null) ? ")" : (":" + theAnnotationLabel + ")")) + " " +
					 "WHERE " + this.selector + " " +
					 "RETURN a as annotations, id(a) as id, LAST(labels(a)) as label");
			return this;
		}

	  public FolderAction withItems() {
			with("MATCH (f:Folder:"+ NeoCatalog.this.namespace + ")<-[PART_OF]-(c:CatalogItem) " +
					 "WHERE " + this.selector + " " +
					 "RETURN c as items, id(c) as id, LAST(labels(c)) as label, toString(c.itemId) as itemId");
			return this;
		}

		public FolderAction withItemAnnotations() {
			return withItemAnnotations(null);
		}

		public FolderAction withItemAnnotations(String theAnnotationLabel) {
			with("MATCH (f:Folder:"+ NeoCatalog.this.namespace + ")<-[PART_OF]-(c:CatalogItem)<-[:ANNOTATION_OF]-(a:Annotation:"+ NeoCatalog.this.namespace +
					 ((theAnnotationLabel == null) ? ")" : (":" + theAnnotationLabel + ")")) + " " +
					 "WHERE " + this.selector + " " +
			//
			//		 ((theAnnotationLabel == null) ?
			//		 		"WITH c,collect({id:id(a),label:LAST(labels(a))}) as ans " :
			//		 		"WITH c,collect(a) as ans ") +
			//
					 "WITH c, collect({properties:a,label:LAST(labels(a)),id:id(a)}) as ans " + 
					// "RETURN {name:c.name, id:id(c), annotations:ans} as items, id(c) as id");
					 "RETURN c as items, id(c) as id, ans as annotations");
			return this;
		}
	
		public FolderAction withItemModels() {
			with("MATCH (f:Folder:"+ NeoCatalog.this.namespace + ")<-[PART_OF]-(c:CatalogItem)<-[:MODEL_OF]-(t:TOSCA:Template) " +
					 "WHERE " + this.selector + " " +
					 "WITH c,collect(t) as mdls " + 
					// "RETURN {name:c.name, id:id(c), models:mdls} as items, id(c) as id");
					 "RETURN c as items, id(c) as id, mdls as models");
			return this;
		}

		public FolderAction withParts() {
			with("MATCH (f:Folder:"+ NeoCatalog.this.namespace + ")<-[PART_OF]-(ff:Folder:" + NeoCatalog.this.namespace + ") " +
					 "WHERE " + this.selector + " " +
					 "RETURN ff as parts, id(ff) as id, LAST(labels(ff)) as label, toString(ff.itemId) as itemId");
			return this;
		}
		
		public FolderAction withPartAnnotations() {
			return withPartAnnotations(null);
		}

		public FolderAction withPartAnnotations(String theAnnotationLabel) {
			with("MATCH (f:Folder:"+ NeoCatalog.this.namespace + ")<-[PART_OF]-(p:Folder:" + NeoCatalog.this.namespace + ")<-[:ANNOTATION_OF]-(a:Annotation:"+ NeoCatalog.this.namespace +
					 ((theAnnotationLabel == null) ? ")" : (":" + theAnnotationLabel + ")")) + " " +
					 "WHERE " + this.selector + " " +
//
//					 ((theAnnotationLabel == null) ?
//					 		"WITH p,collect({id:id(a),label:LAST(labels(a))}) as ans " :
//					 		"WITH p,collect(a) as ans ") + 
//
					 "WITH p, collect({properties:a,label:LAST(labels(a)),id:id(a)}) as ans " + 
					 "RETURN {name:p.name, id:id(p), annotations:ans} as parts, id(p) as id");
			return this;
		}

	}

	public class ItemAction extends NeoAction<Catalog.Item>
													implements Catalog.ItemAction<Catalog.Item> {

		private String filter;

		private ItemAction(String theFilter) {
			super(null, Item.class, new NestingDecoder());
			this.filter = theFilter;
			with("MATCH (x:CatalogItem) " +
					 "WHERE " + this.filter + " " +
					 "RETURN x, id(x) as id, LAST(labels(x)) as label, toString(x.itemId) as itemId");
		}

		public ItemAction withModels() {
			return (ItemAction)
				with("MATCH (x:CatalogItem)<-[:MODEL_OF]-(t:TOSCA:Template) " +
							"WHERE " + this.filter + " " +
							"RETURN t as models, id(t) as id");
		}

		/* find a a way to add the annotations label */
		public ItemAction withAnnotations() {
			return (ItemAction)
				with("MATCH (x:CatalogItem)<-[:ANNOTATION_OF]-(a:Annotation:" + NeoCatalog.this.namespace + ") " +
							"WHERE " + this.filter + " " +
							"RETURN a as annotations, id(a) as id, LAST(labels(a)) as label");
		}

	}
}


