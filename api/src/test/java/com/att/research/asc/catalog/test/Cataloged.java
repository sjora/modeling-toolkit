package com.att.research.asc.catalog.test;

import java.net.URI;

import org.json.JSONObject;

import com.att.research.asc.catalog.commons.Future;

import com.att.research.asc.catalog.Catalog;
import com.att.research.asc.catalog.Catalog.Type;
import com.att.research.asc.catalog.Catalog.Element;
import com.att.research.asc.catalog.Catalog.Item;
import com.att.research.asc.catalog.Catalog.Mixels;
import com.att.research.asc.catalog.Catalog.Folder;
import com.att.research.asc.catalog.Catalog.Folders;
import com.att.research.asc.catalog.Catalog.Template;
import com.att.research.asc.catalog.Catalog.Templates;
import com.att.research.asc.catalog.neo.NeoCatalog;
import com.att.research.asc.catalog.asdc.ASDCCatalog;
import com.att.research.asc.catalog.cognita.CognitaCatalog;


public class Cataloged {

	public static void main(String[] theArgs) throws Exception {

		URI argUri = new URI(theArgs[0]),
				catalogUri = null;

		String scheme = argUri.getScheme();
		if (argUri.getFragment() != null) {
			catalogUri = new URI(argUri.getSchemeSpecificPart() + "#" + argUri.getFragment());
		}
		else {
			catalogUri = new URI(argUri.getSchemeSpecificPart());
		}

		Catalog cat;
		if ("neo".equals(scheme)) {
			cat = new NeoCatalog(catalogUri);
		}
		else if ("asdc".equals(scheme)) {
			cat = new ASDCCatalog(catalogUri);
		}
		else if ("cognita".equals(scheme)) {
			cat = new CognitaCatalog();
//http://cognita-dev1-vm01-core.eastus.cloudapp.azure.com:8000/ccds
			((CognitaCatalog)cat).setCommonDataUri(catalogUri);
			((CognitaCatalog)cat).setNexusUri(
					new URI("http://cognita-nexus01.eastus.cloudapp.azure.com:8081/repository/repo_cognita_model_maven/"));
		}
		else {
			throw new IllegalArgumentException("Unknown catalog type " + scheme);
		}

		if (theArgs[1].equals("template")) {
			System.out.println(
				cat.template(theArgs[2])
					.withInputs()
					.withOutputs()
					.withNodes()
					.withNodeProperties()
					.withNodePropertiesAssignments()
					.withNodeRequirements()
					.withNodeCapabilities()
					.withNodeCapabilityProperties()
					.withNodeCapabilityPropertyAssignments()
					.withPolicies()
					.withPolicyProperties()
					.withPolicyPropertiesAssignments()
					.execute()
					.waitForResult()
					.data()
					.toString(2));
		}
		else if (theArgs[1].equals("roots")) {
			Folders folders = 
				cat.roots()
					.waitForResult();
			for (Folder f: folders)
				System.out.println(
						f.data()
							.toString(2));
		}
		else if (theArgs[1].equals("type")) {
			Type type = 
					cat.type("", theArgs[2])
								.withHierarchy()
								.withRequirements()
								.withCapabilities()
								.execute()
								.waitForResult();
			System.out.println(type.data().toString(2));
		}
		else if (theArgs[1].equals("folder")) {
			Folder f =	
				cat.folder(theArgs[2])
					.withAnnotations()
					.withItems()
					.withItemAnnotations()
					.withItemModels()
					.withParts()
					.withPartAnnotations()
					.execute()
					.waitForResult();

			System.out.println(f.data().toString(2));
			for (Item i: f.items().waitForResult())
				System.out.println("* " + i.data());
		}
		else if (theArgs[1].equals("item")) {
			Item item = //(Item)
				cat.item(theArgs[2])
					.withAnnotations()
					//.withModels()
					.execute()
					.waitForResult();
			System.out.println(
					item
						.data()
						.toString(2));

			Future<Templates> futureTemplates = item.models();
			Templates tmpls = futureTemplates.waitForResult();
			
			//Templates tmpls = item.models().waitForResult();
			
			for (Template t: tmpls)
				System.out.println("* " + t.data());
		}
		else if (theArgs[1].equals("lookup")) {
			Mixels mixels = //(Item)
				cat.lookup(new JSONObject(theArgs[2]))
					.waitForResult();
			for (Element e: mixels)
				System.out.println(e.data().toString(2));			
		}
	}
}

