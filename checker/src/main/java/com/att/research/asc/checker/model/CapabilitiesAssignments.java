package com.att.research.asc.checker.model;


/**
 * Collection of template specific capability assignment (spec section 3.7.2) 
 */
public interface CapabilitiesAssignments extends TOSCAMap<CapabilityAssignment> {

}

