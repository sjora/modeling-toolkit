package com.att.research.asc.checker.model;


/**
 * Same as property definition
 */
public interface Input extends TOSCAObject<Input> {

	public String name();

	public String type();

	public String description();

	public default Object _default() {
		return info().get("default");
	}

	public boolean required();

	public Status status();

	public default Constraints constraints() {
		return (Constraints)proxy("constraints", Constraints.class);
	}

	public default EntrySchema entry_schema() {
		return (EntrySchema)proxy("entry_schema", EntrySchema.class);
	}

}
