package com.att.research.asc.checker.model;

import java.util.List;


/**
 * Spec section 3.8
 */
public interface Substitution extends TOSCAObject<Substitution> {

	public String node_type();

	public default Mappings capabilities() {
		return (Mappings)proxy("capabilities", Mappings.class);
	}

	public default Mappings requirements() {
		return (Mappings)proxy("requirements", Mappings.class);
	}

	/** */
	public interface Mappings extends TOSCAMap<Mapping> {
	}

	/** */
	public interface Mapping extends TOSCAObject<Mapping> {

		/** to promote the key */
		public String name();

		/**
		 */
		public default String target() {
			return name();
		}

		/** */
		public default List<String> mapping() {
			return (List<String>)info().values().iterator().next();			
		}
	}
}
