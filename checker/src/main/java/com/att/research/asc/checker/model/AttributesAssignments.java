package com.att.research.asc.checker.model;


/*
 * A simple representation of the attribute value assignments, spec section 3.5.11
 */
public interface AttributesAssignments extends TOSCAObject<AttributesAssignments> {

}
