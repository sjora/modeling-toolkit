package com.att.research.asc.checker.model;

import java.util.List;

/*
 * Type specific capability definition, spec section 3.7.1
 */
public interface CapabilityAssignment extends TOSCAObject<CapabilityAssignment> {

	/** */
	public String name();

	/**
	 * An optional list of property definitions for the Capability definition.
	 */
	public default PropertiesAssignments properties() {
		return (PropertiesAssignments)proxy("properties", PropertiesAssignments.class);
	}

	/**
	 * An optional list of attribute definitions for the Capability definition.
	 */
	public default AttributesAssignments attributes() {
		return (AttributesAssignments)proxy("attributes", AttributesAssignments.class);
	}

}
