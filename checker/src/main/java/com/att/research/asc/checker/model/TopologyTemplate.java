package com.att.research.asc.checker.model;


public interface TopologyTemplate extends TOSCAObject<TopologyTemplate> {

	public String description();

	public default Inputs inputs() {
		return (Inputs)proxy("inputs", Inputs.class);
	}
	
	public default Outputs outputs() {
		return (Outputs)proxy("Outputs", Outputs.class);
	}

	public default NodeTemplates node_templates() {
		return (NodeTemplates)proxy("node_templates", NodeTemplates.class);
	}
	
	public default RelationshipTemplates relationship_templates() {
		return (RelationshipTemplates)proxy("relationship_templates", RelationshipTemplates.class);
	}

	public default Groups groups() {
		return (Groups)proxy("groups", Groups.class);
	}

	public default Substitution substitution_mappings() {
		return (Substitution)proxy("substitution_mappings", Substitution.class);
	}

}
