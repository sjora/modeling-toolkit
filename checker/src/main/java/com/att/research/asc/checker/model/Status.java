package com.att.research.asc.checker.model;


public enum Status {

	supported,
	unsupported,
	experimental,
	deprecated

}
