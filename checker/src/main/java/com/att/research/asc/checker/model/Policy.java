package com.att.research.asc.checker.model;

import java.util.List;

/**
 * Policy type definition, spec section 3.7.6
 */
public interface Policy extends TOSCAObject<Policy> {

	public String type();
	
	public String description();
	
	public String version();
	
	public default Metadata metadata() {
		return (Metadata)proxy("metadata", Metadata.class);
	}
	
	public List<String> targets();

	public default PropertiesAssignments properties() {
		return (PropertiesAssignments)proxy("properties", PropertiesAssignments.class);
	}

	public default PolicyType.Triggers triggers() {
		return (PolicyType.Triggers)proxy("triggers", PolicyType.Triggers.class);
	}

}
