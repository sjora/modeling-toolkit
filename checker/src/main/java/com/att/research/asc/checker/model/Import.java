package com.att.research.asc.checker.model;


/**
 * Import defintion, see section 
 */
public interface Import extends TOSCAObject<Import> {

	public String name();

	public String file();

	public String repository();
	
	public String namespace_uri();
	
	public String namespace_prefix();

}
