package com.att.research.asc.checker.model;

/**
 */
public interface Attribute extends TOSCAObject<Attribute> {

	public String name();

	public String type();
	
	public String description();
	
	public default Object _default() {
		return info().get("default");
	}

	public Boolean required();
	
	public String status();

	public String entry_schema();
}
