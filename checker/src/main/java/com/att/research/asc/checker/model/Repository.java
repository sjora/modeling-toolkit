package com.att.research.asc.checker.model;

import java.util.Map;


/*
 * Repository definition (spec section 3.6.3)
 */
public interface Repository extends TOSCAObject<Repository> {

	/** */
	public String name();

	/** */
	public String description();

	/** */
	public String url();
	
	/** */
	public default Credential credential() {
		return (Credential)proxy("credential", Credential.class);
	}

	/** */
	public interface Credential extends TOSCAObject<Credential> {
	
		/** */
		public String protocol();
		
		/** */
		public String token_type();
		
		/** */
		public String token();
		
		/** */
		public String user();
		
		/** */
		public Map<String,String> keys();
	}
}
