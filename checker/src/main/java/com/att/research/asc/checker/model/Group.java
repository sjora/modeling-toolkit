package com.att.research.asc.checker.model;

import java.util.List;

/**
 * Spec section 3.7.5
 */
public interface Group extends TOSCAObject<Group> {

	public String name();

	public String type();
	
	public String description();
	
	public default Metadata metadata() {
		return (Metadata)proxy("metadata", Metadata.class);
	}

	public default PropertiesAssignments properties() {
		return (PropertiesAssignments)proxy("properties", PropertiesAssignments.class);
	}
	
	public default TemplateInterfaces interfaces() {
		return (TemplateInterfaces)proxy("interfaces", TemplateInterfaces.class);
	}

	public List<String> members();

}
