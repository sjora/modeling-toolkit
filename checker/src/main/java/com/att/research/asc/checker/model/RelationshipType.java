package com.att.research.asc.checker.model;

import java.util.List;

/**
 * Relationship type definition, spec section 3.6.10
 */
public interface RelationshipType extends TOSCAObject<RelationshipType> {

	public String name();

	public String derived_from();
	
	public String description();
	
	public String version();
	
	public default Metadata metadata() {
		return (Metadata)proxy("metadata", Metadata.class);
	}

	public default Properties properties() {
		return (Properties)proxy("properties", Properties.class);
	}

	public default Attributes attributes() {
		return (Attributes)proxy("attributes", Attributes.class);
	}

	public default TypeInterfaces interfaces() {
		return (TypeInterfaces)proxy("interfaces", TypeInterfaces.class);
	}

	public List<String> valid_target_types();
	
}
