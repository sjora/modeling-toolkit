package com.att.research.asc.checker.model;

import java.util.List;

/**
 * Artifact type definition, spec section 3.6.4
 */
public interface ArtifactType extends TOSCAObject<ArtifactType> {

	public String name();

	public String derived_from();
	
	public String description();
	
	public String version();
	
	public default Metadata metadata() {
		return (Metadata)proxy("metadata", Metadata.class);
	}
	
	public String mime_type();
	
	public List<String> file_ext();

	public default Properties properties() {
		return (Properties)proxy("properties", Properties.class);
	}

}
