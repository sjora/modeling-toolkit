package com.att.research.asc.checker.model;

/**
 * Data type definition, spec section 3.6.6
 */
public interface DataType extends TOSCAObject<DataType> {

	public String name();

	public String derived_from();
	
	public String description();
	
	public String version();
	
	public default Metadata metadata() {
		return (Metadata)proxy("metadata", Metadata.class);
	}

	public default Properties properties() {
		return (Properties)proxy("properties", Properties.class);
	}

	public default Constraints constraints() {
		return (Constraints)proxy("constraints", Constraints.class);
	}

}
