package com.att.research.asc.checker.model;


/**
 * Spec section 3.5.12
 */
public interface Parameter extends TOSCAObject<Parameter> {

	public String name();

	public String type();

	public String description();
	
	public Object value();

	public default Object _default() {
		return info().get("default");
	}

	public boolean required();

	public Status status();

	public default Constraints constraints() {
		return (Constraints)proxy("constraints", Constraints.class);
	}

	public default EntrySchema entry_schema() {
		return (EntrySchema)proxy("entry_schema", EntrySchema.class);
	}

}
