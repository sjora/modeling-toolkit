package com.att.research.asc.checker.model;

import java.util.List;

/**
 * Policy type definition, spec section 3.6.12
 */
public interface PolicyType extends TOSCAObject<PolicyType> {

	public String name();

	public String derived_from();
	
	public String description();
	
	public String version();
	
	public default Metadata metadata() {
		return (Metadata)proxy("metadata", Metadata.class);
	}
	
	public List<String> targets();

	public default Properties properties() {
		return (Properties)proxy("properties", Properties.class);
	}

	public default Triggers triggers() {
		return (Triggers)proxy("triggers", Triggers.class);
	}

	public interface Triggers extends TOSCAMap<Trigger> {

	}

	public interface Trigger extends TOSCAObject<Trigger> {

		public String description();
		
		public String event_type();
		
		public String schedule();
	
		public default Constraints constraint() {
			return (Constraints)proxy("constraint", Constraints.class);
		}

		public default Constraints condition() {
			return (Constraints)proxy("condition", Constraints.class);
		}

		public int period();
		
		public int evaluations();

		public String method();
		
		public String action();

		//target_filter	
		public default EventFilter target_filter() {
			return (EventFilter)proxy("target_filter", EventFilter.class);
		}

	}
	
	public interface EventFilter extends TOSCAObject<EventFilter> {

		public String node();

		public String requirement();

		public String capability();

	}

}
