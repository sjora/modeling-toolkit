package com.att.research.asc.checker.model;

/*
 * Requirement assignment as it appears in node templates. See spec section 3.7.2
 */
public interface RequirementAssignment extends TOSCAObject<RequirementAssignment> {

	public String name();

	/**
	 * Provide the name of either a:
	 * 	Capability definition within a target node template that can fulfill the requirement.
	 * 	Capability Type that the provider will use to select a type-compatible target node template to fulfill the requirement at runtime.
	 */
	public String capability();

	/**
	 */
	public String node();
	
	/** */
	public default NodeFilter node_filter() {
		return (NodeFilter)proxy("node_filter", NodeFilter.class);
	}

	/** */
	public default RelationshipAssignment relationship() {
		return (RelationshipAssignment)proxy("relationship", RelationshipAssignment.class);
	}

	public interface RelationshipAssignment extends TOSCAObject<RelationshipAssignment> {

		public String type();

		public default PropertiesAssignments properties() {
			return (PropertiesAssignments)proxy("properties", PropertiesAssignments.class);
		}

		public default TemplateInterfaces interfaces() {
			return (TemplateInterfaces)proxy("interfaces", TemplateInterfaces.class);
		}
	}
}
