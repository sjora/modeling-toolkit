package com.att.research.asc.checker;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.MissingResourceException;

/*
 * This class should be generated programatically based on the keys available in messages.properties
 */
public class Messages {

	private ResourceBundle	messages;
	
	public Messages() {
		try {
			this.messages = ResourceBundle.getBundle("com/att/research/asc/checker/messages");
		}
		catch (MissingResourceException mrx) {
			throw new RuntimeException("", mrx);
		}

		//check that the Message enum is in sync with the resource bundle
	}

	public String format(Message theMessage, Object[] theArgs) {
		String message = this.messages.getString(theMessage.name());
		if (message == null)
			throw new RuntimeException("Un-available message: " + theMessage);
 
		return MessageFormat.format(message, theArgs);
	}

	public enum Message {
		EMPTY_TEMPLATE,
		INVALID_CONSTRUCT_REFERENCE,
		INVALID_TYPE_REFERENCE,
		INVALID_TEMPLATE_REFERENCE,
		INVALID_INTERFACE_REFERENCE,
		INVALID_FACET_REFERENCE,
		INCOMPATIBLE_REQUIREMENT_TARGET
	}
}
