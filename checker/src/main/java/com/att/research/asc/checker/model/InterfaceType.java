package com.att.research.asc.checker.model;

import java.util.Map;

import com.google.common.collect.Maps;

/**
 * Interface type definition, spec section 3.6.5
 */
public interface InterfaceType extends TOSCAObject<InterfaceType> {

	public String name();

	public String derived_from();
	
	public String description();
	
	public String version();
	
	public default Metadata metadata() {
		return (Metadata)proxy("metadata", Metadata.class);
	}

	public default Properties inputs() {
		return (Properties)proxy("inputs", Properties.class);
	}

	/**
   * The set of operations, made up by all keys but the ones above ..
   */
	public default Operations operations() {
		return (Operations)
			TOSCAProxy.record(info(),
												info ->	TOSCAProxy.buildMap(null,
																										Maps.filterKeys((Map)info,
													 																					key -> !("derived_from".equals(key) ||
																											  						 				 "description".equals(key) ||
																																						 "version".equals(key) ||
																																						 "metadata".equals(key) ||
																																						 "inputs".equals(key))),
															 											Operations.class));
	
	}
	

}
