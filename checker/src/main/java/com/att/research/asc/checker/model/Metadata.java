package com.att.research.asc.checker.model;

/*
 * No predefined entries here, so just use the java.util.Map interface get, i.e. get("some_entry_name")
 */
public interface Metadata extends TOSCAObject<Metadata> {

}
