package com.att.research.asc.checker.model;

import java.util.List;

/*
 */
public interface Range extends List<Object> {

	public default Object lower() {
		return get(0);
	}

	public default Object upper() {
		return get(1);
	}

	public default boolean isUnbounded() {
		return "UNBOUNDED".equals(upper());
	}
}
