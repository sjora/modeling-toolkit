package com.att.research.asc.checker.model;

/**
 * Used in data type, property, input and so on definitions, see spec section  
 */
public interface EntrySchema extends TOSCAObject<EntrySchema> {

	public String type();

	public String description();

	public default Constraints constraints() {
		return (Constraints)proxy("constraints", Constraints.class);
	}
}
