package com.att.research.asc.checker.model;

import java.util.List;

/*
 * Type specific capability definition, spec section 3.6.2
 */
public interface Capability extends TOSCAObject<Capability> {

	/**
	 * The required name of the Capability Type the capability definition is based upon.
   */
	public String type();

	/**
	 * The optional description of the Capability definition.
	 */	
	public String description();

	/**
	 * An optional list of one or more valid names of Node Types that are supported as valid sources of any 
	 * relationship established to the declared Capability Type.
	 */
	public List<String> valid_source_types();

	/**
	 * The optional minimum and maximum occurrences for the capability.
	 * By default, an exported Capability should allow at least one relationship to be formed with it with a
	 * maximum of UNBOUNDED relationships.
	 */
	public default Range occurences() {
		return (Range)proxyList("occurences", Range.class);
	}

	/**
	 * An optional list of property definitions for the Capability definition.
	 */
	public default Properties properties() {
		return (Properties)proxy("properties", Properties.class);
	}

	/**
	 * An optional list of attribute definitions for the Capability definition.
	 */
	public default Attributes attributes() {
		return (Attributes)proxy("attributes", Attributes.class);
	}

}
