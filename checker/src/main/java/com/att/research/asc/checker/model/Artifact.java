package com.att.research.asc.checker.model;

/**
 * Artifact definition, spec section 3.5.6
 */
public interface Artifact extends TOSCAObject<Artifact> {

	public String name();

	public String type();
	
	public String description();
	
	public String file();
	
	public String repository();
	
	public String deploy_path();
	
}
