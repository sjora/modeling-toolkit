package com.att.research.asc.checker.model;

import java.util.List;

/**
 */
public interface RelationshipTemplate extends TOSCAObject<RelationshipTemplate> {

	public String name();

	public String type();
	
	public String description();
	
	public String copy();

	public default Metadata metadata() {
		return (Metadata)proxy("metadata", Metadata.class);
	}

	public default PropertiesAssignments properties() {
		return (PropertiesAssignments)proxy("properties", PropertiesAssignments.class);
	}
	
	public default AttributesAssignments attributes() {
		return (AttributesAssignments)proxy("attributes", AttributesAssignments.class);
	}
	
	public default TemplateInterfaces interfaces() {
		return (TemplateInterfaces)proxy("interfaces", TemplateInterfaces.class);
	}

}
