package com.att.research.asc.checker.model;

/*
 * Requirement definition, as it appears in node type definitions (spec section 3.6.3)
 */
public interface Requirement extends TOSCAObject<Requirement> {

	/** */
	public String name();

	/** */
	public String capability();

	/** */
	public String node();

	/** */		
	public default Range occurences() {
		return (Range)proxy("relationship", Range.class);
	}

	public default Relationship relationship() {
		return (Relationship)proxy("relationship", Relationship.class);
	}

	/**
	 * Spec section 3.6.3.2.3 
	 */
	public interface Relationship extends TOSCAObject<Relationship> {

		public String type();

		public default TypeInterfaces interfaces() {
			return (TypeInterfaces)proxy("interfaces", TypeInterfaces.class);
		}
	}

}
