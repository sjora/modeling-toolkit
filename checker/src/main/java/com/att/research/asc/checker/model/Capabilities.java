package com.att.research.asc.checker.model;


/**
 * Collection of type specific capability definitions (spec section 3.6.2) 
 */
public interface Capabilities extends TOSCAMap<Capability> {

}

