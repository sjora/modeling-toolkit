package com.att.research.asc.checker.model;


/**
 * This is the type specific defintion, as per spec section 3.5.13.2.2
 */
public interface Operation extends TOSCAObject<Operation> {


		public String name();
		
		public String description();
	
		public String implementation();

		public default Properties inputs() {
			return (Properties)proxy("inputs", Properties.class);
		}

}
