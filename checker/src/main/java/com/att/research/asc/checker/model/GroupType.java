package com.att.research.asc.checker.model;

import java.util.List;

/**
 * Group type definition, spec section 3.6.11
 */
public interface GroupType extends TOSCAObject<GroupType> {

	public String name();

	public String derived_from();
	
	public String description();
	
	public String version();
	
	public default Metadata metadata() {
		return (Metadata)proxy("metadata", Metadata.class);
	}
	
	public List<String> members();

	public default Properties properties() {
		return (Properties)proxy("properties", Properties.class);
	}

	public default Attributes attributes() {
		return (Attributes)proxy("attributes", Attributes.class);
	}

	public default TypeInterfaces interfaces() {
		return (TypeInterfaces)proxy("interfaces", TypeInterfaces.class);
	}

	public default Requirements requirements() {
		return (Requirements)proxy("requirements", Requirements.class);
	}

	public default Capabilities capabilities() {
		return (Capabilities)proxy("capabilities", Capabilities.class);
	}
	
}
