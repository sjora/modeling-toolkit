package com.att.research.asc.checker.processing;

import com.att.research.asc.checker.Catalog;


/**
 * Just in case you might want to do something with a template (set) once it was checked
 */
public interface Processor<T extends Processor<T>> {

  /* */
	public ProcessBuilder<T> process(Catalog theCatalog);
}
