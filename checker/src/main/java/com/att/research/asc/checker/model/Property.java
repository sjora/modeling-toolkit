package com.att.research.asc.checker.model;

/**
 * See spec section 3.5.8
 */
public interface Property extends TOSCAObject<Property> {

	public String name();

	public String type();

	public String description();

	public default Object _default() {
		return info().get("default");
	}

	public boolean required();

	public Status status();

	public default Constraints constraints() {
		return (Constraints)proxy("constraints", Constraints.class);
	}

	public default EntrySchema entry_schema() {
		return (EntrySchema)proxy("entry_schema", EntrySchema.class);
	}
}
