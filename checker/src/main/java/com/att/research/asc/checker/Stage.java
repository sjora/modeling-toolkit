package com.att.research.asc.checker;

/*
 * The Target processing stages (well, do not pick at processor ..) 
 */
public enum Stage {
  
	located, /* not really used as we do not track a Target that we cannot locate */ 
	parsed,  /* yaml parsing succesfully completed */
	validated, /* syntax check succesfully completed: document is compliant to yaml tosca grammar */
	cataloged, /* all the constructs have been cataloged */
	checked; /* 'semantic' checking completed */
	
	private static final Stage[] stages = values();

	public Stage next() {
		return stages[ordinal() + 1];
	}
}


