package com.att.research.asc.checker.model;


/*
 * A simple representation of the property value assignments, to be used through the Map interface
 * Working with this more basic representation keeps all (jx)paths expressions valid 
 */
public interface PropertiesAssignments extends TOSCAObject<PropertiesAssignments> {

}
