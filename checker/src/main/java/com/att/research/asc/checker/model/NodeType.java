package com.att.research.asc.checker.model;

/**
 * Node type definition, spec section 3.6.9
 */
public interface NodeType extends TOSCAObject<NodeType> {

	public String name();

	public String derived_from();
	
	public String description();
	
	public String version();
	
	public default Metadata metadata() {
		return (Metadata)proxy("metadata", Metadata.class);
	}

	public default Properties properties() {
		return (Properties)proxy("properties", Properties.class);
	}

	public default Attributes attributes() {
		return (Attributes)proxy("attributes", Attributes.class);
	}

	public default Requirements requirements() {
		return (Requirements)proxy("requirements", Requirements.class);
	}

	public default Capabilities capabilities() {
		return (Capabilities)proxy("capabilities", Capabilities.class);
	}

	public default TypeInterfaces interfaces() {
		return (TypeInterfaces)proxy("interfaces", TypeInterfaces.class);
	}

	public default Artifacts artifacts() {
		return (Artifacts)proxy("artifacts", Artifacts.class);
	}
}
