package com.att.research.asc.checker.model;


public interface ServiceTemplate extends TOSCAObject<ServiceTemplate> {

	public String tosca_definitions_version();

	public String description();

	public default Metadata metadata() {
		return (Metadata)proxy("metadata", Metadata.class);
	}
	
	public default Imports imports() {
		return (Imports)proxy("imports", Imports.class);
	}
	
	public default Repositories repositories() {
		return (Repositories)proxy("repositories", Repositories.class);
	}
	
	public default ArtifactTypes artifact_types() {
		return (ArtifactTypes)proxy("artifact_types", ArtifactTypes.class);
	}
	
	public default DataTypes data_types() {
		return (DataTypes)proxy("data_types", DataTypes.class);
	}
	
	public default NodeTypes node_types() {
		return (NodeTypes)proxy("node_types", NodeTypes.class);
	}
	
	public default GroupTypes group_types() {
		return (GroupTypes)proxy("group_types", GroupTypes.class);
	}
	
	public default PolicyTypes policy_types() {
		return (PolicyTypes)proxy("policy_types", PolicyTypes.class);
	}
	
	public default RelationshipTypes relationship_types() {
		return (RelationshipTypes)proxy("relationship_types", RelationshipTypes.class);
	}
	
	public default CapabilityTypes capability_types() {
		return (CapabilityTypes)proxy("capability_types", CapabilityTypes.class);
	}
	
	public default InterfaceTypes interface_types() {
		return (InterfaceTypes)proxy("interface_types", InterfaceTypes.class);
	}

	public default TopologyTemplate topology_template() {
		return (TopologyTemplate)proxy("topology_template", TopologyTemplate.class);
	}
}
