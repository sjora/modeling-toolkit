package com.att.research.asc.checker.model;

import java.util.List;


/*
 * used to render TOSCA constructs that are list of actual constructs:
 *  - requirements
 */
public interface TOSCASeq<T extends TOSCAObject<T>> extends List<T> {

}
