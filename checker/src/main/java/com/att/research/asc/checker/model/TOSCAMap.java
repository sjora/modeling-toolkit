package com.att.research.asc.checker.model;

import java.util.Map;


/*
 * used to render TOSCA constructs that are maps of names to actual construct data:
 *  - node types, etc
 *  - topology template inputs, etc
 */
public interface TOSCAMap<T extends TOSCAObject<T>> extends Map<String, T> {

}
