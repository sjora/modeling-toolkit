package com.att.research.asc.checker.model;

import java.util.List;

public interface NodeTemplate extends TOSCAObject<NodeTemplate> {

	public String name();

	public String type();
	
	public String description();
	
	public List<String> directives();
	
	public String copy();

	public default Metadata metadata() {
		return (Metadata)proxy("metadata", Metadata.class);
	}

	public default PropertiesAssignments properties() {
		return (PropertiesAssignments)proxy("properties", PropertiesAssignments.class);
	}
	
	public default AttributesAssignments attributes() {
		return (AttributesAssignments)proxy("attributes", AttributesAssignments.class);
	}
	
	public default CapabilitiesAssignments capabilities() {
		return (CapabilitiesAssignments)proxy("capabilities", CapabilitiesAssignments.class);
	}
	
	public default RequirementsAssignments requirements() {
		return (RequirementsAssignments)proxy("requirements", RequirementsAssignments.class);
	}

	public default TemplateInterfaces interfaces() {
		return (TemplateInterfaces)proxy("interfaces", TemplateInterfaces.class);
	}

	public default Artifacts artifacts() {
		return (Artifacts)proxy("artifacts", Artifacts.class);
	}

}
