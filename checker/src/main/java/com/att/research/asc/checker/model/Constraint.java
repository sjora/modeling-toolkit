package com.att.research.asc.checker.model;

/**
 * constraint definition, spec section 3.6.6
 */
public interface Constraint extends TOSCAObject<Constraint> {

	public Constraint.Type name();

	/* this is a one entry map so here we pick the single 
	 */
	public default Object expression() {
		return info().values().iterator().next();
	}

	public enum Type {
		equal,
		greater_than,
		greater_or_equal,
		less_than,
		less_or_equal,
		in_range,
		valid_values,
		length,
		min_length,
		max_length,
		pattern
	}
}
