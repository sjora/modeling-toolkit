package com.att.research.asc.checker.model;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;


/*
 * Interface definition used in templates (node, relationship)
 */
public interface TemplateInterface extends TOSCAObject<TemplateInterface> {

	public String name();

	public default Inputs inputs() {
		return (Inputs)proxy("inputs", Inputs.class);
	}

	/**
	 * See InterfaceType for the reason for the implementation below. 
	 * Use the template specific operation definition, as per spec section 3.5.13.2.3
	 */
	public default TemplateInterface.Operations operations() {
		return (Operations)
			TOSCAProxy.record(info(),
												info ->	TOSCAProxy.buildMap(null,
																										Maps.filterKeys((Map)info,
													 																					key -> !("inputs".equals(key))),
															 											Operations.class));
	}

	/**
	 * Is this to be viewed as an 'operation assignment' ??
	 */	
	public interface Operations extends TOSCAMap<Operation> {
	}

	/*
	 * Template specific operation definition, section 3.5.13.2.3
	 */
	public interface Operation extends TOSCAObject<Operation> {

		public String name();
		
		public String description();
	
		public default PropertiesAssignments inputs() {
			return (PropertiesAssignments)proxy("inputs", PropertiesAssignments.class);
		}
		
		public default Implementation implementation() {
			return (Implementation)proxy("implementation", Implementation.class);
		}

		/**
		 */
		public interface Implementation extends TOSCAObject<Implementation> {
		
			public String primary();
			
			public List<String> dependencies();
	
		}
			
	}

}
