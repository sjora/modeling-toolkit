package com.att.research.asc.checker.model;

import java.util.List;

public interface CapabilityType extends TOSCAObject<CapabilityType> {

	public String name();

	public String derived_from();
	
	public String description();

	public String version();
	
	public List<String> valid_source_types();

	public default Properties properties() {
		return (Properties)proxy("properties", Properties.class);
	}

	public default Attributes attributes() {
		return (Attributes)proxy("attributes", Attributes.class);
	}
}
