package com.att.research.asc.checker.model;

/**
 * Node filter definition, spec section 
 */
public interface NodeFilter extends TOSCAObject<NodeFilter> {

	public default Properties properties() {
		return (Properties)proxy("properties", Properties.class);
	}

	public default Capabilities capabilities() {
		return (Capabilities)proxy("capabilities", Capabilities.class);
	}
	
}
