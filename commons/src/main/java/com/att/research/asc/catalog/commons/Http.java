package com.att.research.asc.catalog.commons;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpEntity;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.DefaultResponseErrorHandler;

import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import org.springframework.http.converter.HttpMessageConverter;

public class Http {

	protected Http() {
	}
	
	
	public static <T> Future<T> exchange(String theUri, HttpMethod theMethod, HttpEntity theRequest, Class<T> theResponseType) {
	
		AsyncRestTemplate restTemplate = new AsyncRestTemplate();

		List<HttpMessageConverter<?>> converters = restTemplate.getMessageConverters();
		converters.add(0, new JSONHttpMessageConverter());
		restTemplate.setMessageConverters(converters);

		HttpFuture<T> result = new HttpFuture<T>();
		try {
			restTemplate
				.exchange(theUri, theMethod, theRequest, theResponseType)
					.addCallback(result.callback);
		}
		catch (RestClientException rcx) {
			return Futures.failedFuture(rcx);
		}
		catch (Exception x) {
			return Futures.failedFuture(x);
		}
	 
		return result;
	}



	public static class HttpFuture<T>
										extends Futures.BasicFuture<T> {

		HttpFuture() {
		}

		ListenableFutureCallback<ResponseEntity<T>> callback = new ListenableFutureCallback<ResponseEntity<T>>() {

			public void	onSuccess(ResponseEntity<T> theResult) {
				HttpFuture.this.result(theResult.getBody());
			}

			public void	onFailure(Throwable theError) {
				if (theError instanceof HttpClientErrorException) {
					HttpFuture.this.cause(new Exception((HttpClientErrorException)theError));
				}
				else {
					HttpFuture.this.cause(theError);
				}
			}
		};

	}
}
