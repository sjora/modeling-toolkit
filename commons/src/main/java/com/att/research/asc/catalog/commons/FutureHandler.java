package com.att.research.asc.catalog.commons;



/**
 * Modeled after the vertx future
 */
@FunctionalInterface
public interface FutureHandler<T> {

	public void handle(Future<T> theResult);
	
}
