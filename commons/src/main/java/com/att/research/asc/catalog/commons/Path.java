package com.att.research.asc.catalog.commons;


import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;

import java.util.stream.Collectors;

import java.util.function.Supplier;
import java.util.function.Predicate;

import org.apache.commons.io.FilenameUtils;

/**
 */
public class Path<E extends Path.Element> implements Comparable<Path>, Iterable<E> {

	public static Path<Element> root() {
		return new Path<Element>(Element::new);
	}

	//
	private List<E> 		elements = new ArrayList<E>(10);
	private Supplier<E>	elementsFactory;

	/* */
	public Path(Supplier<E> theElementsFactory) {
		this.elementsFactory = theElementsFactory;
	}

	public E enter(String theName) {
//		return elementsFactory.get()
//						.name(theName);
		E newElement = this.elementsFactory.get();
		newElement.name(theName);
		this.elements.add(newElement);
		return newElement;
	}

	public E exit() {
		return this.elements.remove(this.elements.size()-1); 
	}

	public E element(int thePos) {
		return this.elements.get(thePos);
	}

	public int elementCount() {
		return this.elements.size();
	}

	@Override
	public Iterator<E> iterator() {
		return Collections.unmodifiableList(this.elements).iterator();
	}

	/** */
	public E enclosing(Predicate<E> thePredicate) {
		return this.elements
						.stream()
						.filter(thePredicate)
						.findFirst()
						.get();
	} 

	/** */
	public boolean match(String thePattern) {
		return FilenameUtils.wildcardMatch(toString(), thePattern);		
	}

	/** */
	@Override
	public String toString() {
		return this.elements
									.stream()
										.map(Object::toString)
            					.collect(Collectors.joining("/","/",""));
	}

	@Override
	public boolean equals(Object thePath) {
		return 0 == this.compareTo((Path)thePath);
	}

	@Override
	public int compareTo(final Path thePath) {
		int res = 0;
		for (Iterator<E> ei = elements.iterator(), pei = thePath.elements.iterator();
				 ei.hasNext() && pei.hasNext() && res == 0; ) {
			res = ei.next().compareTo(pei.next());
		}
		return res;
	}


	/** */
	public static class Element<E extends Element>
																								implements Comparable<E> {

		private String name;

		public Element() {
		}

		public E name(String theName) {
			this.name = theName;
			return (E)this;
		}

		public String name() {
			return this.name;
		}
	
		/** */
		@Override
		public String toString() {
			return this.name;
		}

		@Override
		public boolean equals(Object theElement) {
			return 0 == this.compareTo((E)theElement);
		}

		@Override
		public int compareTo(E theElement) {
			return this.name.compareTo(theElement.name());
		}
	}

	public static void main(String[] theArgs) {

		Path p = Path.root();
		System.out.println("Enter " + p.enter("abc") + ", " + p);
		System.out.println("Enter " + p.enter("def") + ", " + p);
		System.out.println("Enter " + p.enter("xyz") + ", " + p);

		System.out.println("Match: " + p.match(theArgs[0]));
			
	}

}
