package com.att.research.asc.catalog.commons;

import java.io.Reader;
import java.io.FileReader;
import java.io.IOException;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Collections;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import java.util.logging.Logger;
import java.util.logging.Level;

import org.apache.commons.jxpath.Pointer;
import org.apache.commons.jxpath.JXPathContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;

import org.yaml.snakeyaml.Yaml;


/**
 * Practically a copy of the Validator's service Recycler, minus the Spring framework aspects + picking up the
 * description of every node
 */
public class Recycler {

	private Logger log = Logger.getLogger(Recycler.class.getName());
	private List<Map> 	 imports;
	private List<String> metas;

	public Recycler() {
		withImports();
		withMetas(null);
	}

	public Recycler withImports(String... theImports) {
		log.finest("Setting imports to " + theImports);
		ListBuilder importsBuilder = new ListBuilder();
		for (int i = 0; i < theImports.length; i++)
			importsBuilder.add(new MapBuilder()
													.put("i" + i, theImports[i])
													.build());
		this.imports = importsBuilder.build();
		return this;
	}
/*
	public List<String> getImports() {
		return this.imports;
	}
*/

	private List imports() {
		ListBuilder importsBuilder = new ListBuilder();
		for (Map e: this.imports)
			importsBuilder.add(new MapBuilder()
													.putAll(e)
													.build());
		
		return importsBuilder.build();
	}

	public Recycler withMetas(String... theMetas) {
		this.metas = (theMetas == null) ? Collections.EMPTY_LIST : Arrays.asList(theMetas);
		return this;
	}

	public Object recycle(final Reader theSource) throws Exception {
		return this.recycle(new ObjectMapper().readValue(theSource, (Class)HashMap.class));
	}
    
	public Object recycle(final Object theDump) throws Exception {
  
		final JXPathContext jxroot = JXPathContext.newContext(theDump);
    jxroot.setLenient(true);

		final Map<String, Object> nodeTemplates = 
			(Map<String, Object>)new MapBuilder()
				.putAll(
						StreamSupport
							.stream(
								Spliterators.spliteratorUnknownSize((Iterator<Pointer>)jxroot.iteratePointers("/nodes"), 16), false)
							.map(p -> {
 										JXPathContext jxnode = jxroot.getRelativeContext(p);
										return new AbstractMap.SimpleEntry<String,Object>(
											(String)jxnode.getValue("name") + "_" + (String)jxnode.getValue("nid"),
											new MapBuilder()
													.put("type", jxnode.getValue("type/name"))
													.put("description", jxnode.getValue("description"))
													.putOpt("metadata", nodeMetadata(jxnode))
													.putOpt("properties", nodeProperties(jxnode))
													.putOpt("requirements", nodeRequirements(jxnode))
													.build());
        					})::iterator)
				.buildOpt();

			return new MapBuilder()
									.put("tosca_definitions_version", "tosca_simple_yaml_1_0_0")
									.put("imports", imports())
									.put("topology_template", new MapBuilder()
																							.putOpt("node_templates", nodeTemplates)
																							.build())
									.build();
	}
	
	/* */								 
	private Object nodeProperties(JXPathContext theNodeContext) {
		return
			new MapBuilder()
				.putAll(
					StreamSupport.stream(
						Spliterators.spliteratorUnknownSize((Iterator<Map>)theNodeContext.iterate("properties"), 16), false)
													.map(m -> new AbstractMap.SimpleEntry(m.get("name"), this.nodeProperty(m)))
													.filter(e -> e.getValue() != null)
							::iterator)
				.buildOpt();
	}
    
	/* */
	private Object nodeProperty(final Map theSpec) {
		Object value = theSpec.get("value");
		if (value == null) {
			value = theSpec.get("default");
			if (value == null) {
				/*final*/ Map assign = (Map)theSpec.get("assignment");
				if (assign != null) {
					value = assign.get("value");
				}
			}
		}
		String type = (String)theSpec.get("type");
		if (value != null && type != null) {
			try {
				if ("map".equals(type) && !(value instanceof Map)) {
					value = new ObjectMapper().readValue(value.toString(), new TypeReference<Map>(){});
				}
				else if ("list".equals(type) && !(value instanceof List)) {
					value = new ObjectMapper().readValue(value.toString(), new TypeReference<List>(){});
				}
				else if ("integer".equals(type) && (value instanceof String)) {
					value = Integer.valueOf((String)value);		
				}
				else if ("float".equals(type) && (value instanceof String)) {
					value = Double.valueOf((String)value); //double because that's how the yaml parser would encode it 		
				}
			}
			catch (NumberFormatException nfx) {
				log.fine("Failed to process String representation " + value + " of numeric data: " + nfx);
				nfx.printStackTrace();
			}
			catch (IOException iox) {
				//should propagate as an unchecked exception?
				log.fine("Failed to process " + value.getClass().getName() + " representation of a collection: " + iox);
				iox.printStackTrace();
			}
		}
		return value;
	}
	
	/* */
	private List nodeRequirements(JXPathContext theNodeContext) {
		return 
			new ListBuilder()
				.addAll(
					StreamSupport.stream(
						Spliterators.spliteratorUnknownSize((Iterator<Map>)theNodeContext.iterate("requirements"), 16), false)
													.flatMap(m -> this.nodeRequirement(m, theNodeContext).stream())
					//nicer that the ListBuilder buy cannot handle the empty lists, i.e. it will generate empty requirement lists
					//								.collect(Collectors.toList())
													.toArray())
				.buildOpt();
	}

	/*
	 * @param theSpec the requirement entry that appears within the node specification
	 * @param theNodeContext .. Should I pass the root context instead of assuming that the nodes context has it as parent? 
	 * @return a List as one requirement (definition) could end up being instantiated multiple times
 	 */
	private List nodeRequirement(final Map theSpec, JXPathContext theNodeContext/*Iterator theTargets*/) {

		final ListBuilder value = new ListBuilder();

		final Map target = (Map)theSpec.get("target");
		final Map capability = (Map)theSpec.get("capability");

//System.out.println("Looking up for relation " + theSpec.get("name") + " for node " + theNodeContext.getValue("nid"));

		//this are actual assignments
		for (Iterator i = 
					theNodeContext.getParentContext()
						.iterate("/relations[@n2='" + theNodeContext.getValue("nid") + "']/meta[@p2='" + theSpec.get("name") +"']");
					 i.hasNext(); ) {

			String targetNode = (String)((Map)i.next()).get("n1");

//System.out.println("\tFound relation " + theSpec.get("name") + " from " + theNodeContext.getValue("nid") + " to " + targetNode);

			//make sure target exists
			if (null == theNodeContext.getParentContext().getValue("/nodes[@nid='" + targetNode + "']")) {
				log.warning("Relation points to non-existing node " + targetNode);
				continue; //this risks of producing a partial template ..
			}

			value.add(new MapBuilder()
									.put(theSpec.get("name"), new MapBuilder()
																							.putOpt("node", targetNode)
																							.putOpt("capability", capability == null ? null : capability.get("name"))
																							.build()) 
									.build());
		}

		//temporary
		for (Iterator i = 
					theNodeContext.getParentContext()
						.iterate("/relations[@n1='" + theNodeContext.getValue("nid") + "']/meta[@p1='" + theSpec.get("name") +"']");
					 i.hasNext(); ) {

			String targetNode = (String)((Map)i.next()).get("n2");

//System.out.println("\tFound relation " + theSpec.get("name") + " from " + theNodeContext.getValue("nid") + " to " + targetNode);

			//make sure target exists
			if (null == theNodeContext.getParentContext().getValue("/nodes[@nid='" + targetNode + "']")) {
				log.warning("Relation points to non-existing node " + targetNode);
				continue; //this risks of producing a partial template ..
			}

			value.add(new MapBuilder()
									.put(theSpec.get("name"), new MapBuilder()
																							.putOpt("node", targetNode)
																							.putOpt("capability", capability == null ? null : capability.get("name"))
																							.build()) 
									.build());
		}
		//end temporary

		if (value.isEmpty()) {
			value.add(new MapBuilder()
									.put(theSpec.get("name"), new MapBuilder()
																							.putOpt("node", target == null ? null : target.get("name"))
																							.putOpt("capability", capability == null ? null : capability.get("name"))
																							.build()) 
									.build());
		}

		return value.build();
	}

	/* */								 
	private Object nodeMetadata(JXPathContext theNodeContext) {
		return
			new MapBuilder()
				.putAll(
						this.metas
							.stream()
							.flatMap(m -> {
												Object v = theNodeContext.getValue(m);
												if (v == null)
													return Stream.empty();
												if (v instanceof Map)
													return ((Map)v).entrySet()
																	.stream()
																	.map(e -> new AbstractMap.SimpleEntry<String,Object>
																															(((Map.Entry)e).getKey().toString(),
																															 ((Map.Entry)e).getValue().toString()));
												
												return Stream.of(new AbstractMap.SimpleEntry<String,Object>(m, v.toString()));
											})
						::iterator)
				.buildOpt();
	}


	public static String toString(Object theVal) {
		return new Yaml().dump(theVal);
	}
    
}
