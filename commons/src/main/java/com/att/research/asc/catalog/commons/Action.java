package com.att.research.asc.catalog.commons;


/**
 */
public interface Action<T> {

	public Future<T> execute();

}
