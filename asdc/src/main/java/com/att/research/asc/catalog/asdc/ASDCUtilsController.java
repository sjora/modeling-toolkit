package com.att.research.asc.catalog.asdc;

import java.io.StringReader;

import java.util.logging.Logger;
import java.util.logging.Level;

import java.util.UUID;
import java.util.Map;
import java.util.List;
import java.util.concurrent.Callable;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
 
import org.springframework.beans.BeansException;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.json.JSONObject;


@RestController
@ConfigurationProperties(prefix="asdcUtilsController")
public class ASDCUtilsController implements ApplicationContextAware {

	private static Logger log = Logger.getLogger(ASDCUtilsController.class.getName());

	private ApplicationContext			appCtx;

	public void setApplicationContext(ApplicationContext theCtx) throws BeansException {
    this.appCtx = theCtx;
	}

	@RequestMapping(value={"/utils/clone/{assetType}/{sourceId}/{targetId}"}, method={RequestMethod.GET}, produces={"application/json"})
	public ResponseEntity<String[]> clone(@PathVariable("assetType") String theAssetType,
																				@PathVariable("sourceId") String theSourceId,
																				@PathVariable("targetId") String theTargetId) {

		ASDCUtils utils = (ASDCUtils)this.appCtx.getBean("asdcutils");
		List<JSONObject> clones = null;
		try {
			clones = utils.cloneAssetArtifacts(ASDC.AssetType.valueOf(theAssetType),
																					UUID.fromString(theSourceId),
																					UUID.fromString(theTargetId))
										.withLabel("clone")
										.execute()
										.waitForResult();
		}
		catch (Exception x) {
			return new ResponseEntity(x.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		String[] uuids = null;
		if (clones != null) {
			//why in the workd do I need the cast??
			uuids = (String[])clones.stream()
																	.map(e -> e.getString("uuid"))
																	.toArray(String[]::new);
		}
		return new ResponseEntity(uuids, HttpStatus.OK);
	}
	
	@RequestMapping(value={"/utils/blueprint"}, method={RequestMethod.POST}, produces={"application/json"})
	public ResponseEntity<String> blueprint(@RequestBody String theCdump) {

		ASDCUtils utils = (ASDCUtils)this.appCtx.getBean("utils");
		String blueprint = null;
		try {
			blueprint = utils.buildBlueprint(new StringReader(theCdump))
												.waitForResult()
													.waitForResult();
		}
		catch (Exception x) {
			return new ResponseEntity(x.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity(blueprint, HttpStatus.OK);
	}
	
	@PostConstruct
	public void initController() {
		log.entering(getClass().getName(), "initASDCUtilsController");

		//Done
		log.log(Level.INFO, "ASDCUtilsController started");
	}

	@PreDestroy
	public void cleanupController() {
		log.entering(getClass().getName(), "cleanupASDCUtilsController");
	}

}
