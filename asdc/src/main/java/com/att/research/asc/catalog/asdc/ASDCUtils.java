package com.att.research.asc.catalog.asdc;

import java.io.File;
import java.io.Reader;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.UncheckedIOException;

import java.net.URI;

import java.util.List;
import java.util.LinkedList;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import java.util.Properties;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import java.util.function.Function;
import java.util.function.BiFunction;

import java.util.zip.ZipInputStream;
import java.util.zip.ZipEntry;

import org.json.JSONObject;
import org.json.JSONArray;

import com.att.research.asc.catalog.commons.Future;
import com.att.research.asc.catalog.commons.Futures;
import com.att.research.asc.catalog.commons.Actions;
import com.att.research.asc.catalog.commons.Recycler;
import com.att.research.asc.catalog.commons.Http;

import com.att.research.asc.checker.Target;
import com.att.research.asc.checker.Construct;
//import com.att.research.asc.checker.MapBuilder;
import com.att.research.asc.checker.TargetLocator;
import com.att.research.asc.checker.Checker;
import com.att.research.asc.checker.Catalog;
import com.att.research.asc.checker.CheckerException;

import org.apache.commons.lang3.StringUtils;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import org.springframework.util.Base64Utils;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Pointer;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Component("asdcutils")
@Scope("singleton")
@ConfigurationProperties(prefix="asdcutils")
public class ASDCUtils {

	@Autowired
	private ASDC				asdc;
	@Autowired
	private	Blueprinter	blueprint;

	public ASDCUtils() {
	}

	public ASDCUtils(URI theASDCURI) {
		this(theASDCURI, null);
	}

	public ASDCUtils(URI theASDCURI, URI theBlueprinterURI) {
		this.asdc = new ASDC();
		this.asdc.setUri(theASDCURI);
		if (theBlueprinterURI != null) {
			this.blueprint = new Blueprinter();
			this.blueprint.setUri(theBlueprinterURI);
		}
	}
	
	public ASDCUtils(ASDC theASDC) {
		this (theASDC, null);
	}

	public ASDCUtils(ASDC theASDC, Blueprinter theBlueprinter) {
		this.asdc = theASDC;
		this.blueprint = theBlueprinter;
	}
	
	public CloneAssetArtifactsAction cloneAssetArtifacts(ASDC.AssetType theAssetType, UUID theSourceId, UUID theTargetId) {
		return new CloneAssetArtifactsAction(this.asdc, theAssetType, theSourceId, theTargetId);
	}

	public static class CloneAssetArtifactsAction extends ASDC.ASDCAction<CloneAssetArtifactsAction, List<JSONObject>> {

		private ASDC.AssetType 	assetType;
		private UUID 						sourceId,
														targetId;

		protected CloneAssetArtifactsAction(ASDC theASDC, ASDC.AssetType theAssetType, UUID theSourceId, UUID theTargetId) {
			theASDC.super(new JSONObject());
			this.assetType = theAssetType;
			this.sourceId = theSourceId;
			this.targetId = theTargetId;
		//	with("artifactGroupType", ASDC.ArtifactGroupType.DEPLOYMENT);
		}

		protected CloneAssetArtifactsAction self() {
			return this;
		}
	
		public CloneAssetArtifactsAction withLabel(String theLabel) {
			return with("artifactLabel", theLabel);
		}

		protected String[] mandatoryInfoEntries() {
			return new String[] {};
		}
		/*
		public Future<List<JSONObject>> execute() {
			checkMandatory();	

			final Futures.Accumulator<JSONObject> accumulator = new Futures.Accumulator<JSONObject>();

			new Futures.Accumulator()
				.add(asdc().getAssetArchive(this.assetType, this.sourceId))
				.add(asdc().getAsset(this.assetType, this.sourceId, JSONObject.class))
				.accumulate()
				.setHandler(assetFuture -> {
											System.out.println("*** " + assetFuture.result());
											processArtifacts((List)assetFuture.result(),
																			 (JSONObject theInfo, byte[] theData) -> {
																					theInfo.remove("artifactChecksum");
																					theInfo.remove("artifactUUID");
																					theInfo.remove("artifactVersion");
																					theInfo.remove("artifactURL");
																					theInfo.put("description", theInfo.remove("artifactDescription"));
																					theInfo.put("payloadData", Base64Utils.encodeToString(theData));
																					return theInfo;
																				}, null)
												.map(artifactInfo -> {
																							System.out.println("** create artifact from " + artifactInfo + " and " + this.info);
																							try {
																								return asdc().createAssetArtifact(this.assetType, this.targetId)
																															.withInfo(ASDC.merge(artifactInfo, this.info))
																															.withOperator(this.operatorId)
																							//								.withGroupType(ASDC.ArtifactGroupType.DEPLOYMENT)
																															.execute();
																							}
																							catch(Throwable t) {
																							System.out.println("** error during action " + t);
																								//must return the same type as the return above .. any better way
																								//to convey the error??  
																								return Futures.succeededFuture(new JSONObject()
																																									.put("error", t.toString()));
																							} 
																						})
												.collect(Collector.of(() -> { return accumulator; },
																							Futures.Accumulator<JSONObject>::add,
																							Futures.Accumulator<JSONObject>::addAll,
																 							acc -> acc.accumulate(),
																							Collector.Characteristics.UNORDERED));
										});

			return accumulator;
		}
		*/

		public Future<List<JSONObject>> execute() {
			checkMandatory();	

			final Actions.Sequence<JSONObject> sequencer = new Actions.Sequence<JSONObject>();

			new Actions.Sequence()
				.add(asdc().getAssetArchiveAction(this.assetType, this.sourceId))
				.add(asdc().getAssetAction(this.assetType, this.sourceId, JSONObject.class))
				.execute()
				.setHandler(assetFuture -> {
											System.out.println("*** " + assetFuture.result());
											processArtifacts((List)assetFuture.result(),
																			 (JSONObject theInfo, byte[] theData) -> {
																					theInfo.remove("artifactChecksum");
																					theInfo.remove("artifactUUID");
																					theInfo.remove("artifactVersion");
																					theInfo.remove("artifactURL");
																					theInfo.put("description", theInfo.remove("artifactDescription"));
																					theInfo.put("payloadData", Base64Utils.encodeToString(theData));
																					return theInfo;
																				}, null)
												.forEach(artifactInfo ->sequencer.add(
																										asdc().createAssetArtifact(this.assetType, this.targetId)
																														.withInfo(ASDC.merge(artifactInfo, this.info))
																														.withOperator(this.operatorId)));
											sequencer.execute();
										});
			
			return sequencer.future();
		}
	} //the Action class

	/* */
	private static JSONObject lookupArtifactInfo(JSONArray theArtifacts, String theName) {
	
		for (int i = 0; theArtifacts != null && i < theArtifacts.length(); i++) {
			JSONObject artifactInfo = theArtifacts.getJSONObject(i);
			if (theName.equals(artifactInfo.getString("artifactName"))) {
System.out.println("Found artifact info " + artifactInfo);
				return artifactInfo;
			}
		}

		return null;	
	}

	private static byte[] extractArtifactData(ZipEntry theEntry, InputStream theEntryStream) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			byte[] buff = new byte[4096];
 	  	int cnt = 0;
 	  	while ((cnt = theEntryStream.read(buff)) != -1) {
   			baos.write(buff, 0, cnt);
   		}
		}
		finally {
 		  baos.close();
		}
		return baos.toByteArray();
	}


	/**
	 * Recycle a cdump, fetch all relevant ASDC artifacts, interact with Shu's toscalib service in order to generate
	 * a blueprint. No 'Action' object here as there is nothig to set up. 
	 */
	public Future<Future<String>> buildBlueprint(Reader theCdump) {

		final Recycler recycler = new Recycler();
		Object template = null;
		
		try {
			template = recycler.recycle(theCdump);
			
		}
		catch (Exception x) {
			return Futures.failedFuture(x);
		}

		JXPathContext jxroot = JXPathContext.newContext(template);
    jxroot.setLenient(true);

		//based on the output of ASDCCatalog the node description will contain the UUID of the resource declaring it
		List uuids = (List)
						StreamSupport
							.stream(
								Spliterators.spliteratorUnknownSize(
															jxroot.iterate("topology_template/node_templates/*/description"), 16), false)
							.distinct()
							.filter(desc -> desc != null)
							//the desc contains the full URI and the resource uuid is the 5th path element
							.map(desc -> desc.toString().split("/")[5])
     					.collect(Collectors.toList());

			//prepare fetching all archives/resource details
			final Futures.Accumulator accumulator = new Futures.Accumulator();
			uuids.stream()
					 .forEach(uuid -> {
												UUID rid = UUID.fromString((String)uuid);
												accumulator.add(
															this.asdc.getAssetArchive(ASDC.AssetType.resource, rid));
												accumulator.add(
															this.asdc.getAsset(ASDC.AssetType.resource, rid, JSONObject.class));
										});

			final byte[] templateData = recycler.toString(template).getBytes(/*"UTF-8"*/);
			//retrieve all resource archive+details, prepare blueprint service request and send its request
			return Futures.advance(accumulator.accumulate(),
														 (List theArchives) -> {
																	Blueprinter.BlueprintAction action = blueprint.generateBlueprint();
																	processArtifacts(theArchives,
																									 (JSONObject theInfo, byte[] theData) ->
																																new JSONObject()
																																	.put(theInfo.getString("artifactDescription"),
															 																	 			 Base64Utils.encodeToString(theData)),
																									 (Stream<JSONObject> theAssetArtifacts) ->
																																theAssetArtifacts.reduce(new JSONObject(),
																																 ASDC::merge))
																		.forEach(artifactInfo -> action.withModelInfo(artifactInfo));	
																	
																	return action.withTemplateData(templateData)
																							 .execute();
															});
	}

	public Future<Future<String>> buildBlueprint2(Reader theCdump) {
		return processCdump(theCdump,
												(theTemplate, theArchives) -> {
														Blueprinter.BlueprintAction action = blueprint.generateBlueprint();
														processArtifacts(theArchives,
																						 (JSONObject theInfo, byte[] theData) -> 
																																new JSONObject()
																																	.put(theInfo.getString("artifactDescription"),
															 																	 			 Base64Utils.encodeToString(theData)),
																						 (Stream<JSONObject> theAssetArtifacts) ->
																																theAssetArtifacts.reduce(new JSONObject(),
																																 ASDC::merge))
																.forEach(artifactInfo -> action.withModelInfo(artifactInfo));	
																	
														return action
																		.withTemplateData(Recycler.toString(theTemplate).getBytes())
																		.execute();

												});
	}
	
	public Future<String> buildBlueprint3(Reader theCdump) {
		return Futures.advance(buildCatalog(theCdump),
													 catalog -> new Cloudify(catalog).createBlueprintDocument());
	}


	private static class Tracker implements TargetLocator {

		private static enum Position {
			SCHEMA,
			TEMPLATE,
			TRANSLATE;
		}
		private static final int Positions = Position.values().length;

		private List<Target> tgts = new ArrayList<Target>(3);

		public Tracker() {
			clear();
		}

		public boolean addSearchPath(URI theURI) { return false; } 
		public boolean addSearchPath(String thePath) { return false; }
		public Iterable<URI> searchPaths() { return Collections.EMPTY_LIST; }

		protected int position(String... theKeys) {
			for (String key: theKeys) {
				if ("schema".equals(key))
					return Position.SCHEMA.ordinal();
				if ("template".equals(key))
					return Position.TEMPLATE.ordinal(); 
				if ("translate".equals(key))
					return Position.TRANSLATE.ordinal();
			}
			return -1;
		}

		public Target resolve(String theName) {
			for (Target tgt: tgts) {
				if (tgt != null && tgt.getName().equals(theName))
					return tgt;
			}
			return null;
		}

		public void track(JSONObject theInfo, final byte[] theData) {
			String	uri = theInfo.getString("artifactURL").split("/")[5];
			String name = theInfo.getString("artifactName"), 
						 desc = theInfo.getString("artifactDescription"),
						 label = theInfo.getString("artifactLabel");
			int 	 pos = position(desc, label);

System.out.println("Tracking "  + name + " at " + pos + ", " + theInfo.optString("artifactURL"));

			if (pos > -1) {
				tgts.set(pos, 
								 new Target(name,	URI.create("asdc:" + uri + "/" + name)) {
															public Reader open() throws IOException {
    														return new BufferedReader(
																	new InputStreamReader(
																		new ByteArrayInputStream(theData)));
																	}
															 });
			}
		}

		public boolean hasSchema() { return tgts.get(Position.SCHEMA.ordinal()) != null; }
		public Target schema() { return tgts.get(Position.SCHEMA.ordinal()); }
		
		public boolean hasTemplate() { return tgts.get(Position.TEMPLATE.ordinal()) != null; }
		public Target template() { return tgts.get(Position.TEMPLATE.ordinal()); }
		
		public boolean hasTranslation() { return tgts.get(Position.TRANSLATE.ordinal()) != null; }
		public Target translation() { return tgts.get(Position.TRANSLATE.ordinal()); }

		public void clear() {
			if (tgts.size() == 0) {
				for (int i = 0; i < Positions; i++)  tgts.add(null);
			}
			else
				Collections.fill(tgts, null);
		}
	}


	private Checker buildChecker() {
		try {
			return new Checker();
		}
		catch(CheckerException cx) {
			cx.printStackTrace();
			return null;
		}
	}

	public Future<Catalog> buildCatalog(Reader theCdump) {

		//
		//the purpose of the tracking is to be able to resolve import references within the 'space' of an
		//asset's artifacts
		//processing order is important too so we 'order the targets: schema, template, translation
		//
		final Tracker tracker = new Tracker();
		final Catalog catalog = Checker.buildCatalog();

		return processCdump(theCdump,
												(theTemplate, theArchives) -> {

														final Checker checker = buildChecker();
														if (checker == null)
															return (Catalog)null;
														checker.setTargetLocator(tracker);

														processArtifacts(
																						 theArchives,
																						 (JSONObject theInfo, byte[] theData) -> {
																								tracker.track(theInfo, theData);
																								return (Catalog)null;
															 							 },
																						 // aggregation: this is where the actual processing takes place now that
																						 // we have all the targets
																						 (Stream<Catalog> theAssetArtifacts) -> {
																								//the stream is full of nulls, ignore it, work with the tracker

																								try {
																									if (tracker.hasSchema())
																										checker.check(tracker.schema(), catalog);
																									if (tracker.hasTemplate())
																										checker.check(tracker.template(), catalog);
																									if (tracker.hasTranslation())
																										checker.check(tracker.translation(), catalog);
																								}
																								catch (CheckerException cx) {
																									//got to do better than this
																									System.err.println(cx);
																								}
																								finally {
																									tracker.clear();
																								}
																								return checker.catalog();
																						 });

														Target cdump = new Target("cdump", URI.create("asdc:cdump"));
														cdump.setTarget(theTemplate);

														try {
															checker.validate(cdump, catalog);
														}
														catch (CheckerException cx) {
															cx.printStackTrace();
														}

														return catalog;						
													});
	}

	/* The common process of recycling, retrieving all related artifacts and then doing 'something' */
	private <T> Future<T> processCdump(Reader theCdump, BiFunction<Object, List, T> theProcessor) {
	
		final Recycler recycler = new Recycler();
		Object template = null;
		try {
			template = recycler.recycle(theCdump);

		}
		catch (Exception x) {
			return Futures.failedFuture(x);
		}

		JXPathContext jxroot = JXPathContext.newContext(template);
    jxroot.setLenient(true);

		//based on the output of ASDCCatalog the node description will contain the UUID of the resource declaring it
		List uuids = (List)
						StreamSupport
							.stream(
								Spliterators.spliteratorUnknownSize(
															jxroot.iterate("topology_template/node_templates/*/description"), 16), false)
							.distinct()
							.filter(desc -> desc != null)
							//the desc contains the full URI and the resource uuid is the 5th path element
							.map(desc -> desc.toString().split("/")[5])
     					.collect(Collectors.toList());

			//prepare fetching all archives/resource details
/*
			final Futures.Accumulator accumulator = new Futures.Accumulator();
			uuids.stream()
					 .forEach(uuid -> {
												UUID rid = UUID.fromString((String)uuid);
												accumulator.add(
															this.asdc.getAssetArchive(ASDC.AssetType.resource, rid));
												accumulator.add(
															this.asdc.getAsset(ASDC.AssetType.resource, rid, JSONObject.class));
										});
			
			final Object tmpl = template;
			return Futures.advance(accumulator.accumulate(),
														 (List theArchives) -> theProcessor.apply(tmpl, theArchives));
*/
			//serialized fetch version
			final Actions.Sequence sequencer = new Actions.Sequence();
			uuids.stream()
					 .forEach(uuid -> {
												UUID rid = UUID.fromString((String)uuid);
												sequencer.add(
															this.asdc.getAssetArchiveAction(ASDC.AssetType.resource, rid));
												sequencer.add(
															this.asdc.getAssetAction(ASDC.AssetType.resource, rid, JSONObject.class));
										});

			final Object tmpl = template;
			return Futures.advance(sequencer.execute(),
														 (List theArchives) -> theProcessor.apply(tmpl, theArchives));
	}

	private static <T> Stream<T> processArtifacts(List theArtifactData,
																							BiFunction<JSONObject, byte[], T> theProcessor,
																							Function<Stream<T>, T> theAggregator) {
			
		Stream.Builder<T> assetBuilder = Stream.builder();
			
		for (int i = 0; i< theArtifactData.size(); i = i+2) { //cute old style loop
			
			JSONObject assetInfo = (JSONObject)theArtifactData.get(i+1);
			byte[] assetData = (byte[])theArtifactData.get(i+0);

			JSONArray artifacts = assetInfo.optJSONArray("artifacts");

			Stream.Builder<T> artifactBuilder = Stream.builder();
			ZipInputStream zipper = null;
			try {
				//we process the artifacts in the order they are stored in the archive .. fugly
				zipper = new ZipInputStream(new ByteArrayInputStream(assetData));
				for (ZipEntry zipped = zipper.getNextEntry(); zipped != null; zipped = zipper.getNextEntry()) {
					JSONObject artifactInfo = lookupArtifactInfo(artifacts, StringUtils.substringAfterLast(zipped.getName(), "/"));
					if (artifactInfo != null) {
//						if (theProcessor != null) {
							artifactBuilder.add(theProcessor.apply(artifactInfo, extractArtifactData(zipped, zipper)));
//						}
//						else {
//							artifactBuilder.add(artifactInfo);
//						}
  				}
	 				zipper.closeEntry();	
				}
			}
			catch (IOException iox) {
				throw new UncheckedIOException(iox);
			}
			finally {
				if (zipper != null) {
					try {	zipper.close(); } catch (IOException iox) {}
				}
			}

			if (theAggregator != null) {
				assetBuilder.add(theAggregator.apply(artifactBuilder.build()));	
			}
			else {
				artifactBuilder.build()
											 .forEach(entry -> assetBuilder.add(entry));
			}
		}

		return assetBuilder.build();
	}

	/** */
	public static void main(String[] theArgs) throws Exception {
/*
	ASDC _asdc = new ASDC();
	_asdc.setUri(new URI(theArgs[0]));
	ASDCUtils _utils = new ASDCUtils(_asdc);

	_utils.buildBlueprint(new FileReader(theArgs[1]))
					.waitForResult()
						.waitForResult();

	if (0 == 0)
		return;
*/
		CommandLineParser parser = new BasicParser();
		
		Options options = new Options();
		options.addOption(OptionBuilder
														  .withArgName("target")
															.withLongOpt("target")
                               .withDescription("target asdc system")
															.hasArg()
															.isRequired()
															.create('t') );
			
		options.addOption(OptionBuilder
														  .withArgName("action")
															.withLongOpt("action")
                               .withDescription("one of upload, update, delete, clone")
															.hasArg()
															.isRequired()
															.create('a') );

		options.addOption(OptionBuilder
														  .withArgName("assetType")
															.withLongOpt("assetType")
                               .withDescription("one of resource, service, product")
															.hasArg()
															.isRequired()
															.create('k') ); //k for 'kind' ..
		
		options.addOption(OptionBuilder
														  .withArgName("instance")
															.withLongOpt("instance")
                               .withDescription("asset instance name")
															.hasArg()
															.create('i') );

		options.addOption(OptionBuilder
														  .withArgName("assetId")
															.withLongOpt("assetId")
                               .withDescription("asset uuid")
															.hasArg()
															.isRequired()
															.create('u') ); //u for 'uuid'
		
		options.addOption(OptionBuilder
														  .withArgName("operatorId")
															.withLongOpt("operatorId")
                               .withDescription("operator at&t id")
															.hasArg()
															.isRequired()
															.create('o') );
		
		options.addOption(OptionBuilder
														  .withArgName("artifactId")
															.withLongOpt("artifactId")
                               .withDescription("artifact (subject) id")
															.hasArg()
															.create('s') );
		
		options.addOption(OptionBuilder
														  .withArgName("sourceAssetId")
															.withLongOpt("sourceAssetId")
                              .withDescription("source asset uuid (when cloning)")
															.hasArg()
															.create('x') ); 
		
		options.addOption(OptionBuilder
														  .withArgName("content")
															.withLongOpt("content")
                              .withDescription("artifact content (as a file path), for upload and update")
															.hasArg()
															.create('c') ); 

		options.addOption(OptionBuilder
														  .withArgName("blueprinter")
															.withLongOpt("blueprinter")
                              .withDescription("blueprint service uri")
															.hasArg()
															.create('b') ); 

		options.addOption(OptionBuilder
														  .withArgName("message")
															.withLongOpt("message")
                              .withDescription("lifecycle message")
															.hasArg()
															.isRequired()
															.create('m') );

		Option propertiesOption = OptionBuilder
														  .withArgName("property=value")
															.withLongOpt("property")
                              .withDescription("property to be provided as input for an action: -Pname=value")
															.hasArgs(2)
															.withValueSeparator()
															.create("P");

		options.addOption(propertiesOption);
 
		CommandLine line = null;
		try {
   		line = parser.parse(options, theArgs);
		}
		catch(ParseException exp) {
			System.err.println(exp.getMessage());
			new HelpFormatter().printHelp("asdc", options);
			return;
		}

		ASDC asdc = new ASDC();
		asdc.setUri(new URI(line.getOptionValue("target")));

		Blueprinter blue = null;
		if (line.hasOption("blueprinter")) {
			blue = new Blueprinter();
			blue.setUri(new URI(line.getOptionValue("blueprinter")));
		}	
		ASDCUtils utils = new ASDCUtils(asdc, blue);

		ASDC.ASDCAction asdcAction = null;
		String action = line.getOptionValue("action");
		String instance = line.getOptionValue("instance");
		String lifecycleTarget = null;

		if (action.equals("upload")) {
			String content = line.getOptionValue("content");
			if (content == null) {
				System.err.println("Need to indicate the artifact content (a file)");
				new HelpFormatter().printHelp("asdc", options);
				return;
			}

			if (instance == null)
				asdcAction = asdc.createAssetArtifact(ASDC.AssetType.valueOf(line.getOptionValue("assetType")),
																							UUID.fromString(line.getOptionValue("assetId")));
			else
				asdcAction = asdc.createAssetInstanceArtifact(
																							ASDC.AssetType.valueOf(line.getOptionValue("assetType")),
																							UUID.fromString(line.getOptionValue("assetId")),
																							instance);

			((ASDC.ArtifactUploadAction)asdcAction).withContent(new File(content));			
			lifecycleTarget = line.getOptionValue("assetId");
		}
		else if (action.equals("update")) {
			//normally an update is done on the result of a get ..
			String content = line.getOptionValue("content");
			if (content == null) {
				System.err.println("Need to indicate the artifact content (a file)");
				new HelpFormatter().printHelp("asdc", options);
				return;
			}
			String artifactId = line.getOptionValue("artifactId");
			if (artifactId == null) {
				System.err.println("Need to indicate the artifactId");
				new HelpFormatter().printHelp("asdc", options);
				return;
			}

			if (instance == null)
				asdcAction = asdc.updateAssetArtifact(ASDC.AssetType.valueOf(line.getOptionValue("assetType")),
																							UUID.fromString(line.getOptionValue("assetId")),
																							new JSONObject()
																								.put("artifactUUID", artifactId));
			else
				asdcAction = asdc.updateAssetInstanceArtifact(
																							ASDC.AssetType.valueOf(line.getOptionValue("assetType")),
																							UUID.fromString(line.getOptionValue("assetId")),
																							instance,
																							new JSONObject()
																								.put("artifactUUID", artifactId));
			
			((ASDC.ArtifactUpdateAction)asdcAction).withContent(new File(content));			
			lifecycleTarget = line.getOptionValue("assetId");
		}
		else if (action.equals("delete")) {
			String artifactId = line.getOptionValue("artifactId");
			if (artifactId == null) {
				System.err.println("Need to indicate the artifactId");
				new HelpFormatter().printHelp("asdc", options);
				return;
			}

			if (instance == null)
				asdcAction = asdc.deleteAssetArtifact(ASDC.AssetType.valueOf(line.getOptionValue("assetType")),
																							UUID.fromString(line.getOptionValue("assetId")),
																							UUID.fromString(artifactId));
			else
				asdcAction = asdc.deleteAssetInstanceArtifact(
																							ASDC.AssetType.valueOf(line.getOptionValue("assetType")),
																							UUID.fromString(line.getOptionValue("assetId")),
																							instance,
																							UUID.fromString(artifactId));
			lifecycleTarget = line.getOptionValue("assetId");
		}
		else if (action.equals("clone")) {
			asdcAction = utils.cloneAssetArtifacts(ASDC.AssetType.valueOf(line.getOptionValue("assetType")),
																						 UUID.fromString(line.getOptionValue("sourceAssetId")),
																						 UUID.fromString(line.getOptionValue("assetId")));
			lifecycleTarget = line.getOptionValue("sourceAssetId");
		}
		else if (action.equals("blueprint")) {
			System.out.println(
				utils.buildBlueprint(new FileReader(line.getOptionValue("content")))
					.waitForResult()
						.waitForResult()
			);
			return;	
		}
		else if (action.equals("blueprint3")) {
			System.out.println(
				utils.buildBlueprint3(new FileReader(line.getOptionValue("content")))
					.waitForResult());
			return;
		}

		asdcAction.withOperator(line.getOptionValue("operatorId"));

		Properties props = line.getOptionProperties("property");
System.out.println("props: " + props);
		if (props != null)
			asdcAction.withInfo(new JSONObject(props));

		if (lifecycleTarget != null) {
			try {
				System.out.println(
					asdc.cycleAsset(ASDC.AssetType.valueOf(line.getOptionValue("assetType")),
												UUID.fromString(lifecycleTarget),
												ASDC.LifecycleState.Checkout,
												line.getOptionValue("operatorId"),
												line.getOptionValue("message"))
							.waitForResult()
							.toString(2));
			}
			catch (Exception x) {
				System.out.println("Checkout of " + line.getOptionValue("assetType") + " " + line.getOptionValue("assetId") + " has failed: " + x);
				//try anyway	
			}
		}
		
		try {							
			System.out.println(
					asdcAction
						.execute()
						.waitForResult());
		}
		catch (Exception x) {
			System.out.println("Action on " + line.getOptionValue("assetType") + " " + line.getOptionValue("assetId") + " has failed : " + x);
			x.printStackTrace();
		}
		
		if (lifecycleTarget != null) {
			try { 
				System.out.println(
					asdc.cycleAsset(ASDC.AssetType.valueOf(line.getOptionValue("assetType")),
													UUID.fromString(lifecycleTarget),
													ASDC.LifecycleState.Checkin,
													line.getOptionValue("operatorId"),
													line.getOptionValue("message"))
							.waitForResult()
							.toString(2));
			}
			catch (Exception x) {
				System.out.println("Checkin of " + line.getOptionValue("assetType") + " " + line.getOptionValue("assetId") + " has failed : " + x);
			}
		}
	}
	

}

