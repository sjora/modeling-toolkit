package com.att.research.asc.catalog.asdc;

import java.util.Arrays;

import org.springframework.web.client.HttpClientErrorException;

import org.json.JSONObject;
import org.json.JSONArray;

/** */
public class ASDCException extends Exception {

	private static final String[] ERROR_CLASSES = new String[] { "policyException", "serviceException" };

	private JSONObject content;

	public ASDCException(HttpClientErrorException theError) {
		super(theError);

		String body = theError.getResponseBodyAsString();
		if (body != null) {
			JSONObject error = new JSONObject(body)
													.getJSONObject("requestError");
			if (error != null) {
				this.content = Arrays.stream(ERROR_CLASSES)
															.map(c -> error.optJSONObject(c))
															.filter(x -> x != null)
															.findFirst()
															.orElse(null);
			}
		}
	}

	public String getASDCMessageId() {
		return this.content == null ? "" : this.content.optString("messageId");
	}

	public String getASDCMessage() {
		if (this.content == null)
			return "";

		String msg = content.optString("text");
		if (msg != null) {
			JSONArray vars = content.optJSONArray("variables");
			if (vars != null) {
				for (int i = 0; i < vars.length(); i++) {
					msg = msg.replaceAll("%"+(i+1), vars.optString(i));
				}
			}
			return msg;
		}
		else
			return "";
	}

	@Override
	public String getMessage() {
		return "ASDC " + getASDCMessageId() + " " + getASDCMessage() + "\n" + super.getMessage();
	}
}
