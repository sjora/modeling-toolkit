package com.att.research.asc.catalog.asdc;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import java.util.Arrays;


//@Configuration
//@ComponentScan("com.att.research.asc.catalog.engine")
//@EnableAutoConfiguration
@SpringBootApplication
@ImportResource({"classpath:asdc.xml"})
public class ASDCEngine {

	public static void main(String[] args) {
		//ApplicationContext ctx = 
			SpringApplication.run(ASDCEngine.class, args);
	}

}
