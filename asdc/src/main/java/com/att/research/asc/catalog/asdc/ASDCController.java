package com.att.research.asc.catalog.asdc;

import java.util.logging.Logger;
import java.util.logging.Level;

import java.util.UUID;
import java.util.Map;
import java.util.List;
import java.util.concurrent.Callable;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
 
import org.springframework.beans.BeansException;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import com.att.research.asc.catalog.commons.MapBuilder;


@RestController
@ConfigurationProperties(prefix="asdcController")
public class ASDCController implements ApplicationContextAware {

	private static Logger log = Logger.getLogger(ASDCController.class.getName());

	private ApplicationContext			appCtx;

	public void setApplicationContext(ApplicationContext theCtx) throws BeansException {
    this.appCtx = theCtx;
	}

	@RequestMapping(value={"/resources"}, method={RequestMethod.GET}, produces={"application/json"})
	public ResponseEntity<String[]> resources(HttpServletRequest theRequest) {

		ASDC asdc = (ASDC)this.appCtx.getBean("asdc");
		List resources = null;
		try {
			resources = asdc.getResources(List.class)
												.waitForResult();
		}
		catch (Exception x) {
			return new ResponseEntity(x.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		String[] uuids = null;
		if (resources != null) {
			//why in the workd do I need the cast??
			uuids = (String[])resources.stream()
																	.map(e -> new MapBuilder()
																									.put("uuid", ((Map)e).get("uuid"))
																									.put("name", ((Map)e).get("name"))
																									.build()
																									.toString())
																	.toArray(String[]::new);
		}
		return new ResponseEntity(uuids, HttpStatus.OK);
	}
	
	@RequestMapping(value={"/resource/{theResourceId}"}, method={RequestMethod.GET}, produces={"application/json"})
	public ResponseEntity<String> resource(@PathVariable String theResourceId) {

		ASDC asdc = (ASDC)this.appCtx.getBean("asdc");
		String res = null;
		try {
			//asdc.getResourceArchive(UUID.fromString(theResourceId));
			res = asdc.getResource(UUID.fromString(theResourceId), Map.class)
											.waitForResult()
											.toString();
		}
		catch (IllegalArgumentException iax) {
			return new ResponseEntity(iax.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		}
		catch (Exception x) {
			return new ResponseEntity(x.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity(res, HttpStatus.OK);
	}
	
	@RequestMapping(value={"/services"}, method={RequestMethod.GET}, produces={"application/json"})
	public ResponseEntity<String[]> services(HttpServletRequest theRequest) {
		
		ASDC asdc = (ASDC)this.appCtx.getBean("asdc");
		List services = null;
		try {
			services = asdc.getServices(List.class)
													.waitForResult();
		}
		catch (Exception x) {
			return new ResponseEntity(x.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		String[] uuids = null;
		if (services != null) {
			uuids = (String[])services.stream()
																	.map(e -> new MapBuilder()
																									.put("uuid", ((Map)e).get("uuid"))
																									.put("name", ((Map)e).get("name"))
																									.build()
																									.toString())
																	.toArray(String[]::new);
		}
		return new ResponseEntity(uuids, HttpStatus.OK);
	}
	
	@RequestMapping(value={"/service/{theServiceId}"}, method={RequestMethod.GET}, produces={"application/json"})
	public ResponseEntity<String> service(@PathVariable String theServiceId) {

		ASDC asdc = (ASDC)this.appCtx.getBean("asdc");
		String res = null;
		try {
			res = asdc.getService(UUID.fromString(theServiceId))
								.waitForResult()
								.toString();
		}
		catch (IllegalArgumentException iax) {
			return new ResponseEntity(iax.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		}
		catch (Exception x) {
			return new ResponseEntity(x.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity(res, HttpStatus.OK);
	}

	@PostConstruct
	public void initController() {
		log.entering(getClass().getName(), "initASDCController");

		//Done
		log.log(Level.INFO, "ASDCController started");
	}

	@PreDestroy
	public void cleanupController() {
		log.entering(getClass().getName(), "cleanupASDCController");
	}

	private URI requestURI(HttpServletRequest theRequest) {
		try {
			return new URI(String.format("%s://%s:%d%s", theRequest.getScheme(),
																									 theRequest.getServerName(),
																									 theRequest.getServerPort(),
																									 theRequest.getRequestURI().toString()));
		}
		catch(URISyntaxException urisx) {
			throw new RuntimeException(urisx);
		}		
	}

	
}
